static const char *RcsId = "$Id: ClassFactory.cpp,v 1.1 2011/02/24 17:19:51 olivierroux Exp $";
//+=============================================================================
//
// file :        ClassFactory.cpp
//
// description : C++ source for the class_factory method of the DServer
//               device class. This method is responsible for the creation of
//               all class singleton for a device server. It is called
//               at device server startup
//
// project :     TANGO Device Server
//
// $Author: olivierroux $
//
// $Revision: 1.1 $
// $Date: 2011/02/24 17:19:51 $
//
// SVN only:
// $HeadURL: $
//
// CVS only:
// $Source: /cvsroot/tango-ds/Motion/Axis/XPSGroup/src/ClassFactory.cpp,v $
// $Log: ClassFactory.cpp,v $
// Revision 1.1  2011/02/24 17:19:51  olivierroux
// - initial import #18423
//
//
// copyleft :    European Synchrotron Radiation Facility
//               BP 220, Grenoble 38043
//               FRANCE
//
//-=============================================================================
//
//  		This file is generated by POGO
//	(Program Obviously used to Generate tango Object)
//
//         (c) - Software Engineering Group - ESRF
//=============================================================================


#include <tango.h>
#include <XPSBoxClass.h>
#include <XPSGroupClass.h>
#include <XPSAxisClass.h>
#include <XPSTrajectoryClass.h>
#include <XPSSingleAxisGroupClass.h>

/**
 *	Create XPSGroupClass singleton and store it in DServer object.
 */

void Tango::DServer::class_factory()
{

	add_class(XPSBox_ns::XPSBoxClass::init("XPSBox"));
	add_class(XPSGroup_ns::XPSGroupClass::init("XPSGroup"));
	add_class(XPSSingleAxisGroup_ns::XPSSingleAxisGroupClass::init("XPSSingleAxisGroup"));
	add_class(XPSTrajectory_ns::XPSTrajectoryClass::init("XPSTrajectory"));
	add_class(XPSAxis_ns::XPSAxisClass::init("XPSAxis"));

}

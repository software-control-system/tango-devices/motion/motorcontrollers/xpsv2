//-------------------------------------------------------------------
//- project : classes for XPS control
//- file : SocketPool2 Ssocket management
//-------------------------------------------------------------------


#ifndef __SOCKETPOOL_H__
#define __SOCKETPOOL_H__

#include <yat/network/ClientSocket.h>
#include <yat/threading/Mutex.h>
#include <vector>

namespace xps_ns
{
// ============================================================================
// SHORTCUT TO THE <SocketPool> SINGLETON
// ============================================================================
#define SOCK_POOL SocketPool::instance ()


//-------------------------------------------
//- some define, enums, const
//-------------------------------------------
#define MAX_COM_RETRY 5
//- TANGODEVIC-1197: tunning 3000ms instead of 30000ms
#define kCONNECTION_TMO_MSECS 3000 

//-------------------------------------------
//- class SocketItem
//-------------------------------------------
class SocketItem
{
public : 

  SocketItem (int sock_num,  std::string ip_address, int port, int itmout, int otmout,  bool is_blocking);
  ~SocketItem ();  
  
  bool is_used (void);
  bool is_com_error (void);
  bool is_blocking_cmd_sock (void) 
    {return is_sock_for_blocking_cmd;}
  int reserve (void);
  void send_recv (char sSendString[], char sReturnString[], int iReturnStringSize);
  
  //- TANGODEVIC-1197:
  //----------------------------------
  bool is_socket_lost_connection();
  
  long get_counters_comm_errors()
  {
    return m_com_consecutive_errors;
  } 
  void incr_counters_comm_errors()
  {
    m_com_consecutive_errors++;
    m_com_errors++;
  }
  
  
  void reset_counters_comm_errors()
  {
    m_com_consecutive_errors = 0;
    m_com_errors = 0;
  };
  
  yat::ClientSocket * get_socket() 
  {
    return m_sock;
  };
  
  void set_socket (yat::ClientSocket * l_socket)
  {
    m_sock = l_socket;
  }
  //- TANGODEVIC-1197:
  void connect();
  //-----------------------------------
  
private :
	size_t m_socket_number;
	yat::ClientSocket * m_sock;
	bool m_used;
	bool is_connected;
	bool is_sock_for_blocking_cmd;
	long m_com_errors;
	long m_com_consecutive_errors;
	long m_com_success;
	std::string _ip_address;
	int _port;
	int _intmout; 
	int _outmout;	
};

//-------------------------------------------
//- class SocketPool
//-------------------------------------------
class SocketPool
{

//-------------------------------------------
public: 
  //-------------------------------------------
  //- configs, enums, const
  //-------------------------------------------
  static const int TOTAL_SOCKET_NUMBER = 8;
  typedef struct Config
  {
    std::string ip_address;
    int port;
    int in_tmout;
    int o_tmout;
    int start_tmout;
    //- socket number for blocking commands (default = 1)
    size_t nb_blocking_sockets;
    //- socket number for NON blocking commands (min = 2, max =  6)
    size_t nb_non_blocking_sockets;

    //- Ctor -------------
    Config ();
    //- Ctor -------------
    Config ( const Config & _src);
    //- operator = -------
    Config& operator = (const Config & src);
  } Config;

  //----------------------------------------
  typedef enum
  {
    SW_UNKNOWN_STATE,
    SW_NOT_INITIALIZED,
    SW_INITIALIZED,
    SW_INITIALIZATION_ERROR,
    SW_NOT_CONFIGURED,
    SW_ALLOC_ERROR,
    SW_CONFIGURED,
    SW_CONFIGURATION_ERROR,
   SW_COMMUNICATION_ERROR		
  } ClassState;

  //----------------------------------------
  //- creation of singleton if not already done
  void initialize (const Config & conf);

  //- configure Objects
  void sw_run (void);

  //- release SocketPool
  void release (void);

  //-------------------------------------------
  //- get socket numbers for XPS lib
  size_t get_blocking_socket();
  size_t get_non_blocking_socket();

  //-------------------------------------------
  //- get the class state /status
  ClassState state (void);

  std::string status (void) 
  {
    yat::AutoMutex <yat::Mutex> guard (this->status_lock);
    return status_str;
  }

  //-------------------------------------------
  // Provide an interface to YAT Sockets, same interface as XPS lib
  void SendAndReceive (size_t socketID, char sSendString[], char sReturnString[], int iReturnStringSize);

  //-------------------------------------------
  static SocketPool * instance ()
  {
    if (! SocketPool::singleton)
      SocketPool::singleton = new SocketPool();
    return SocketPool::singleton;
  }

  //- TANGODEVIC-1197:
  //  revive socket in case of lost connection. 
  //------------------------------------------------
  void reset_socket_connection();

//-------------------------------------------
private :

  void set_status (std::string new_status) 
  {
    yat::AutoMutex <yat::Mutex> guard (this->status_lock);
    status_str = new_status;
  }

  //----------------------------------------
  //- CTOR / DTOR
  //----------------------------------------
  SocketPool (); 
  ~SocketPool ();

  //----------------------------------------
  //- the singleton 
  //----------------------------------------
  static SocketPool * singleton;

  //----------------------------------------
  //- configuration structure
  Config conf;

  //----------------------------------------
  // The list of all created sockets
  std::vector <SocketItem *> v_sock;
  size_t SOCK_NONBLOCK_MIN_NUMBER;
  size_t SOCK_NONBLOCK_MAX_NUMBER;
  size_t SOCK_BLOCK_MIN_NUMBER;
  size_t SOCK_BLOCK_MAX_NUMBER;

  yat::Mutex block_sock_mutex;
  yat::Mutex non_block_sock_mutex;

  size_t nb_non_block_sock_reserved_peak;

  //----------------------------------------
  //- the class state/status and useful stuff
  ClassState internal_state;
  std::string status_str;

  std::stringstream status_stream;
  yat::Mutex status_lock;

};
} //- namespace
#endif  //__SOCKETPOOL_H__

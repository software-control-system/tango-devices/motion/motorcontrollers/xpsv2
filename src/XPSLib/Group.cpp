//--------------------------------------------
//- Project XPS Group Tango Device Server
//- Group control  CPP class
//- First instanciated object
//- creates the SocketPool
//--------------------------------------------

#include <yat/Portability.h>
#include <yat4tango/ExceptionHelper.h>
#include <yat/utils/XString.h>
#include "Group.h"
#include "xps_c8_drivers.h"
//-TANGODEVIC-1265:
// const int MAXTIME_FOR_GROUP_INITIALIZE = 2000;  
const int MAXTIME_FOR_GROUP_INITIALIZE = 10000; //- Wait 10 sec  

const int MAXTIME_FOR_TMOV = 50;
const int UNKNOWN_XPS_GROUP_STATE = MAX_XPS_GROUP_STATES_STRINGS - 1;

namespace xps_ns
{
  //- Singleton initialization
  Group * Group::group = 0;

  //-------------------------------------------
  //- Config
  //-------------------------------------------
  Group::Config::Config ():
  group_name ("must be defined")
  {
    //- make some place
    positioner_names.resize (MAX_NUMBER_OF_POSITIONERS);
	positioner_tango_attribute_names.resize (MAX_NUMBER_OF_POSITIONERS);

    group_moving_timer_tmout_ms = 100;
  }

  Group::Config::Config (const Config & _src)
  {
    *this = _src;
  }

  // Group::Config::operator =
  Group::Config &  Group::Config::operator = (const Config & src)
  {
    if (& src == this)
      return *this;
    group_name       = src.group_name;
    positioner_names = src.positioner_names;
	positioner_tango_attribute_names = src.positioner_tango_attribute_names;

    group_moving_timer_tmout_ms = src.group_moving_timer_tmout_ms;
    return *this;
  }

  //-------------------------------------------
  //- CTOR
  //-------------------------------------------
  Group::Group () : 
  internal_state (Group::SW_NOT_INITIALIZED),
    xps_group_state (Group::GRP_UNKNOWN),
    trajectory (0)
  {
  //  //std::cout << "Group::Group <-\n";
    status_str.reserve (1024);
    status_str = "TODO : populate status\n";
    this->m_request_for_recopy_read_in_write = false;
  }

  //-------------------------------------------
  //- DTOR
  //-------------------------------------------
  Group::~Group ()
  {
    // NO-OP	
  }


  //-------------------------------------------
  //- Initialization
  //-------------------------------------------
  void Group::initialize (const SocketPool::Config & ip_conf,
    const Group::Config & grp_conf,
    const PeriodicTask::Config & thread_conf)
  {

//    //std::cout << "Group::initialize <-\n";
    this->internal_state = Group::SW_NOT_INITIALIZED;
    this->conf = grp_conf;
    this->previous_group_state = GRP_NOT_INIT;

    //- check group name
    if ((grp_conf.group_name == "") || 
      (grp_conf.group_name.find ("must be defined") != std::string::npos))
    {
      //std::cerr << "Group::configure <group name> error\n";
      internal_state = Group::SW_INITIALIZATION_ERROR;
    }


    //- initialize SocketPool (Tango::DevFailed on error)
    try
    {
 //     //std::cout << "Group::initialize Socket Pool\n";
 #ifndef TU_TRAJECTORY_TESTS
      SocketPool::instance ();
      SocketPool::instance()->initialize (ip_conf);
#endif	  
    }
    catch (Tango::DevFailed &e)
    {
      //std::cerr << "Group::initialize <Socket Pool> construction failed DevFailed caught <" << e.errors[0].desc << ">\n";
      internal_state = SW_INITIALIZATION_ERROR;
    }

    catch (...)
    {
      //std::cerr << "Group::initialize <Socket Pool> construction failed  (...) caught" << 
        //std::cerr << "Group::initialize : (...) caught\n";
      internal_state = SW_INITIALIZATION_ERROR;
    }

    // Now create positionners
    //std::cout << "Group::initialize check positioner list. Number of positioners= " << grp_conf.positioner_names.size()<< std::endl;

    //- check positioner list size
    if ((grp_conf.positioner_names.size () < 1) || (grp_conf.positioner_names.size () > MAX_NUMBER_OF_POSITIONERS))
    {
      //std::cerr << "Group::initialize <Positioner names list> out of range [1...8]\n";
      internal_state = Group::SW_INITIALIZATION_ERROR;
    }
	
	   //- check positioner list size
    if ((grp_conf.positioner_tango_attribute_names.size () < 1) || (grp_conf.positioner_tango_attribute_names.size () > MAX_NUMBER_OF_POSITIONERS))
    {
      //std::cerr << "Group::initialize <Positioner names list> out of range [1...8]\n";
      internal_state = Group::SW_INITIALIZATION_ERROR;
    }
	else
	{
	positioner_tango_attribute_names= grp_conf.positioner_tango_attribute_names;
	}

    size_t i = 0;
    //- create positioners vector
    try
    {

      for ( i = 0; i < grp_conf.positioner_names.size (); i++)
      {
        //std::cout << "Group::initialize () create positioner " << grp_conf.positioner_names.at(i) << "\n";
        Positioner * p = new Positioner (grp_conf.positioner_names.at(i), i);
        positioners.push_back (p);
      }
    }
    catch (...)
    {
      //std::cerr << "Group::initialize <Positioner List> initialization of <" << grp_conf.positioner_names [i] << "> failed caught (...)\n";
      internal_state = Group::SW_INITIALIZATION_ERROR;
      //- THROW_DEVFAILED ("SW_INITIALIZATION_ERROR",
      //-                  "(...) caught trying initialize Positioner",
      //-                  "Group::initialize ()");
    }

    // Instanciate refreshing task
    try
    {
      PeriodicTask::instance ();
      PeriodicTask::instance ()->initialize (thread_conf);    
    }
    catch (Tango::DevFailed &e)
    {
      //std::cerr << "Group::initialize <PeriodicTask> initialization error : Tango::DevFailed caught <" << e.errors[0].desc << ">\n";
      internal_state = Group::SW_INITIALIZATION_ERROR;
      //- throw (e);
    }
    catch (...)
    {
      //std::cerr << "Group::initialize <PeriodicTask> initialization error (...) caught\n";
      internal_state = Group::SW_INITIALIZATION_ERROR;
      //- THROW_DEVFAILED ("SW_INITIALIZATION_ERROR",
      //-                  "(...) caught trying initialize PeriodicTask",
      //-                  "Group::initialize ()");
    }

    //- configure trajectory
    Trajectory::Config traj_conf;
    traj_conf.group_name = conf.group_name;
    traj_conf.positioner_names = conf.positioner_names;
    try
    {
      //- creation of the Trajectory
      trajectory = new Trajectory (traj_conf);
#ifndef TU_TRAJECTORY_TESTS	
internal_state = Group::SW_INITIALIZED;
#endif	  
    }
    catch (Tango::DevFailed &e)
    {
      //std::cerr << "Group::initialize <Trajectory> initialization error : Tango::DevFailed caught <" << e.errors[0].desc << ">\n";
      internal_state = Group::SW_INITIALIZATION_ERROR;
      //-throw (e);
    }
    catch (...)
    {
      //std::cerr << "Group::initialize <Trajectory> initialization error : (...) caught\n";
      internal_state = Group::SW_INITIALIZATION_ERROR;
      //- THROW_DEVFAILED ("SW_INITIALIZATION_ERROR",
      //-                 "(...) caught trying initialize Trajectory",
      //-                 "Group::initialize ()");
    }


    if (internal_state == Group::SW_INITIALIZATION_ERROR)
      return;
    else
      internal_state = Group::SW_INITIALIZED;

  }

  //-------------------------------------------
  //- configuration of the group
  //-------------------------------------------
  void Group::sw_run (void)
  {
    //std::cout << "Group::sw_run <-\n";

    if (internal_state != Group::SW_INITIALIZED)
    {
 //     //std::cerr << "Group::sw_run Group initialization failed, abandon of internal classes activation\n";
      THROW_DEVFAILED ("BAD_STATE",
        "Group initialization failed, abandon of internal classes activation",
        "Group::sw_run ()");
    }

    // sw_run SocketPool BEFORE ANY [Positioner or Trajectory] creation as tey request TCP/IP communication 
    //std::cout << "Group::sw_run try to <sw_run> Socket Pool\n";
    try
    {
#ifndef TU_TRAJECTORY_TESTS	
      SocketPool::instance()->sw_run ();
#endif
    }
    catch (Tango::DevFailed &e)
    {
//      //std::cerr << "Group::sw_run <SocketPool> configuration error : Tango::DevFailed caught <" << e.errors[0].desc << ">\n";
      internal_state = Group::SW_CONFIGURATION_ERROR;
      throw (e);
    }
    catch (yat::SocketException &se)
    {
 //     //std::cerr << "Group::sw_run <SocketPool> configuration error : yat::SocketException caught <" << se.errors[0].desc << ">\n";
      internal_state = Group::SW_CONFIGURATION_ERROR;
      THROW_YAT_TO_TANGO_EXCEPTION (se);
    }
    catch (...)
    {
 //     //std::cerr << "Group::sw_run <SocketPoo>l configuration error : (...) caught\n";
      internal_state = Group::SW_CONFIGURATION_ERROR;
      THROW_DEVFAILED ("CONFIGURATION_ERROR",
        "(...) caught trying to sw_run SocketPool",
        "Group::sw_run ()");
    }

    size_t i = 0;
    try
    {
      for (i = 0; i < positioners.size (); i++)
      {
		positioners [i]->sw_run ();
      }
    }
    catch (Tango::DevFailed &e)
    {
      //std::cerr << "Group::sw_run Positioner initialization <" << positioners [i]->get_name () << ">  error : Tango::DevFailed caught <" << e.errors[0].desc << ">\n";
      internal_state = Group::SW_CONFIGURATION_ERROR;
      throw (e);
    }
    catch (...)
    {
      //std::cerr << "Group::sw_run Positioner initialization <" << positioners [i]->get_name () << ">  error : (...) caught\n";
      internal_state = Group::SW_CONFIGURATION_ERROR;
      THROW_DEVFAILED ("SW_INITIALIZATION_ERROR",
        "(...) caught trying to sw_run Positioners",
        "Group::sw_run ()");
    }

    //- start the periodic thread
    try
    {
#ifndef TU_TRAJECTORY_TESTS		
      PeriodicTask::instance ()->sw_run ();
#endif	  
    }
    catch (...)
    {
      //std::cerr << "Group::sw_run PeriodicTask initialization error : (...) caught\n";
      internal_state = Group::SW_CONFIGURATION_ERROR;
      THROW_DEVFAILED ("SW_INITIALIZATION_ERROR",
        "(...) caught trying to sw_run PeriodicTask",
        "Group::sw_run ()");
    }

    internal_state = Group::SW_CONFIGURED;
#ifndef TU_TRAJECTORY_TESTS	
    //- initialize setpoints with current positions
    this->hw_get_positions ();
#endif	
    //
    for (size_t i = 0; i < this->conf.positioner_names.size(); i++)
    {
      positioners.at (i)->set_tango_setpoint (positioners.at (i)->get_xps_setpoint () + positioners.at (i)->get_offset ());
    }

    //- AFTER POSITIONERS!
    try
    {
      trajectory->sw_run ();
    }
    catch (Tango::DevFailed &e)
    {
      std::cerr << "Group::sw_run <Trajectory> initialization error : Tango::DevFailed caught <" << e.errors[0].desc << ">\n";
      internal_state = Group::SW_CONFIGURATION_ERROR;
      throw (e);
    }
    catch (...)
    {
      //std::cerr << "Group::sw_run <Trajectory> initialization error : (...) caught\n";
      internal_state = Group::SW_CONFIGURATION_ERROR;
      THROW_DEVFAILED ("SW_INITIALIZATION_ERROR",
        "(...) caught trying initialize Trajectory",
        "Group::sw_run ()");
    }

    //- timers for MOVING State and initialize_reference_positio
    this->tmov.set_unit (yat::Timeout::TMO_UNIT_MSEC);
    this->tmov.set_value (conf.group_moving_timer_tmout_ms);
 //   this->force_state_moving (conf.group_moving_timer_tmout_ms) ;

    this->tinitialize.set_unit (yat::Timeout::TMO_UNIT_MSEC);
    this->tinitialize.set_value (MAXTIME_FOR_GROUP_INITIALIZE);
    this->tinitialize.disable ();

    //- force the copy of READ part of positions in WRITE part
    this->previous_group_state = GRP_MOVING;
    
#ifndef TU_TRAJECTORY_TESTS	
    this->hw_get_xps_setpoints ();
#endif	
    for (size_t i = 0; i < this->conf.positioner_names.size (); i++)
      tango_setpoints_internal [i].sp = positioners.at (i)->get_xps_setpoint () + positioners.at (i)->get_offset ();

    this->internal_state = Group::SW_CONFIGURED;

    //std::cout << "Group::sw_run () ending ->\n";
  }

  //-------------------------------------------
  //- returns the group State
  //-------------------------------------------
  xps_ns::Group::GroupState Group::get_group_state (void) 
  {

    if (internal_state != Group::SW_CONFIGURED)
    {
      return Group::GRP_UNKNOWN;
    }
	// 
	GroupState tmp_group_state;
    //- see doc in ../../doc for explanation of values
//std::cout << "  xps_ns::Group::GroupState Group::get_group_state (void) , xps_group_state=  "<< xps_group_state <<std::endl;
   switch (xps_group_state)
      {
      case 0:
      case 1:
      case 2:
      case 3:
      case 4:
      case 5:
      case 6:
      case 7:
      case 8:
      case 9:
      case 42:
      case 50:
      case 52:
      case 60:
      case 61:
      case 62:
      case 63:
      case 66:
      case 67:
      case 71:
      case 72:
      case 83:
         tmp_group_state = Group::GRP_NOT_INIT;
         break;
      case 10:
      case 11:
      case 12:
      case 13:
      case 14:
      case 15:
      case 16:
      case 17:
      case 18:
      case 19:
      case 56:
      case 70:
      case 77:
      case 79:
        tmp_group_state = Group::GRP_READY;
        break;
      case 20:
      case 21:
      case 22:
      case 23:
      case 24:
      case 25:
      case 26:
      case 27:
      case 28:
      case 29:
      case 30:
      case 31:
      case 32:
      case 33:
      case 34:
      case 35:
      case 36:
      case 37:
      case 38:
      case 39:
      case 58:
      case 59:
      case 74:
      case 75:
      case 76:
      case 80:
      case 81:
      case 82:
      case 84:
      case 85:
      case 86:
      case 87:
      case 88:
      case 89:
      case 90:
      case 91:
      case 92:
      case 93:
        tmp_group_state  = Group::GRP_DISABLED;
        break;
      case 40:
        this->group_state = Group::GRP_EMERGENCY_BRAKE;
        break;
      case 41:
      case 49:
      case 64:
      case 65:
      case 68:
      case 69:
        tmp_group_state  = Group::GRP_INITIALIZING;
        break;
      case 43:
      case 44:
      case 45:
      case 47:
      case 48:
      case 51:
      case 55:
      case 78:
        tmp_group_state  = Group::GRP_MOVING;
        break;
      case 46:
      case 73:
        tmp_group_state  = Group::GRP_RUNNING;
        break;
      default:
		tmp_group_state  =Group::GRP_UNKNOWN;
        break;
      }  //end switch 
	  

		// during the tmov timer period we trust the state returned by the XPS only after we detect a first MOVING transition TANGODEVIC-623

//std::cout << "  xps_ns::Group::GroupState Group::get_group_state (void) , tmp_group_state=  "<< tmp_group_state <<std::endl;

    if ((tmov.enabled()) && (!tmov.expired()))
    {
//  	//std::cout << "Group::get_group_state return GRP_MOVING because tmov timer not expired . tmov.time_to_expiration = "<< tmov.time_to_expiration()  << std::endl;
		if (tmp_group_state  != Group::GRP_MOVING)
			{ 
			this->group_state =Group::GRP_MOVING;
			//std::cout << "  xps_ns::Group::GroupState Group::--->> FORCED STATE  group_state=  "<< group_state <<std::endl;
			}
		else
			{
			this->group_state =Group::GRP_MOVING;
			tmov.disable();
			//std::cout << "  xps_ns::Group::GroupState Group::  <<--->> BREAK TIMER  group_state=  "<< group_state <<std::endl;
			}
    }
	else
	{	
	this->group_state =tmp_group_state ;
	}
	//  return Group state
  	return group_state;
}
  //-------------------------------------------
  //- returns the group name
  //-------------------------------------------
  std::string Group::group_name (void)
  {
    //std::cout << "Group::group_name <-" << conf.group_name << "\n";
    if (conf.group_name.find ("must be defined") != std::string::npos)
    {
      //std::cerr << "Group::group_name <group name not configured> error\n";
      THROW_DEVFAILED ("PARAMETER_ERROR",
        "Group name not defined [call support]",
        "Group::group_name ()");
    }
    return conf.group_name;
  }

  //-------------------------------------------
  //- returns the trajectory pointer
  //-------------------------------------------
  Trajectory * Group::get_trajectory (void)
  {
    if (trajectory)
      return trajectory;

    //std::cerr << "Group::get_trajectory <trajectory not instanciated> error\n";
    THROW_DEVFAILED ("CONFIGURATION_ERROR",
      "trajectory not instanciated [call support]",
      "Group::get_trajectory ()");

  }

  //-------------------------------------------
  //- returns the Positioner pointer
  //-------------------------------------------
  Positioner * Group::get_positioner (std::string requested_name) const
  {
    //std::cout << "Group::get_positioner for name <" << requested_name << "> <-\n";

    for (size_t i = 0; i < positioners.size (); i++)
    {
      if (positioners.at (i)->get_name () == requested_name)
        return positioners [i];
    }
    //std::cerr << "Group::get_positioner <positioner name not found> error\n";
    THROW_DEVFAILED ("CONFIGURATION_ERROR",
      "Positioner name not found [call support]",
      "Group::get_positioner ()");
  }

  // ============================================================================
  // Group::start
  // ============================================================================
  void Group::start () 
  {
    //- all OK : get setpoint from each positioner
  //  //std::cout << "Group::start" << std::endl;
    if (internal_state != SW_CONFIGURED)
    {
      //std::cerr << "Group::start () OPERATION_NOT_ALLOWED initialisation of Group failed [check properties, power on XPS, cables...]\n";
      THROW_DEVFAILED ("OPERATION_NOT_ALLOWED",
        "initialisation of Group failed [check properties, power on XPS, cables...]",
        "Group::start");
    }
    if (this->is_moving ())
    {
      std::stringstream s;
      //std::cerr <<  "Group::start () group is already moving\n";
      THROW_DEVFAILED ("OPERATION_NOT_ALLOWED",
        "group is already moving",
        "Group::start");
    }

    this->go_to_pos_internal ();
  }

  // ============================================================================
  // Group::go_to_positions
  // ============================================================================
  void Group::go_to_positions (std::vector <double> positions) 
  {
    //std::cout << "Group::go_to_positions" << std::endl;
    if (internal_state != SW_CONFIGURED)
    {
      //std::cerr << "Group::go_to_positions () OPERATION_NOT_ALLOWED initialisation of Group failed [check properties, power on XPS, cables...]\n";
      THROW_DEVFAILED ("OPERATION_NOT_ALLOWED",
        "initialisation of Group failed [check properties, power on XPS, cables...]",
        "Group::go_to_positions");
    }
    if (this->is_moving ())
    {
      std::stringstream s;
      //std::cerr <<  "Group::go_to_positions group is already moving\n";
      THROW_DEVFAILED ("OPERATION_NOT_ALLOWED",
        "group is already moving",
        "Group::go_to_positions");
    }
// Reset Timemout to the group timer value 
//    this->force_state_moving (conf.group_moving_timer_tmout_ms) ;

    if (positions.size () != positioners.size ())
    {
      //std::cerr << "Group::go_to_positions () mismatch between positioner number and positions number\n";
      THROW_DEVFAILED ("SOFTWARE ERROR",
        " mismatch between positioner number and positions numbe [check properties, call support]",
        "Group::go_to_positions");
    }
    //- write the setpoints in the positioner class instances
    //- so that offsets are correctly managed
    for (size_t i = 0; i < positioners.size (); i++)
    {
      positioners [i]->set_tango_setpoint (positions.at (i));
      tango_setpoints_internal[i].new_setpoint_present = true;
      tango_setpoints_internal[i].sp = positions.at (i);
    }

    this->go_to_pos_internal ();
  }

  // ============================================================================
  // Group::go_to_pos_internal
  // ============================================================================
  void Group::go_to_pos_internal (void)
  {
 //   //std::cout << "Group::go_to_pos_internal <-\n";
 // Reset Timemout to the group timer value 
    this->force_state_moving () ;

   if (internal_state != SW_CONFIGURED)
    {
      //std::cerr << "Group::go_to_pos_internal () OPERATION_NOT_ALLOWED initialisation of Group failed [check properties, power on XPS, cables...]\n";
      THROW_DEVFAILED ("OPERATION_NOT_ALLOWED",
        "initialisation of Group failed [check properties, power on XPS, cables...]",
        "Group::go_to_pos_internal");
    }
    
    double setp [MAX_NUMBER_OF_POSITIONERS];
    for (size_t i = 0; i < positioners.size (); i++)
    {
      //- check for setpoint has non-initialized value
      if (yat::is_nan (positioners.at(i)->get_tango_setpoint()))
      {
        std::stringstream s;
        s << "positioner <" << positioners.at(i)->get_name () << ">"
          << " setpoint value <" << positioners.at(i)->get_tango_setpoint () << ">"
          << "not initialized!";
        //std::cerr << "Group::go_to_positions () " << s.str () << std::endl;
        THROW_DEVFAILED ("PARAMETER_ERROR",
          s.str ().c_str (),
          "Group::go_to_pos_internal ()");
      }
      //- check for setpoint out of soft limits 
      double sltmin = 0.0;
      double sltmax = 0.0;
      positioners.at (i)->get_tango_min_max_limits (sltmin, sltmax);
      double setpoint = positioners.at (i)->get_tango_setpoint ();
      if ((setpoint < sltmin) || (setpoint > sltmax))
      {
        std::stringstream s;
        s << "positioner <" << positioners.at(i)->get_name () << ">"
          << " setpoint value <" << positioners.at(i)->get_tango_setpoint () << ">"
          << "out of range [" << sltmin << "|" << sltmax << "]";
        //std::cerr << "Group::go_to_pos_internal () " << s.str () << std::endl;
        THROW_DEVFAILED ("OUT_OF_RANGE",
          s.str ().c_str (),
          "Group::go_to_pos_internal ()");
      }
//-      setp [i] = (positioners.at (i)->get_tango_setpoint () - positioners.at (i)->get_offset ());
      //- Yves Garreau demands partie W de l'attribut si l'attribut a ete ecrit ou xps setpoint
      setp [i] = tango_setpoints_internal[i].new_setpoint_present ? 
                 tango_setpoints_internal[i].sp - positioners.at (i)->get_offset () : 
                 positioners.at (i)->get_xps_setpoint ();
      tango_setpoints_internal[i].new_setpoint_present = false;
      
 /*     if (tango_setpoints_internal[i].new_setpoint_present)
        //std::cout << "go_to_pos_internal : positioner <" << positioners.at (i)->get_name () << "> YES setpoint present <" << setp[i] << ">\n"; 
      else
        //std::cout << "go_to_pos_internal : positioner <" << positioners.at (i)->get_name () << "> NO setpoint present <" << setp[i] << ">\n"; 
 */
   }
    size_t s_b = SocketPool::instance()->get_blocking_socket();
    int ret = GroupMoveAbsolute (s_b,
      const_cast <char*> (conf.group_name.c_str()),
      positioners.size (),
      setp);
    throw_on_error (std::string ("Group::go_to_pos_internal::GroupMoveAbsolute"), ret);
  }

  // ============================================================================
  // Group::stop
  // ============================================================================
  void Group::stop () 
  {
    //std::cout << "Group::stop" << std::endl;
    if (internal_state != SW_CONFIGURED)
    {
      //std::cerr << "Group::stop () OPERATION_NOT_ALLOWED initialisation of Group failed [check properties, power on XPS, cables...]\n";
      THROW_DEVFAILED ("OPERATION_NOT_ALLOWED",
        "initialisation of Group failed [check properties, power on XPS, cables...]",
        "Group::off");
    }
    //- request to recopy positions in setpoints to avoid possible collision problems with setpoints not updated
    this->request_for_recopy_read_in_write ();
    this->hw_get_group_state();
    //- jogging
    if ((this->get_xps_group_state () == 47))
    {
      //std::cout << "Group::stop () before GroupJogModeDisable" << std::endl;
      size_t s_n_b = SocketPool::instance()->get_non_blocking_socket();
      int ret = GroupJogModeDisable (s_n_b, const_cast <char*> (conf.group_name.c_str()));
      if ( ret!=-22 )
      {
        throw_on_error (std::string ("Group::stop::GroupJogModeDisable"), ret);
      }
    }
    //- positioning
    else
    {
      //std::cout << "Group::stop () before GroupMoveAbort" << std::endl;
      size_t s_n_b = SocketPool::instance()->get_non_blocking_socket();
      int ret = GroupMoveAbort (s_n_b, const_cast <char*> (conf.group_name.c_str()));
      if ( ret!=-22 )
      {
        throw_on_error (std::string ("Group::stop::GroupMoveAbort"), ret);
      }
    }    
    return;
  }

  // ============================================================================
  // Group::kill
  // ============================================================================
  void Group::kill () 
  {
    //std::cout << "Group::kill" << std::endl;
    if (internal_state != SW_CONFIGURED)
    {
      //std::cerr << "Group::kill () OPERATION_NOT_ALLOWED initialisation of Group failed [check properties, power on XPS, cables...]\n";
      THROW_DEVFAILED ("OPERATION_NOT_ALLOWED",
        "initialisation of Group failed [check properties, power on XPS, cables...]",
        "Group::kill");
    }
    size_t s_b = SocketPool::instance()->get_blocking_socket();
    int ret = GroupKill (s_b, const_cast <char*> (conf.group_name.c_str()));
    throw_on_error (std::string ("Group::kill"), ret);
  }

  // ============================================================================
  // Group::on
  // ============================================================================
  void Group::on () 
  {
    //std::cout << "Group::on" << std::endl;
    if (internal_state != SW_CONFIGURED)
    {
      //std::cerr << "Group::on () OPERATION_NOT_ALLOWED initialisation of Group failed [check properties, power on XPS, cables...]\n";
      THROW_DEVFAILED ("OPERATION_NOT_ALLOWED",
        "initialisation of Group failed [check properties, power on XPS, cables...]",
        "Group::on");
    }
    size_t s_b = SocketPool::instance()->get_blocking_socket();
    int ret = GroupMotionEnable (s_b, const_cast <char*> (conf.group_name.c_str()));
    throw_on_error (std::string ("Group::on::GroupMotionEnable"), ret);
    //
    //JIRA TANGODEVIC-414 
    //- sleep 300 ms to let controleur take into account the command before executing next ones
    omni_thread::sleep (0, 300000000);	  
  }

  // ============================================================================
  // Group::off
  // ============================================================================
  void Group::off () 
  {
    //std::cout << "Group::off" << std::endl;
    if (internal_state != SW_CONFIGURED)
    {
      //std::cerr << "Group::off () OPERATION_NOT_ALLOWED initialisation of Group failed [check properties, power on XPS, cables...]\n";
      THROW_DEVFAILED ("OPERATION_NOT_ALLOWED",
        "initialisation of Group failed [check properties, power on XPS, cables...]",
        "Group::off");
    }
    this->hw_get_group_state();

    if ((this->get_xps_group_state () == 44) ||
      (this->get_xps_group_state () == 47))
      this->stop ();

    size_t s_b = SocketPool::instance()->get_blocking_socket();
    int ret = GroupMotionDisable (s_b, const_cast <char*> (conf.group_name.c_str()));
    throw_on_error (std::string ("Group::on::GroupMotionDisable"), ret);
    //JIRA TANGODEVIC-414 
    //- sleep 300 ms to let controleur take into account the command before executing next ones
     omni_thread::sleep (0, 300000000);

  }

  // ============================================================================
  // Group::initialize_reference_position
  // ============================================================================
  void Group::initialize_reference_position () 
  {
    //std::cout << "Group::initialize_reference_position <-" << std::endl;
    if (internal_state != SW_CONFIGURED)
    {
      //std::cerr << "Group::initialize_reference_position () OPERATION_NOT_ALLOWED initialisation of Group failed [check properties, power on XPS, cables...]\n";
      THROW_DEVFAILED ("OPERATION_NOT_ALLOWED",
        "initialisation of Group failed [check properties, power on XPS, cables...]",
        "Group::initialize_reference_position");
    }
	//- TANGODEVIC-1265:
	// GroupKill and wait group state = NOTINIT <7>	
    do
    {
	   size_t s_b = SocketPool::instance()->get_blocking_socket();
	   int ret = GroupKill (s_b, const_cast <char*> (conf.group_name.c_str()));
	  
	  // Wait group state = NOTINIT
	  //- sleep 50 ms
	  omni_thread::sleep (0, 50000000);
	   
	   //std::cout << "Group::initialize_reference_position, made GroupKill ret code <" << ret << ">" << std::endl;
	} while ( this->get_xps_group_state () != 7);
	
	
	//- TANGODEVIC-1265:
	// Group state = NOTINIT
    if (this->get_xps_group_state () <= 9) 
    {  
	   size_t s_b = SocketPool::instance()->get_blocking_socket(); 
	   int ret = GroupInitialize (s_b, const_cast <char*> (conf.group_name.c_str()));
	   //std::cout << "Group::initialize_reference_position, made GroupInitialize ret code <" << ret << ">" << std::endl;
       throw_on_error (std::string ("Group::initialize_reference_position::GroupInitialize"), ret);

	   //- TANGODEVIC-1265:
	   //- wait for state to be NOTREF <42>
	   // Reset Timemout to the group timer value 
	   this->force_state_moving () ;
	   
	   while ( this->get_xps_group_state () != 42)
       {
		  // Wait group state = NOTREF
		  //- sleep 50 ms
	   	   omni_thread::sleep (0, 50000000);
	   }      
	}

	// GroupHomeSearch and wait group state = HOMING				
	do
	{
	    size_t s_b = SocketPool::instance()->get_blocking_socket();
	    int ret = GroupHomeSearch (s_b, const_cast <char*> (conf.group_name.c_str()));
	    
		// Wait group state = HOMING
	    //- sleep 50 ms
	    omni_thread::sleep (0, 50000000);
	} while ( this->get_xps_group_state () != 43);
		
    //- try to restore soft limits
    for (size_t i = 0; i < positioners.size (); i++)
      positioners[i]->restore_tango_min_max_limits ();

    return;
  }

// ============================================================================
  // Group::define_positions
  // ============================================================================
  void Group::define_positions (std::vector <double> positions) 
  {
 //   std::cout << "Group::define_positions " << std::endl;
   
  //  std::cout << "Group::define_positions positions.size ()= " << positions.size ()  << std::endl;
  //  std::cout << "Group::define_positions positioners.size ()= " << positioners.size ()  << std::endl;

    if (internal_state != SW_CONFIGURED)
    {
       THROW_DEVFAILED ("OPERATION_NOT_ALLOWED",
        "initialisation of Group failed [check properties, power on XPS, cables...]",
        "Group::define_positions");
    }

 
    if (positions.size () != positioners.size ())
    {
      //std::cerr << "Group::define_positions () mismatch between positioner number and positions number\n";
    std::string st_positions=   yat::XString<double>::to_string(positions.size () );
    std::string st_positioners=   yat::XString<double>::to_string(positioners.size () );
    
    std::string message_to_send= 
            " mismatch between positioners number = " + 
            st_positioners + " and positions number= " + st_positions+ " [check properties, call support]";
            
           THROW_DEVFAILED ("SOFTWARE ERROR",message_to_send.c_str(),"Group::define_positions");
    }
    int ret;
    size_t s_n_b = SocketPool::instance()->get_non_blocking_socket();
    ret = GroupKill (s_n_b, const_cast <char*> (conf.group_name.c_str()));
    throw_on_error (std::string ("Group::define_positions::GroupKill"), ret);

    s_n_b = SocketPool::instance()->get_non_blocking_socket();
    ret = GroupInitialize (s_n_b, const_cast <char*> (conf.group_name.c_str()));
    throw_on_error (std::string ("Group::define_positions::GroupInitialize"), ret);

    s_n_b = SocketPool::instance()->get_non_blocking_socket();
    ret = GroupReferencingStart (s_n_b, const_cast <char*> (conf.group_name.c_str()));
    throw_on_error (std::string ("Group::define_positions::GroupReferencingStart"), ret);

    std::string full_name;
    for (size_t i = 0; i < positions.size (); i++)
    {
      full_name = conf.group_name + "." + conf.positioner_names.at (i);
      s_n_b = SocketPool::instance()->get_non_blocking_socket();
      ret = GroupReferencingActionExecute (s_n_b, const_cast <char*> (full_name.c_str()), "SetPosition", "None", positions.at (i));
      throw_on_error (std::string ("Group::define_positions::GroupReferencingActionExecute"), ret);
    }
    s_n_b = SocketPool::instance()->get_non_blocking_socket();
    ret = GroupReferencingStop (s_n_b, const_cast <char*> (conf.group_name.c_str()));
    throw_on_error (std::string ("Group::define_positions::GroupReferencingStop"), ret);
    
    //- try to restore soft limits
    for (size_t i = 0; i < positioners.size (); i++)
      positioners[i]->restore_tango_min_max_limits ();

    return;
  }

  // ============================================================================
  // Group::hw_get_xps_setpoints
  // ============================================================================
  void Group::hw_get_xps_setpoints (void)
  {
#ifndef TU_TRAJECTORY_TESTS	  
	//- //std::cout << "Group::hw_get_setpoints" << std::endl;
	if (internal_state != SW_CONFIGURED)
		return;

    // Get data from Hardware
    try 
    {
		double xps_setp [MAX_NUMBER_OF_POSITIONERS];
		size_t s_n_b = SocketPool::instance()->get_non_blocking_socket();
		int ret = GroupPositionSetpointGet (s_n_b,
		const_cast <char*> (conf.group_name.c_str()),
							conf.positioner_names.size(),
							xps_setp);

		throw_on_error ("Group::hw_get_setpoints::GroupPositionSetpointGet", ret);
 
		// No exception then store read values
		for (size_t i = 0; i < this->conf.positioner_names.size(); i++)
			positioners.at (i)->set_xps_setpoint (xps_setp [i]);

    }
	// if error reading values store NAN
    catch (Tango::DevFailed &e)
    {
      //std::cerr << "Group::hw_get_xps_setpoints (DevFailed) caught <" << e.errors[0].desc << ">\n";

      // No data in positionner 
		for (size_t i = 0; i < this->conf.positioner_names.size(); i++)
			positioners.at (i)->set_xps_setpoint (yat::IEEE_NAN);

     // rethrow exception
     throw(e);
    }
#endif	
  }

  // ============================================================================
  // Group::hw_get_group_state
  // ============================================================================
  void Group::hw_get_group_state (void) 
  {
    //- //std::cout << "Group::hw_get_group_status " << std::endl;
    if (internal_state != SW_CONFIGURED)
      return;
#ifndef TU_TRAJECTORY_TESTS	
    try 
    {
      size_t s_n_b = SocketPool::instance()->get_non_blocking_socket();
      int ret = GroupStatusGet (s_n_b,
								const_cast <char*> (conf.group_name.c_str()),
								& xps_group_state);

      throw_on_error ("Group::hw_get_group_state::GroupStatusGet", ret);
    }
    catch (Tango::DevFailed &e)
    {
      //std::cout << "Group::hw_get_group_state (DevFailed) caught <" << e.errors[0].desc << ">\n";
      // No valid state from HW
      xps_group_state = UNKNOWN_XPS_GROUP_STATE;

     // rethrow exception
     throw(e);
    }

    catch (...)
    {
      //std::cout << "Group::hw_get_group_state (..) caught <" <<  ">\n";
      // No valid state from HW
      xps_group_state = UNKNOWN_XPS_GROUP_STATE;

     // throw exception
      THROW_DEVFAILED ("COMMUNICATION_ERROR",
        "Cannot get group state from HW [call soft support]",
        "Group::hw_get_group_state");
    }
#endif	
  }

  // ============================================================================
  // Group::hw_get_positions
  // ============================================================================
  void Group::hw_get_positions (void) 
  {
    if (internal_state != SW_CONFIGURED)
    {
      //std::cerr << "Group::hw_get_positions internal_state is < " << class_states_str [this->internal_state] << ">\n";
      return;
    }
#ifndef TU_TRAJECTORY_TESTS	
    try
    {
		// Get data from Hardware
		double pos [MAX_NUMBER_OF_POSITIONERS];
		size_t s_n_b = SocketPool::instance()->get_non_blocking_socket();
		int ret = GroupPositionCurrentGet ( s_n_b,
											const_cast <char*> (conf.group_name.c_str()),
											conf.positioner_names.size(),
											pos);

		throw_on_error ("Group::hw_get_positions::GroupPositionCurrentGet", ret);
  
        // Store position in Positioners
        for (size_t i = 0; i < this->conf.positioner_names.size(); i++)
        {
			positioners.at (i)->set_read_position (pos [i]);
  //       cout << "positioner" << positioners.at (i)->get_name() << ",pos="<< pos [i] << endl;
  //        cout << "positioner" << positioners.at (i)->get_name() << ",get_position()="<< positioners.at (i)->get_position() << endl;
        }
    }
	// if error reading hardware store NaN
    catch (Tango::DevFailed &e)
    {
		for (size_t i = 0; i < this->conf.positioner_names.size(); i++)
			positioners.at (i)->set_read_position (yat::IEEE_NAN);

     // rethrow exception
     throw(e);
    }
#endif
  }

  // ============================================================================
  // Group::hw_get_min_max_limits
  // ============================================================================
  void Group::hw_get_min_max_limits (void) 
  {
    if (internal_state != SW_CONFIGURED)
    {
      //std::cerr << "Group::hw_get_positions internal_state is < " << class_states_str [this->internal_state] << ">\n";
      return;
    }

    try
    {
		for (size_t i = 0; i < this->conf.positioner_names.size(); i++)
        {
			positioners.at (i)->refresh_xps_min_max_limits();
        }
    }
    catch (Tango::DevFailed &e)
    {
      // 
	  // TODO : set xps limits min max --> NaN ????
	   //std::cerr << "Group::hw_get_min_max_limits (DevFailed) caught <" << e.errors[0].desc << ">\n";

     // rethrow exception
     throw(e);
    }

  }

  // ============================================================================
  // Group::throw_on_error
  // ============================================================================
  void Group::throw_on_error (std::string origin, int return_code) 
  {
    //- //std::cout << "Group::throw_on_error <-" << std::endl;
    if (return_code == 0)
      return;
    //std::cerr << "Group::throw_on_error origin <" << origin << " err code <" << return_code << ">"  << std::endl;
    size_t s_n_b = SocketPool::instance()->get_non_blocking_socket();
    int c = ErrorStringGet (s_n_b, return_code, this->ret_code_string);
    if (c != 0)
    {
      //std::cerr << "Group::throw_on_error ErrorStringGet failed with code <" << c << ">"  << std::endl;
      THROW_DEVFAILED ("COMMUNICATION_ERROR",
        "error code returned by ErrorStringGet [call soft support]",
        "Group::throw_on_error");
    }
    //std::cerr << "Group::throw_on_error command refused with description <" << this->ret_code_string << ">"  << std::endl;
    THROW_DEVFAILED("COMMAND_ERROR",
      ret_code_string,
      origin.c_str ());
  }

  // ============================================================================
  // Group::force_state_moving
  // ============================================================================
  void Group::force_state_moving () 
  {
//    this->tmov.set_value (val_ms);
    this->tmov.restart ();
  }

  // ============================================================================
  // Group::synchronise_tango_setpoints
  // periodically called by the periodic thread 
  // for specific purposes
  // ============================================================================
  void Group::synchronise_tango_setpoints (void) 
  {
    //- //std::cout << "Group::periodic_call <-" << std::endl;
    //- used to trig a copy of attribute position read values to respective write parts
//    if (!this->m_request_for_recopy_read_in_write)
//      return;
    
    GroupState tmp_state = this->get_group_state ();
    if (this->previous_group_state == GRP_MOVING && tmp_state != GRP_MOVING)
    {
      //- update xps setpoint readback
      this->hw_get_xps_setpoints ();
      
      for (size_t i = 0; i < positioners.size (); i++)
      {
/* std::cout << "Group::synchronise_tango_setpoints synchronisation demandee pour le positioner 
          << positioners [i]->get_name () << "> position courante <" 
          << positioners [i]->get_position() << "> consigne courante <"
          << positioners [i]->get_tango_setpoint() << ">\n";
 */       
        tango_setpoints_internal [i].sp = positioners [i]->get_xps_setpoint() + positioners [i]->get_offset ();  
        //- positioners [i]->set_tango_setpoint (positioners [i]->get_xps_setpoint() + positioners [i]->get_offset ());
      }
      this->m_request_for_recopy_read_in_write = false;
    }
    this->previous_group_state = tmp_state;
  }

  // ============================================================================
  // Group::release 
  // ============================================================================
  void Group::release (void) 
  {
    // Just release Socket ressources but keep Positionners objects constructed as they are referenced by XPSAxis devices
    internal_state = Group::SW_NOT_INITIALIZED;
    SocketPool::instance ()->release ();
    //- kill the periodic thread
    PeriodicTask::close ();
    // release positionners ressources
    for (size_t i = 0; i < positioners.size (); i++)
    {
      positioners[i]->release();
    }
  }

  // ============================================================================
  // Group::set_tango_setpoints_internal
  // ============================================================================
  void Group::set_tango_setpoints_internal (double src, int positioner_number)
  {
    tango_setpoints_internal[positioner_number].new_setpoint_present = true;
    tango_setpoints_internal[positioner_number].sp = src;
  }

  // ============================================================================
  // Group::get_tango_setpoints_internal
  // ============================================================================
  double Group::get_tango_setpoints_internal (int positioner_number)
  {
    return tango_setpoints_internal[positioner_number].sp;
  }

  // ============================================================================
  // Group::get_ordered_positioner_list
  // return ordered list of positioners as expected for  Trajectory loading
  // ============================================================================
  std::vector<std::string> Group::get_ordered_positioners_list()
  {
    std::vector<std::string> tmp;
    for (size_t i = 0; i < positioners.size (); i++)
    {
      tmp.push_back( positioner_tango_attribute_names [i]);
    }
    return tmp;
  }

  // ============================================================================
  // Group::exec_low_level_cmd
  // ============================================================================
  std::string Group::exec_low_level_cmd(std::string cmd)
  {
    //std::cout << "Group::exec_low_level_cmd" << std::endl;
    if (internal_state != SW_CONFIGURED)
    {
      //std::cerr << "Group::exec_low_level_cmd () OPERATION_NOT_ALLOWED initialisation of Group failed [check properties, power on XPS, cables...]\n";
      THROW_DEVFAILED("OPERATION_NOT_ALLOWED",
            "initialisation of Group failed [check properties, power on XPS, cables...]",
            "Group::exec_low_level_cmd");
    }

    char strOut[65535];
    size_t s_b = SocketPool::instance()->get_blocking_socket();

    int ret = ExecLowLevelCmd(s_b,
            const_cast <char*> (cmd.c_str()),
            &strOut[0]);

    throw_on_error(std::string("Group::exec_low_level_cmd::ExecLowLevelCmd"), ret);

    return (std::string(strOut));
  }

}//- namespace

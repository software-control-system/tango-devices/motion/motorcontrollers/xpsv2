//=============================================================================
// PeriodicTask.h
//=============================================================================
// abstraction.......periodic task 
// class.............XPS Group V2
// original author...JC d'apres projet ControlBoxV2 de N.Leclercq - SOLEIL
//=============================================================================

#ifndef _PERIODIC_TASK_H_
#define _PERIODIC_TASK_H_

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <yat/threading/Task.h>
#include <yat4tango/ExceptionHelper.h>
#include <yat/time/Timer.h>

// ============================================================================
// SHORTCUT TO THE <PeriodicTask> SINGLETON
// ============================================================================
#define PERIODIC_TASK xps_ns::PeriodicTask::instance()

// ============================================================================
// CONSTs
// ============================================================================
#define kDEFAULT_HWSP_PERIOD            100
#define kDEFAULT_HWSP_STARTUP_TMO       3000
#define kDEFAULT_STATUS_EXPIRATION_TMO  4 * kDEFAULT_HWSP_PERIOD

//-----------------------------------------------------------------------------
#define kDEFAULT_WMH_TMO                975
//-----------------------------------------------------------------------------


namespace xps_ns
{
// ============================================================================
// SOME USER DEFINED MESSAGES
// ============================================================================
//-----------------------------------------------------------------------------
// ============================================================================
// CONSTs
// ============================================================================
  const int MAX_COM_ERROR_LIMIT = 10;
//-----------------------------------------------------------------------------
    typedef enum
    {
    SW_UNKNOWN_STATE,
    SW_NOT_INITIALIZED,
    SW_INITIALIZED,
    SW_INITIALIZATION_ERROR,
    SW_NOT_CONFIGURED,
    SW_ALLOC_ERROR,
    SW_CONFIGURED,
    SW_CONFIGURATION_ERROR
    }ClassState;

// ============================================================================
// class: PeriodicTask <SINGLETON>
// ============================================================================
class PeriodicTask : public yat::Task
{

public:

  //- create a dedicated type for PeriodicTask configuration
  //---------------------------------------------------------
  typedef struct Config
  {
    //- members --------------------
    size_t period_ms;
    size_t startup_timeout_ms;
    size_t status_expiration_timeout_ms;
    //- ctor -----------------------
    Config ();
    //- operator= ------------------
    Config& operator = (const Config& src);
  } Config;

  //----------------------------------------
  // SINGLETON ACCESSOR (SEE HW_STATUS MACRO)
  //----------------------------------------
  static PeriodicTask * instance () throw (Tango::DevFailed);

  //- The following members are thread safe
  //----------------------------------------

  //- singleton configuration --------------
  void initialize (const Config& cfg)
		throw (Tango::DevFailed);


  //- Timestamp of the last 'full' status update
  inline void last_update_timestamp (yat::Timestamp& ts, 
                                     double& elapsed_secs)
  {
    //- enter critical section
    yat::AutoMutex<> guard(this->Task::m_lock);
    //- get both timestamp and elapsed secs
	  yat::Timestamp now;
    _COPY_TIMESTAMP(this->m_last_update_ts, ts);
	  _GET_TIME(now);
	  elapsed_secs = _ELAPSED_SEC(this->m_last_update_ts, now);
  }

  //- Returns the "status expired" error counter
  inline unsigned long status_expired_error_counter (void)
  {
    unsigned long c = 0;
    {
      //- enter critical section
      yat::AutoMutex<> guard(this->Task::m_lock);
      // get value
      c = this->m_status_expired_error_counter;
    }
	  return c;
  }

  //- suspend the task activity
  void suspend_hw_status_update (void)
		throw (Tango::DevFailed);
  
  //- resume the task activity
  void resume_hw_status_update (void)
		throw (Tango::DevFailed);
  

  //- run thread --------------
  void sw_run ()
		throw (Tango::DevFailed);

  //- singleton release --------------------
  static void close ()
		throw (Tango::DevFailed);

  
  //- access to XPS firmware Revision
  short xps_revision (void);

  //- access to HW methods is done through Group class

  int get_consecutive_com_errors_counter (void)
  {
    return m_consecutive_comm_errors;
  }

  //- Communication State stuff ------------
  ClassState get_class_state ( void)
  {
    return class_state;
  }

  //- force status update
  void force_status_update (void);

  unsigned long get_com_error() { return m_com_error;};

protected:
	//- handle_message -----------------------
	virtual void handle_message (yat::Message& msg)
		throw (yat::Exception);

private:
	
  //- The following members are NOT thread safe
  //-------------------------------------------

  //- the singleton ------------------------
  static PeriodicTask * singleton;

	//- ctor ---------------------------------
	PeriodicTask ();

	//- dtor ---------------------------------
	virtual ~PeriodicTask ();

  //- update R/W registers in 1 shot

  //- task configuration
  Config m_cfg;

  //- Elapsed time since last full status update
  yat::Timestamp m_last_update_ts;

  //- A counter for the so called "status expired error"
  unsigned long m_status_expired_error_counter;

  //- Flag: true if we obtained (at least) one full status
  bool m_updated_once;

  //- hw status update flag
  bool m_suspended;

  //- communication state
  ClassState class_state;

  unsigned long m_com_error;
  unsigned long m_consecutive_comm_errors;
  unsigned long m_com_success;
  
  //- consecutive command errors (not enough data), RAZ on good response
  unsigned long m_command_errors;
  
  //- reads on XPS the positions, status, velocity
  void read_hard (void);

  //- index for hardware polling
  size_t m_hw_poll_index;
  
  //- utilities to check command is running
  short cmd_running;
  yat::Timeout cmd_running_timeout;
  bool check_command_running (void);
  void start_command_running (short cmd);
  void reset_command_running (void);

  yat::Mutex com_mutex;
  yat::Mutex status_mutex;

  //- unimplemented/unsupported members
  //-----------------------------------
  PeriodicTask (const PeriodicTask &);
  PeriodicTask & operator = (const PeriodicTask &);

  //- thread state
  yat::Thread::State m_state;

};

} // namespace xps_ns

#endif // _UHR_PROXY_H_


//--------------------------------------------
//- Project : XPS Group Tango Device Server
//- XPS Positioner CPP interface
//--------------------------------------------
#include <yat/Portability.h>
#include <iostream>
#include "Group.h"
#include "Positioner.h"
#include "xps_c8_drivers.h"
#include "XPS_C8_errors.h"

const double FORCE_STATE_MOVING_TIME=100.0;
const int TRIGGER_TIMEOUT=500;
const int SGAMMA_TIMEOUT=500;
const int LIMITS_TIMEOUT=500;

namespace xps_ns
{
  //-------------------------------------------
  //- configs, enums, consts
  //-------------------------------------------
  Positioner::Limits::Limits ()
  {
     min = yat::IEEE_NAN;
     max = yat::IEEE_NAN;
  }

  //-------------------------------------------
  //- CTOR
  //-------------------------------------------
  Positioner::Positioner(std::string posit_name, int pos_number) : 
     Observable (),
     positioner_number (pos_number),
     positioner_name (posit_name),
     class_state (SW_NOT_INITIALIZED),
     position (yat::IEEE_NAN),
     tango_setpoint (yat::IEEE_NAN),
     velocity (yat::IEEE_NAN),
     accuracy(yat::IEEE_NAN),
     max_velocity (yat::IEEE_NAN),
     max_accel (yat::IEEE_NAN)
  {
     min_jerk_time = yat::IEEE_NAN;
     max_jerk_time = yat::IEEE_NAN;
     offset = 0;  		
     force_state_moving_time = FORCE_STATE_MOVING_TIME;
		
    this->full_name = Group::instance ()->group_name () + "." +  this->positioner_name;
    /*
	std::cout << "Positioner::Positioner number <" 
              << this->positioner_number 
              << "> full name <" 
              << this->full_name 
              << ">" << std::endl;
	*/
    this->class_state = SW_INITIALIZED;
  }

  //-------------------------------------------
  //- DTOR
  //-------------------------------------------
  Positioner::~Positioner(void)
  {
    //std::cout << "Positioner::~Positioner for <" << full_name << "> <-\n";
    //- Observable is called implicitely
    //- noop
  }
 //-------------------------------------------
  //-release
  //-------------------------------------------
  void Positioner::release(void)
  {
    this->class_state = SW_INITIALIZED;
  }

  //-------------------------------------------
  //- Positioner::configure
  //-------------------------------------------
  void Positioner::sw_run (void)
  {
    //std::cout << "Positioner::sw_run for <" << this->full_name << "> <-\n";
    if (class_state != Positioner::SW_INITIALIZED)
    {
      //std::cerr << "Positioner::sw_run positioner not initialized error\n";
      THROW_DEVFAILED ("SOFTWARE_FAILURE",
                       "<Positioner not initialized> error",
                       "Positioner::sw_run ()");
    }

    this->class_state = SW_NOT_CONFIGURED;

    //- timers initialization (limit acces to the HW)
    this->trigger_tmout.set_unit (yat::Timeout::TMO_UNIT_MSEC);
    this->trigger_tmout.set_value (TRIGGER_TIMEOUT);
    this->trigger_tmout.enable ();
    this->trigger_tmout.restart ();
    
    this->sgamma_tmout.set_unit (yat::Timeout::TMO_UNIT_MSEC);
    this->sgamma_tmout.set_value (SGAMMA_TIMEOUT);
    this->sgamma_tmout.enable ();
    this->sgamma_tmout.restart ();
 		
    //- get max acceleration, max velocity
    //std::cout << "Positioner::sw_run try to get vel, accel\n";
#ifndef TU_TRAJECTORY_TESTS		
   this->refresh_gamma_parameters();
            
    //- get positioner soft limits
    this->refresh_xps_min_max_limits ();

    this->class_state = SW_CONFIGURED;
  
    //- only a few axes have triggers configured so do not check errors on trigger configuration
    //std::cout << "Positioner::sw_run try to get trigger parameters\n";
    this->get_trigger_parameters ();
#endif 	
 }

	// ============================================================================
	// ACCESSORS
	// ============================================================================
  //-------------------------------------------
  //- Positioner::sw_state
  //-------------------------------------------
  xps_ns::Positioner::ClassState Positioner::sw_state (void)
  {
    //- //std::cout << "Positioner::sw_state <-\n";
    Group::ClassState cst = Group::instance ()->get_class_state ();
    if (cst != xps_ns::Group::SW_CONFIGURED)
    {
      //std::cerr << "Positioner::sw_state get_class_state returned <" << cst << ">\n";
      return Positioner::SW_CONFIGURATION_ERROR;
    }
    return class_state;
  }

  //-------------------------------------------
  //- Positioner::get_position
  //-------------------------------------------
  double Positioner::get_position (void)
  {
     return (this->position + this->offset);
  }
  //-------------------------------------------
  //- Positioner::get_xps_setpoint
  //-------------------------------------------
  double Positioner::get_xps_setpoint (void)
  {
    return xps_setpoint;
  }

  //-------------------------------------------
  //- Positioner::get_tango_setpoint
  //-------------------------------------------
  double Positioner::get_tango_setpoint (void)
  {
      return this->tango_setpoint;
  }
  //-------------------------------------------
  //- Positioner::get_offset
  //-------------------------------------------
  double Positioner::get_offset (void)
  {
    return offset;
  }
  //-------------------------------------------
  //- Positioner::get_velocity 
  //-------------------------------------------
  double Positioner::get_velocity (void)
  {
    this->refresh_gamma_parameters ();
    return max_velocity;

  }


  //-------------------------------------------
  //- Positioner::get_gamma_parameters
  //-------------------------------------------
  void Positioner::get_gamma_parameters (double & max_vel, 
                                         double & max_accel, 
                                         double & min_jerk_tm, 
                                         double & max_jerk_tm)
  {
     // Read HW
     refresh_gamma_parameters ();
     max_vel = this->max_velocity;
     max_accel = this->max_accel;
     min_jerk_tm = this->min_jerk_time;
     max_jerk_tm = this->max_jerk_time;
     
  }

  //-------------------------------------------
  //- Positioner::refresh_gamma_parameters
  //-------------------------------------------
  void Positioner::refresh_gamma_parameters ()
  {
    if (this->sgamma_tmout.expired () || yat::is_nan (this->max_velocity))
    {
      this->sgamma_tmout.restart ();

      int s_n_b = SocketPool::instance()->get_non_blocking_socket();
      int ret = PositionerSGammaParametersGet (s_n_b,
                                               const_cast <char*> (full_name.c_str()),
                                               &this->max_velocity,
                                               &this->max_accel,
                                               &this->min_jerk_time,
                                               &this->max_jerk_time);
      if (ret)
      {
        //std::cerr << "Positioner::refresh_gamma_parameters Error <" << ret << "> trying to execute PositionerSGammaParametersGet" << std::endl;
      }
      throw_on_error ("Positioner::refresh_gamma_parameters::PositionerSGammaParametersGet", ret);
        }
       return;
  }

  //-------------------------------------------
  //- Positioner::get_acceleration
  //-------------------------------------------
  double Positioner::get_acceleration (void)
  {
    this->refresh_gamma_parameters ();
    return this->max_accel;
  }

  //-------------------------------------------
  //- Positioner::refresh_xps_min_max_limits
  //-------------------------------------------
  void Positioner::refresh_xps_min_max_limits ()
  {

      double mini = yat::IEEE_NAN;
      double maxi = yat::IEEE_NAN;
      int s_n_b = SocketPool::instance()->get_non_blocking_socket();
      int ret = PositionerUserTravelLimitsGet (s_n_b,
                                               const_cast <char*> (full_name.c_str()),
                                               & mini,
                                               & maxi);
      
		//- 09/10/2013 do not assign NAN to limits as the last ones are perhaps still valid? 
		//- this->limits.xps.min = this->limits.xps.max = yat::IEEE_NAN;
		throw_on_error (std::string ("Positioner::refresh_xps_min_max_limits::PositionerUserTravelLimitsGet"), ret);

		//- update class member
		this->limits.xps.min = mini + this->offset;
		this->limits.xps.max = maxi + this->offset;
		this->limits.tango.min = limits.xps.min;
		this->limits.tango.max = limits.xps.max;
  }
 
  //-------------------------------------------
  //- Positioner::get_firmware_revision
  //-------------------------------------------
  std::string Positioner::get_firmware_revision (void)
  {
    //- //std::cout << "Positioner::get_firmware_revision <-\n";
    char firm_rev [1024];
    int s_n_b = SocketPool::instance()->get_non_blocking_socket();

    int ret = FirmwareVersionGet (s_n_b, firm_rev);
    throw_on_error (std::string ("Positioner::get_firmware_revision::FirmwareVersionGet"), ret);
    return std::string (firm_rev);
  }
  // TANGODEVIC-1375:
  //-------------------------------------------
  //- Positioner::get_accuracy
  //-------------------------------------------
  double Positioner::get_accuracy (void)
  {
     return accuracy;
  }
  
  // ============================================================================
	// Positioner::Trigger stuff 
	// ============================================================================
  //-------------------------------------------
  //- Positioner::is_trigger_enabled
  //-------------------------------------------
  bool Positioner::is_trigger_enabled (void)
  {
    //- //std::cout << "Positioner::is_trigger_enabled for <" << full_name << "> <-\n";
    if (class_state != SW_CONFIGURED)
      return false;
    this->get_trigger_parameters ();
    return trigger_conf.enabled;
  }
  //-------------------------------------------
  //- Positioner::get_trigger_parameters
  //-------------------------------------------
  Positioner::TriggerConf & Positioner::get_trigger_parameters (void)
  {
    //- //std::cout << "Positioner::get_trigger_parameters for <" << full_name << "> <-\n";
    //- do it each (1/2 second)
    if (class_state != SW_CONFIGURED)
    {
                        trigger_conf.start_pos = yat::IEEE_NAN;
			trigger_conf.end_pos = yat::IEEE_NAN;
			trigger_conf.step = yat::IEEE_NAN;
			trigger_conf.pulse_width = yat::IEEE_NAN;
			trigger_conf.encoder_settling_time = yat::IEEE_NAN;
			trigger_conf.enabled = false;
      return trigger_conf;
    }
    if (!this->trigger_tmout.expired ())
      return trigger_conf;
    this->trigger_tmout.restart ();
    
    int s_n_b = SocketPool::instance()->get_non_blocking_socket();
    int ret = PositionerPositionCompareGet (s_n_b,
                                            const_cast <char*> (full_name.c_str()),
                                            & trigger_conf.start_pos,
                                            & trigger_conf.end_pos,
                                            & trigger_conf.step,
                                            & trigger_conf.enabled);
    if (ret != 0)
    {
      //- //std::cerr << "Positioner::get_trigger_parameters <" << full_name << "> PositionerPositionCompareGet failed with err code <" << ret << ">\n";
      trigger_conf.enabled = false;
      return trigger_conf;

    }
    //- pas d'exception car sinon le configure du positioner echoue
    //-    throw_on_error (std::string ("Positioner::get_trigger_parameters::PositionerPositionCompareGet"), ret);
    //- pulse parameters:
    //- pulse_width must be in [0.2|1|2.5|10] es
    //- encoder settling time must be in [0.075|1|4|12] es
    s_n_b = SocketPool::instance()->get_non_blocking_socket();
    ret = PositionerPositionComparePulseParametersGet (s_n_b, 
                                                       const_cast <char *> (full_name.c_str ()),
                                                       & trigger_conf.pulse_width,
                                                       & trigger_conf.encoder_settling_time);
//-    throw_on_error (std::string ("Positioner::get_trigger_parameters::PositionerPositionComparePulseParametersGet"), ret);
    if (ret != 0)
      //std::cerr << "Positioner::get_trigger_parameters <" << full_name << "> PositionerPositionComparePulseParametersGet failed with err code <" << ret << ">\n";
    return trigger_conf;
  }


// ============================================================================
// MUTATORS
// ============================================================================
  //-------------------------------------------
  //- Positioner::set_position
  //-------------------------------------------
  void Positioner::set_position (double pos)
  {
//    //std::cout << "Positioner::set_position for positioner <" << full_name << "> setpoint <" << pos <<"> <-\n";

    if (class_state != SW_CONFIGURED)
    {
      THROW_DEVFAILED ("OPERATION_NOT_ALLOWED",
        "Positioner initialisation failed [check properties, restart device]",
                       "Positioner::set_position");
    }


    //- update internal values
    if (Group::instance ()->is_moving ())
    {
      std::stringstream s;
      s <<  " The group owning the positioner <" << full_name << "> is already moving\n";
      THROW_DEVFAILED ("OPERATION_NOT_ALLOWED",
                       s.str ().c_str (),
                       "Positioner::set_position");
    }
    
    tango_setpoint = pos;
    double tmp = pos - offset;
   		
   //- check the soft limits
   if ((tmp < this->limits.xps.min) || (tmp > this->limits.xps.max))
    {
     THROW_DEVFAILED ("OUT_OF_RANGE",
                       "Position out of soft limits ",
                       "Positioner::set_position");
    }
    
//cout << "Positioner::set_position , force_state_moving_time= " << force_state_moving_time << endl;

    Group::instance ()->force_state_moving ();
    int s_b = SocketPool::instance()->get_blocking_socket();
    int ret = GroupMoveAbsolute (s_b,
                                 const_cast <char*> (full_name.c_str()),
                                 static_cast <int> (1),
                                 &tmp);
    throw_on_error (std::string ("Positioner::set_position::GroupMoveAbsolute"), ret);
  }
  //-------------------------------------------
  //- Positioner::move_relative
  //-------------------------------------------
  void Positioner::move_relative (double inc)
  {
    //std::cout << "Positioner::move_relative <-\n";
    if (class_state != SW_CONFIGURED)
    {
      THROW_DEVFAILED ("OPERATION_NOT_ALLOWED",
        "Positioner initialisation failed [check properties, restart device]",
                       "Positioner::move_relative");
    }
        // Compute new position and move
    double tmp = ((this->get_position () + inc) - this->get_offset ());

    set_position (tmp);
    
  }
  //-------------------------------------------
  //- Positioner::set_setpoint
  //-------------------------------------------
  void Positioner::set_tango_setpoint (double setp)
  {
    tango_setpoint = setp;
  }
  //-------------------------------------------
  //- Positioner::set_offset
  //-------------------------------------------
  void Positioner::set_offset (double offs)
  {
    double previous_offset =offset; 
    this->offset = offs;

 // Modify Tango limits which take into account offset TANGODEVIC-845

     limits.tango.min = limits.tango.min    + (offset  - previous_offset); 
     limits.tango.max = limits.tango.max    + (offset - previous_offset);
	
    // Notify all our observers
#ifndef TU_TRAJECTORY_TESTS	
		this->Notify ();
#endif
  }
  //-------------------------------------------
  //- Positioner::set_velocity
  //-------------------------------------------
  void Positioner::set_velocity (double vel)
  {
    //std::cout << "Positioner::set_velocity <-\n";
    if (class_state != SW_CONFIGURED)
    {
      THROW_DEVFAILED ("OPERATION_NOT_ALLOWED",
        "Positioner initialisation failed [check properties, restart device]",
                       "Positioner::set_velocity");
    }
    double veloc = vel;
    //- get max acceleration, max velocity
    int s_n_b = SocketPool::instance()->get_non_blocking_socket();
    int ret = PositionerSGammaParametersGet (s_n_b,
                                             const_cast <char*> (full_name.c_str()),
                                             &max_velocity,
                                             &max_accel,
                                             &min_jerk_time,
                                             &max_jerk_time);
    throw_on_error ("Positioner::set_velocity::PositionerSGammaParametersGet", ret);
    s_n_b = SocketPool::instance()->get_non_blocking_socket();
    ret = PositionerSGammaParametersSet (s_n_b,
                                         const_cast <char*> (full_name.c_str()),
                                         veloc,
                                         max_accel,
                                         min_jerk_time,
                                         max_jerk_time);
    throw_on_error ("Positioner::set_velocity::PositionerSGammaParametersSet", ret);
    //- read back to store the new values in the class members
    s_n_b = SocketPool::instance()->get_non_blocking_socket();
    ret = PositionerSGammaParametersGet (s_n_b,
                                             const_cast <char*> (full_name.c_str()),
                                             &max_velocity,
                                             &max_accel,
                                             &min_jerk_time,
                                             &max_jerk_time);
    throw_on_error ("Positioner::set_velocity::PositionerSGammaParametersGet", ret);
	
	// Notify XPSAxis to store this value in a memorized attribut
	Notify();
	
    return;
  }
  //-------------------------------------------
  //- Positioner::set_acceleration
  //-------------------------------------------
  void Positioner::set_acceleration (double acc)
  {
    //std::cout << "Positioner::set_acceleration <-\n";
    if (class_state != SW_CONFIGURED)
    {
      THROW_DEVFAILED ("OPERATION_NOT_ALLOWED",
        "Positioner initialisation failed [check properties, restart device]",
                       "Positioner::set_acceleration");
    }
    double accel = acc;
    //- get max acceleration, max velocity
    int s_n_b = SocketPool::instance()->get_non_blocking_socket();
    int ret = PositionerSGammaParametersGet (s_n_b,
                                             const_cast <char*> (full_name.c_str()),
                                             &max_velocity,
                                             &max_accel,
                                             &min_jerk_time,
                                             &max_jerk_time);
    throw_on_error ("Positioner::set_acceleration::PositionerSGammaParametersGet", ret);
    s_n_b = SocketPool::instance()->get_non_blocking_socket();
    ret = PositionerSGammaParametersSet (s_n_b,
                                         const_cast <char*> (full_name.c_str()),
                                         max_velocity,
                                         accel,
                                         min_jerk_time,
                                         max_jerk_time);
    throw_on_error ("Positioner::set_acceleration::PositionerSGammaParametersSet", ret);
    //- read back to store the new values in the class members
    s_n_b = SocketPool::instance()->get_non_blocking_socket();
    ret = PositionerSGammaParametersGet (s_n_b,
                                             const_cast <char*> (full_name.c_str()),
                                             &max_velocity,
                                             &max_accel,
                                             &min_jerk_time,
                                             &max_jerk_time);
    throw_on_error ("Positioner::set_acceleration::PositionerSGammaParametersGet", ret);
    return;
  }

  //-------------------------------------------
  //- Positioner::set_xps_min_max_limits
  //- provided : values without tango offset
  //-------------------------------------------
  void Positioner::set_xps_min_max_limits (double min, double max)
  {
    
 //  	std::cout << "Positioner::set_xps_min_max_limits  for <" << full_name << ">"
 //             << " values [" << min << " : " << max << "] <-\n";

    if (class_state != SW_CONFIGURED)
    {
      THROW_DEVFAILED ("OPERATION_NOT_ALLOWED",
                       "Positioner initialisation failed [check properties, restart device]",
                       "Positioner::set_xps_min_max_limits");
    }
    if (yat::is_nan (min) || yat::is_nan (max))
    {
      //std::cerr << "Positioner::set_xps_min_max_limits [" << min << ":" << max << "] found 1 NAN value, abandon" << std::endl; 
      return;
    }
      
    //- ecrire dans le xps 
    int s_n_b = SocketPool::instance()->get_non_blocking_socket();
    int ret = PositionerUserTravelLimitsSet (s_n_b,
                                             const_cast <char*> (full_name.c_str()),
                                             (min),
                                             (max));

    throw_on_error ("Positioner::set_xps_min_max_limits::PositionerUserTravelLimitsSet", ret);

    //- refresh the internal value with real XPS readback values
    this->refresh_xps_min_max_limits ();
   }

  //-------------------------------------------
  //- Positioner::set_tango_min_max_limits
  //-------------------------------------------
  void Positioner::set_tango_min_max_limits (double min, double max)
  {
   //std::cout << "Positioner::set_tango_min_max_limits  for <" << full_name << ">" << " values [" << min << " : " << max << "] <-\n";

    if (class_state != SW_CONFIGURED)
    {
      THROW_DEVFAILED ("OPERATION_NOT_ALLOWED",
                       "Positioner initialisation failed [check properties, restart device]",
                       "Positioner::set_tango_min_max_limits");
    }
    // JIRA TANGODEVIC-928
	double min_to_set=0;
	double max_to_set=0;
	
    //- copy values locally
    if (! yat::is_nan (min))
      min_to_set = min;
    else
      min_to_set = this->limits.xps.min+ this->offset;  // JIRA TANGODEVIC-983
      
    if (! yat::is_nan (max))
      max_to_set = max;
    else
      max_to_set = this->limits.xps.max+ this->offset ; // JIRA TANGODEVIC-983
      
    
          
   //- try to write the limits in the controller (exception if error or out of range)
 //  std::cout << "\t\tPositioner::set_tango_min_max_limits : Calling set_xps_min_max_limits : min_to_set=" << min_to_set << ">" << ", max_to_set=" << max_to_set << "<-\n";
     this->set_xps_min_max_limits ((min_to_set - this->offset), (max_to_set- this->offset));
	 //
    //- store values locally if they have been successfully writen in the XPS
      this->limits.tango.min = min_to_set;
      this->limits.tango.max = max_to_set;

	  // Notify all our observers that an important data has changed so that the Tango class (XPSAxis and XPSGroup) can potentially store new values in memorised attributes 
		 // See JIRA SPYC-62
    this->Notify ();

  }

  //-------------------------------------------
  //- Positioner::restore_tango_min_max_limits
  //-------------------------------------------
  void Positioner::restore_tango_min_max_limits (void)
  {
    /*
	std::cout << "Positioner::restore_tango_min_max_limits  for <" 
              << full_name << ">" 
              << " values [" << this->limits.tango.min << " : " 
              << this->limits.tango.max << "] <-\n";
	*/
    if (class_state != SW_CONFIGURED)
    {
      THROW_DEVFAILED ("OPERATION_NOT_ALLOWED",
                       "Positioner initialisation failed [check properties, restart device]",
                       "Positioner::set_tango_min_max_limits");
    }
          
   //- try to write the limits in the controller (exception if error or out of range)
   this->set_xps_min_max_limits ((this->limits.tango.min - this->offset), (this->limits.tango.max - this->offset));
   // 
  }


  // ============================================================================
	// Positioner::Trigger stuff
	// ============================================================================
  //-------------------------------------------
  //- Positioner::trigger_enable
  //-------------------------------------------
  void Positioner::trigger_enable (void)
  {
    //std::cout << "Positioner::trigger_enable  for <" << full_name << "> <-\n";
    if (class_state != SW_CONFIGURED)
    {
      THROW_DEVFAILED ("OPERATION_NOT_ALLOWED",
        "Positioner initialisation failed [check properties, restart device]",
                       "Positioner::trigger_enable");
    }
    int s_b = SocketPool::instance()->get_blocking_socket();
    int ret = PositionerPositionCompareEnable (s_b,
                                               const_cast <char *> (full_name.c_str ()));
    throw_on_error (std::string ("Positioner::trigger_enable::PositionerPositionCompareEnable"), ret);
  }

  //-------------------------------------------
  //- Positioner::trigger_disable
  //-------------------------------------------
  void Positioner::trigger_disable (void)
  {
		//std::cout << "Positioner::trigger_disable <-" << std::endl;
    if (class_state != SW_CONFIGURED)
    {
      THROW_DEVFAILED ("OPERATION_NOT_ALLOWED",
        "Positioner initialisation failed [check properties, restart device]",
                       "Positioner::trigger_disable");
    }
    int s_b = SocketPool::instance()->get_blocking_socket();
    int ret = PositionerPositionCompareDisable (s_b,
                                                const_cast <char *> (full_name.c_str ()));
    throw_on_error (std::string ("Positioner::trigger_disable::PositionerPositionCompareDisable"), ret);
  }

  //-------------------------------------------
  //- Positioner::set_trigger_parameters
  //-------------------------------------------
  void Positioner::set_trigger_parameters (TriggerConf trg_conf)
  {
		//std::cout << "Positioner::set_trigger_parameters <-" << std::endl;
    if (class_state != SW_CONFIGURED)
    {
      THROW_DEVFAILED ("OPERATION_NOT_ALLOWED",
        "Positioner initialisation failed [check properties, restart device]",
                       "Positioner::set_trigger_parameters");
    }
    //- pulse parameters:
    //- pulse_width must be in [0.2|1|2.5|10] es
    if ((trg_conf.pulse_width != 0.2) &&
        (trg_conf.pulse_width != 1.0) &&
        (trg_conf.pulse_width != 2.5) &&
        (trg_conf.pulse_width != 10.))
    {
      //std::cerr << "Positioner::set_trigger_parameters positioner OUT_OF_RANGE pulse width parameters valid values [0.2|1|2.5|10] \n";
      THROW_DEVFAILED ("OUT_OF_RANGE",
                       " pulse width parameters valid values [0.2|1.0|2.5|10.0]",
                       "Positioner::set_trigger_parameters ()");
    }
    //- encoder settling time must be in [0.075|1|4|12] es
    if ((trg_conf.encoder_settling_time != 0.075) &&
        (trg_conf.encoder_settling_time != 1.) &&
        (trg_conf.encoder_settling_time != 4.) &&
        (trg_conf.encoder_settling_time != 12.))
    {
      //std::cerr << "Positioner::set_trigger_parameters positioner OUT_OF_RANGE encoder settling time valid values [0.075|1.0|4.0|12.0] \n";
      THROW_DEVFAILED ("OUT_OF_RANGE",
                       "encoder settling time valid values [0.075|1.0|4.0|12.0]",
                       "Positioner::set_trigger_parameters ()");
    }
    int s_b = SocketPool::instance()->get_blocking_socket();
    int ret = PositionerPositionCompareSet (s_b, 
                                            const_cast <char *> (full_name.c_str ()), 
                                            trg_conf.start_pos, 
                                            trg_conf.end_pos,
                                            trg_conf.step);
    throw_on_error (std::string ("Positioner::set_trigger_parameters::PositionerPositionCompareSet"), ret);
    //- pulse parameters
    
    //- sleep 10 ms 
    omni_thread::sleep (0, 10000000);
    //- TODO : check arguments

    s_b = SocketPool::instance()->get_blocking_socket();
    ret = PositionerPositionComparePulseParametersSet (s_b, 
                                                       const_cast <char *> (full_name.c_str ()),
                                                       trg_conf.pulse_width,
                                                       trg_conf.encoder_settling_time);
    throw_on_error (std::string ("Positioner::set_trigger_parameters::PositionerPositionComparePulseParametersSet"), ret);
  }


	// ============================================================================
	// Positioner::throw_on_error
	// ============================================================================
  void Positioner::throw_on_error (std::string origin, int return_code) 
  {
#ifndef TU_TRAJECTORY_TESTS   
    //- //std::cout << "Positioner::throw_on_error <-" << std::endl;
    if (return_code == 0)
      return;
    //std::cerr << "Positioner::throw_on_error origin <" << origin << " err code <" << return_code << ">"  << std::endl;

    int s_n_b = SocketPool::instance()->get_non_blocking_socket();
    int c = ErrorStringGet (s_n_b, return_code, this->ret_code_string);
    if (c != 0)
    {
      //std::cerr << "Positioner::throw_on_error ErrorStringGet failed with code <" << c << ">"  << std::endl;
      THROW_DEVFAILED ("COMMUNICATION_ERROR",
                       "error code returned by ErrorStringGet [call soft support]",
                       "Positioner::throw_on_error");
    }
    //std::cerr << "Positioner::throw_on_error command refused with description <" << this->ret_code_string << ">"  << std::endl;
    THROW_DEVFAILED("COMMAND_ERROR",
                    ret_code_string,
                    origin.c_str ());
#endif					
  }
  
//- TANGODEVIC-57:
//-------------------
// ============================================================================
// Positioner::enable_backlash
// ============================================================================
void Positioner::enable_backlash()
{
	double backlashValue = 0.;
	char strStatus[32];

	int s_n_b = SocketPool::instance()->get_non_blocking_socket();
	int ret  = PositionerBacklashGet (	s_n_b, 
										const_cast <char*> (full_name.c_str()),
										&backlashValue, 
										strStatus); 
	throw_on_error (std::string ("Positioner::enable_backlash::PositionerBacklashGet"), ret);

    std::string stringStatus = strStatus;
    
	if (stringStatus.find("Disable") != std::string::npos)
	{
		s_n_b = SocketPool::instance()->get_non_blocking_socket();
		ret = PositionerBacklashEnable(s_n_b,
									   const_cast <char*> (full_name.c_str()) );
		throw_on_error (std::string ("Positioner::enable_backlash::PositionerBacklashEnable"), ret);
	}	
}
// ============================================================================
// Positioner::disable_backlash
// ============================================================================
void Positioner::disable_backlash()
{
	double backlashValue = 0.;
	char strStatus[32];

	int s_n_b = SocketPool::instance()->get_non_blocking_socket();
	int ret  = PositionerBacklashGet (	s_n_b, 
										const_cast <char*> (full_name.c_str()),
										&backlashValue, 
										strStatus); 
	throw_on_error (std::string ("Positioner::enable_backlash::PositionerBacklashGet"), ret);
	
    std::string stringStatus = strStatus;
    
	if (stringStatus.find("Enable") != std::string::npos)
	{
		s_n_b = SocketPool::instance()->get_non_blocking_socket();
		ret = PositionerBacklashDisable(s_n_b,
									   const_cast <char*> (full_name.c_str()) );
		throw_on_error (std::string ("Positioner::disable_backlash::PositionerBacklashDisable"), ret);
	}	
}
//- TANGODEVIC-1375
//-------------------------------------------
//- Positioner::set_accuracy
//-------------------------------------------
void Positioner::set_accuracy (double accu)
{
  accuracy = accu;

  // Notify all observers that data has changed
  this->Notify ();
}
// ============================================================================
// Positioner::send_low_level_cmd
// ============================================================================
void Positioner::exec_low_level_cmd(std::string cmd, std::string &outParam)
{
  char strOut[65535];

  // use non blocking socket to get an answer
  int s_b = SocketPool::instance()->get_non_blocking_socket();
  int ret = ExecLowLevelCmd(s_b,
      const_cast <char*> (cmd.c_str()),
      &strOut[0]);

  throw_on_error(std::string("Positioner::exec_low_level_cmd::ExecLowLevelCmd"), ret);

  outParam = std::string(strOut);
}

}//- namespace

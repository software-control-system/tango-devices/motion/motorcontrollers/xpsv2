//--------------------------------------------
//- Project XPS Group Tango Device Server
//- XPS Box global control and global functions
//- First instanciated object
//- creates the SocketPool
//--------------------------------------------
#ifndef __GROUP__H__
#define __GROUP__H__

#include <vector>
#include <string>
#include "Positioner.h"
#include "Trajectory.h"
#include "SocketPool.h"
#include "PeriodicTask.h"
#include "GroupStateStr.h"

const unsigned int MAX_NUMBER_OF_POSITIONERS= 8;
const unsigned int MAX_ERROR_CODE_STRING_LENGTH=512;

namespace xps_ns
{
	// ============================================================================
	// CONSTs
	// ============================================================================

	static const std::string class_states_str [8] =
	{
		"Class Unknown State\n",
		"Class Not Initialized\n",
		"Class Initialized\n",
		"Class Initialization Error\n",
		"Class Not Configured\n",
		"Class Allocation Error\n",
		"Class Configured\n",
		"Class Configuration Error\n"
	};

	static const std::string group_states_str [8] =
	{
		"Group Unknown State\n",
		"Group Not Initialized\n",
		"Group Initializing\n",
		"Group Ready\n",
		"Group Disabled\n",
		"Group Emergency Brake\n",
		"Group Moving\n",
		"Group Running\n"
	};

	//-------------------------------------------
	//- Class Group definition 
	//-------------------------------------------
	class Group
	{
	public :
		//-------------------------------------------
		//- configs, enums, consts
		//-------------------------------------------
		typedef enum
		{
			SW_UNKNOWN_STATE,
			SW_NOT_INITIALIZED,
			SW_INITIALIZED,
			SW_INITIALIZATION_ERROR,
			SW_NOT_CONFIGURED,
			SW_ALLOC_ERROR,
			SW_CONFIGURED,
			SW_CONFIGURATION_ERROR
		}ClassState;
		//-------------------------------------------
		typedef enum
		{
			GRP_UNKNOWN,
			GRP_NOT_INIT,
			GRP_INITIALIZING,
			GRP_READY,
			GRP_DISABLED,
			GRP_EMERGENCY_BRAKE,
			GRP_MOVING,
			GRP_RUNNING
		}GroupState;
		//-------------------------------------------
		typedef struct Config
		{
			std::string group_name;
			std::vector <std::string> positioner_names;
			std::vector <std::string> positioner_tango_attribute_names;  // used only to return ordered list of axis 

			long group_moving_timer_tmout_ms;
			Config ();
			Config (const Config & conf);
			//- operator = -------
			Config & operator = (const Config & src);
		} Config;


		//-------------------------------------------
		//- Configuration and Initialization
		//-------------------------------------------
		// construct objects without connection to hardware
		void initialize (const SocketPool::Config & ip_conf, const Group::Config & grp_conf, const PeriodicTask::Config & thread_conf);

		void sw_run (void);

		//-------------------------------------------
		//- Singleton : retourne un pointeur sur l'instance
		//-------------------------------------------
		static Group * instance (void)
		{
			if (group == 0)
				group = new Group ();
			return group;
		}
		//-------------------------------------------
		//- Singleton : detruire le Group
		//-------------------------------------------
		void release (void) ;
		//-------------------------------------------
		//- returns the SW state
		//-------------------------------------------
		ClassState get_class_state (void) const
		{
			return this->internal_state;
		}
		std::string get_class_status_str (void) 
		{ 
        if (internal_state < SW_UNKNOWN_STATE || internal_state > SW_CONFIGURATION_ERROR) 
            internal_state = SW_UNKNOWN_STATE; 
			return class_states_str [internal_state];
		}

		//- get cooked group state (simplified from real numerical XPS Group "State" 
		GroupState get_group_state (void);
    
		std::string get_group_status_str (void) 
		{
      if (xps_group_state < 0 || xps_group_state >= MAX_XPS_GROUP_STATES_STRINGS) 
        xps_group_state = MAX_XPS_GROUP_STATES_STRINGS - 1; 
      return hw_group_states_str [xps_group_state];
		}	
		//- 
		bool is_moving (void)  
		{
			return (get_group_state () == GRP_MOVING);
		}

		//- force state moving (for scanserver)
		void force_state_moving ();

		//-------------------------------------------
		//- useful methods
		//-------------------------------------------
		std::string group_name (void);
		std::vector <std::string> positioner_tango_attribute_names;  // used only to return ordered list of axis 
		Positioner * get_positioner (std::string name) const;
        std::vector<std::string> get_ordered_positioners_list();


		class Trajectory * get_trajectory (void);	
	//-------------------------------------------
		//- Group API related commands
		//-------------------------------------------
		void start ();          // start moving on all positionners of the group
		void stop () ;          // 
		void kill () ;          // 
		void on ();
		void off ();
		void initialize_reference_position ();
		void go_to_positions (std::vector <double> positions);
		void define_positions (std::vector <double> positions);
        std::string exec_low_level_cmd(std::string cmd);

    //- Yves Garreau demands --- To be used only with Dynamic Attributes write!!!
    //- DO NOT USE IT FOR ANY PURPOSE    
    void set_tango_setpoints_internal (double src, int positioner_number);
    double get_tango_setpoints_internal (int positioner_number);

    unsigned long get_periodic_task_com_error() { return PeriodicTask::instance ()->get_com_error();};

		//-------------------------------------------
		//- retrieve Group API related data for PeriodicTask class
		//-------------------------------------------
		friend class PeriodicTask;  // The PeriodicTask requires access to some methods
	private:
		void hw_get_positions (void);
		void hw_get_xps_setpoints (void);
		void hw_get_min_max_limits (void);
		void hw_get_group_state (void);
		//- called periodically by PeriodicTask
		void synchronise_tango_setpoints (void);

		friend class Trajectory ;  // 
		//-------------------------------------------
		//- for Positioner access to current position, current velocity, current xps setpoint
		//-------------------------------------------
	private :
		//- return the raw XPS Group State
		int get_xps_group_state (void) const
		{
			return xps_group_state;
		}	
		//- pointer on singleton
		static Group * group;
		//- CTOR
		Group ();
		//- DTOR
		virtual ~Group ();
		//- internal state
		ClassState internal_state;
		GroupState group_state;

		//- request for move abort done
		GroupState previous_group_state;

		//- To force state to be moving after a position write
		yat::Timeout tmov;
		yat::Timeout tinitialize;

		//- status
		std::string status_str;
		int xps_group_state;

		void go_to_pos_internal (void);
		void throw_on_error (std::string origin, int ret_code);

		char ret_code_string [MAX_ERROR_CODE_STRING_LENGTH];

		//- GROUP configuration data
		Config conf;

		//- the positioners vector
		std::vector <Positioner *> positioners;

		//- the trajectory class
		Trajectory * trajectory;
		
		//- used to request a recopy of real positions in setpoints
		//- mainly for Stop command and after trajectory has ended
		//- used to avoid collisions
		void request_for_recopy_read_in_write (void) { m_request_for_recopy_read_in_write = true;};
		bool m_request_for_recopy_read_in_write;
    
    
    //- pour les demandes d'yves garreau
	//- JGO : What are they????
    struct tango_setp
    {
      bool new_setpoint_present;
      double sp;
      tango_setp () : new_setpoint_present (false), sp (yat::IEEE_NAN)
      {
        //- NOOP
      };
    };
    struct tango_setp tango_setpoints_internal [MAX_NUMBER_OF_POSITIONERS];
    
	};

}//- namespace

#endif //- __GROUP__H__

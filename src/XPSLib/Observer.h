
#ifndef __OBSERVER_H
#define __OBSERVER_H

#include <iostream>
#include <list>
#include <iterator>
#include <algorithm>
#include <string>


namespace xps_ns
{
typedef double Info;

class Observable;
//*******************************************************************
//
//		Class Observer : Read Pattern Description 
//
//	http://en.wikipedia.org/wiki/Observer_pattern
//
//*******************************************************************

class Observer
{
protected:
  //- ce n'est pas necessaire  pour nous
  std::list<Observable*> m_list;
  typedef std::list<Observable*>::iterator iterator; 
  typedef std::list<Observable*>::const_iterator const_iterator;
  virtual ~Observer() = 0;
public:
  Observer ()
  {
  };
  virtual void Update() = 0;
  void AddObs(Observable* obs);
  void DelObs(Observable* obs);
};
//*******************************************************************
//
//		Class Observable : Read Pattern Description 
//
//	http://en.wikipedia.org/wiki/Observer_pattern
//
//*******************************************************************

class Observable
{
  std::list<Observer*> m_list;
  typedef std::list<Observer*>::iterator iterator; 
  typedef std::list<Observer*>::const_iterator const_iterator;

public:
  Observable ()
  {
  };
   void AddObs (Observer* obs);
   void DelObs (Observer* obs);
   virtual ~Observable();
protected:
   void Notify (void);
private : 

};

} //- namespace


#endif //- __OBSERVER_H



//=============================================================================
// PeriodicTask.cpp
//=============================================================================
// abstraction.......XPS communication class/thread
// class.............PeriodicTask
// simple thread that calls Group class methods to periodically refresh the HW
// original author...J.Coquet d'apres projet ControlBox : N.Leclercq - SOLEIL
//=============================================================================

// ============================================================================
// DEPENDENCIES
// ============================================================================
#include <tango.h>
#include <yat/utils/XString.h>
#include "PeriodicTask.h"
#include "Group.h"
#include "SocketPool.h"

// ============================================================================
// SOME CONSTs
// ============================================================================
//-----------------------------------------------------------------------------
//-----------------------------------------------------------------------------

namespace xps_ns  
{
// ============================================================================
// SINGLETON
// ============================================================================
PeriodicTask * PeriodicTask::singleton = 0;
// ============================================================================
// STATIC MEMBERS
// ============================================================================

// ============================================================================
// PeriodicTask::Config::Config
// ============================================================================
PeriodicTask::Config::Config () :
  period_ms (500),
  startup_timeout_ms (5000),
  status_expiration_timeout_ms (1000)
{
  //- noop ctor
}

// ============================================================================
// PeriodicTask::Config::operator=
// ============================================================================
PeriodicTask::Config& PeriodicTask::Config::operator = (const Config& src)
{
  period_ms = src.period_ms;
  startup_timeout_ms = src.startup_timeout_ms;
  status_expiration_timeout_ms = src.status_expiration_timeout_ms;

  return *this;
}


// ============================================================================
// PeriodicTask::init <STATIC MEMBER>
// ============================================================================
PeriodicTask * PeriodicTask::instance ()	throw (Tango::DevFailed)
{
  YAT_TRACE_STATIC("xps_ns::PeriodicTask::init");

  //- already instanciated?
  if (PeriodicTask::singleton)
    return PeriodicTask::singleton;

  try
  {
    //- instanciate
    PeriodicTask::singleton = new PeriodicTask();
    if (PeriodicTask::singleton == 0)
      throw std::bad_alloc();
  }
  catch (const std::bad_alloc&)
  {
    THROW_DEVFAILED(_CPTC("OUT_OF_MEMORY"),
		_CPTC("xps_ns::PeriodicTask::singleton allocation failed"),
		_CPTC("xps_ns::PeriodicTask::init")); 
  }
  catch (Tango::DevFailed& df)
  {
    RETHROW_DEVFAILED(df,
		_CPTC("SOFTWARE_ERROR"),
		_CPTC("xps_ns::PeriodicTask::singleton initialisation failed"),
		_CPTC("xps_ns::PeriodicTask::init")); 
  }
  catch (const yat::Exception& ye)
  {		
    _YAT_TO_TANGO_EXCEPTION(ye, te);
    throw te;  
  }
  catch (...)
  {
    THROW_DEVFAILED(_CPTC("UNKNOWN_ERROR"),
		_CPTC("xps_ns::PeriodicTask::singleton initialisation failed [unknown exception]"),
		_CPTC("xps_ns::PeriodicTask::init")); 
  }

  return NULL; // for compil warning (should not be here)
}

// ============================================================================
// PeriodicTask::close  <STATIC MEMBER>
// ============================================================================
void PeriodicTask::close ()
	throw (Tango::DevFailed)
{
	YAT_TRACE_STATIC("xps_ns::PeriodicTask::close");
  
	try
	{
		//- check
		if (! PeriodicTask::singleton)
			return;
 
		//- ask the underlying yat::Task to exit
		PeriodicTask::singleton->exit();

		//- reset singleton 
		PeriodicTask::singleton = 0; 

		//- NEVER CALL DELETE ON <PeriodicTask::singleton>
		//- see yat::Task impl for more info...
	}
	_HANDLE_YAT_EXCEPTION("yat::Task::exit", "PeriodicTask::close"); 
}

// ============================================================================
// PeriodicTask::PeriodicTask
// ============================================================================
PeriodicTask::PeriodicTask ()
	: m_cfg (), 
    m_status_expired_error_counter (0),
		m_updated_once (false),
		m_suspended (false),
    m_com_error (0),
	//- TANGODEVIC-1197:
    // m_consecutive_comm_errors (0),
    m_state (yat::Thread::STATE_NEW)
{
	YAT_TRACE("xps_ns::PeriodicTask::PeriodicTask");

	//- configure optional msg handling
	this->enable_timeout_msg(false);
	//- enable periodic msgs
	this->enable_periodic_msg(false);
	
}

// ============================================================================
// PeriodicTask::~PeriodicTask
// ============================================================================
PeriodicTask::~PeriodicTask ()
{
	YAT_TRACE("xps_ns::PeriodicTask::~PeriodicTask");
}

// ============================================================================
// PeriodicTask::configure
// ============================================================================
void PeriodicTask::initialize (const Config& _cfg) 
	throw (Tango::DevFailed)
{
	YAT_TRACE("xps_ns::PeriodicTask::initialize");

  
  { //- enter critical section
    //-	yat::AutoMutex<yat::Mutex> guard(this->m_lock);
    //- get thread state under critical section
    m_state = this->state(); 
    
	  //- copy configuration
	  this->m_cfg = _cfg; 

	  //- enable periodic msgs
	  this->enable_periodic_msg(true); 
	  this->set_periodic_msg_period(this->m_cfg.period_ms);

	  //- be sure <m_cfg.status_expiration_tmo_ms> is set to a <correct> value
	  if (this->m_cfg.status_expiration_timeout_ms < 2 * this->m_cfg.period_ms)
		  this->m_cfg.status_expiration_timeout_ms = 2 * this->m_cfg.period_ms;

    //- init of the last full status update timestamp
	  _GET_TIME(this->m_last_update_ts);
  } //- end of critical section
}


// ============================================================================
// PeriodicTask::start
// ============================================================================
void PeriodicTask::sw_run () throw (Tango::DevFailed)
{
	//- start the task if not running (must be done outside critical section)
	if (m_state != yat::Thread::STATE_RUNNING)
  {
    try
    {
      //- start the underlying task
		  go(m_cfg.startup_timeout_ms);
    }
    catch (yat::Exception &e)
    {
     THROW_YAT_TO_TANGO_EXCEPTION(e);
    }
  }
}

// ============================================================================
// PeriodicTask::handle_message
// ============================================================================
void PeriodicTask::handle_message (yat::Message& _msg)
	throw (yat::Exception)
{
	//- YAT_TRACE("xps_ns::PeriodicTask::handle_message"); 

  //- enter critical section
  yat::AutoMutex<yat::Mutex> guard(this->m_lock);

	//- YAT_LOG("PeriodicTask::handle_message::receiving msg " << _msg.to_string());

	//-static yat::Timestamp last, now;
	//-_GET_TIME(now);
	//-if (_IS_VALID_TIMESTAMP(last))
	//-	YAT_LOG("PeriodicTask::handle_message::DT since last exec is " << _ELAPSED_MSEC(last, now) << " msecs");

  try
  {
	  //- handle msg
	  switch (_msg.type())
	  {
		  //- TASK_INIT ----------------------
		  case yat::TASK_INIT:
			  {
				  //- "initialization" code goes here
				  YAT_LOG("PeriodicTask::handle_message::THREAD_INIT::PeriodicTask-task is starting up");
				  //- try to get data from the controller
				  this->m_hw_poll_index = 0;

				  m_com_error = 0;
				  //- TANGODEVIC-1197:
				  // m_consecutive_comm_errors = 0;
				  m_com_success = 0;

				  this->read_hard ();
			  } 
			  break;
		  //- TASK_EXIT ----------------------
		  case yat::TASK_EXIT:
			  {
				  //- "release" code goes here
				  YAT_LOG("PeriodicTask::handle_message::THREAD_EXIT::PeriodicTask-task is quitting");
			  }
			  break;
		  //- TASK_PERIODIC ------------------
		  case yat::TASK_PERIODIC:
			  {
				//- 
				this->read_hard ();
							
				//- timestamp periodique
				yat::Timestamp ts;
				std::string date;
				_GET_TIME(ts);
				_TIMESTAMP_TO_DATE (ts, date);
				YAT_LOG( " PeriodicTask::handle_message date = " << date << std::endl);
			  }
			  break;
		  //- USER DEFINED MESSAGES ----------
			//- default ------------------------
		  default:
			  YAT_LOG("PeriodicTask::handle_message::unhandled msg type received");
        //std::cerr << "PeriodicTask::handle_message::unhandled msg type received" << std::endl;
			  break;
	  }
	}
	catch (Tango::DevFailed& te)
	{
	  YAT_LOG("PeriodicTask::handle_message::TANGO exception caught");
	  _TANGO_TO_YAT_EXCEPTION(te, ye);
#if defined (YAT_ENABLE_LOG)
	  ye.dump();
#endif
	  throw ye;  
	}
	catch (...)
	{		
	  YAT_LOG("PeriodicTask::handle_message::UNKNOWN exception caught");
	  throw;  
	}
}
// ============================================================================
// PeriodicTask::read_hard
// ============================================================================
void PeriodicTask::read_hard (void)
{
  YAT_LOG("PeriodicTask::read_hard::<-");
  //- //std::cout << "PeriodicTask::read_hard::<-" << std::endl;
  
  // if (this->m_consecutive_comm_errors > MAX_COM_RETRY)
  // {
    // std::cerr << "PeriodicTask::read_hard communication fatal error [try to restart DServer]" << std::endl;
    // return;
  // }
// Refresh all data from Controller

	try 
	{
		//- call Group positions accessor
		Group::instance ()->hw_get_positions ();
		//- call Group status accessor
		Group::instance ()->hw_get_group_state ();
		//- Synchronize tango_set_point in case of abort
		Group::instance ()->synchronise_tango_setpoints ();
		// 
		Group::instance ()->hw_get_xps_setpoints ();
		Group::instance ()->hw_get_min_max_limits () ;
	  }
	  catch (Tango::DevFailed &e)
	  {
		//std::cerr << "PeriodicTask::read_hard caught Tango::DevFailed [" << e.errors[0].desc << "]" << std::endl;
		++m_com_error;
		return;
	  }
	}
} // namespace xps_ns

//-------------------------------------------------------------------
//- project : classes for XPS control
//- file : SocketPool2 Ssocket management
//-------------------------------------------------------------------#include <iostream>

#include "SocketPool.h"
#include <yat4tango/ExceptionHelper.h>

namespace xps_ns
{
//------------------------------------------------
//- SocketItem CTOR
//------------------------------------------------
SocketItem::SocketItem (int sock_num, std::string ip_address, int port, int itmout, int otmout, bool blocking) : 
  m_socket_number (sock_num),
  m_sock (0),
  m_used (false),
  is_sock_for_blocking_cmd (blocking),
  m_com_errors (0),
  m_com_consecutive_errors (0),
  m_com_success (0),
  _ip_address(ip_address), 
  _port(port), 
  _intmout(itmout), 
  _outmout(otmout)
  {
        // std::cout << "SocketItem::SocketItem CTOR number <" << m_socket_number << "> <-" << std::endl;
	
	//- TANGODEVIC-1197:
	// First state: create the Client socket and initialize connect options
	//--------------------------------------------------------------------
	// connect to the Socket: protocol is yat::Socket::TCP_PROTOCOL	
    m_sock = new yat::ClientSocket (yat::Socket::TCP_PROTOCOL);
	
    m_sock->set_option(yat::Socket::SOCK_OPT_KEEP_ALIVE, 1);
    m_sock->set_option(yat::Socket::SOCK_OPT_NO_DELAY, 1);
 

	//- TANGODEVIC-1197:	
	//-----------------------------------------------------------
    //- sufficient time to realise the socket connection 
    m_sock->set_option(yat::Socket::SOCK_OPT_ITIMEOUT, kCONNECTION_TMO_MSECS);
    m_sock->set_option(yat::Socket::SOCK_OPT_OTIMEOUT, kCONNECTION_TMO_MSECS);
	
	is_connected = false;
     // std::cout << "SocketItem::SocketItem CTOR ended <" << m_socket_number << "> <-" << std::endl;

  }

//------------------------------------------------
//- SocketItem connect
//------------------------------------------------
void SocketItem::connect(void)
{
  //- TANGODEVIC-1197:
  // Second state: Bind and connect the socket to ip address, nb port.
  //               Because of probable Exception throw, this treatment has been moved from CTOR SockItem()	
  // std::cout << "SocketItem::SocketItem <" << m_socket_number << "> trying to connect socket : "<<  _ip_address << ", port = " << _port <<endl;
  yat::Address addr(_ip_address, _port);

  //- connect to peer (TCP) 
  m_sock->connect(addr); 

  // std::cout << "SocketItem::SocketItem <" << m_socket_number << "> connect DONE : "<<  _ip_address << ", port = " << _port <<endl;
	
  //- TANGODEVIC-1197:
  //-----------------------------------------------------------
  // sets the user in/out time out
  m_sock->set_option(yat::Socket::SOCK_OPT_ITIMEOUT, _intmout);
  m_sock->set_option(yat::Socket::SOCK_OPT_OTIMEOUT, _outmout);

  is_connected = true;
}

//------------------------------------------------
//- SocketItem DTOR
//------------------------------------------------
SocketItem::~SocketItem ()
{
  // std::cout << "--> DTOR ~SocketItem_... <" << m_socket_number << "> "<< std::endl;
  m_sock->disconnect ();

  is_connected = false;

  if (m_sock)
    delete m_sock;
  m_sock = 0;
  // std::cout << "--> DTOR SocketItem ended " << std::endl;
}

//------------------------------------------------
//- SocketItem send_recv
//------------------------------------------------
void SocketItem::send_recv (char sSendString[], char sReturnString[], int iReturnStringSize)
{
  //- //std::cout << "SocketItem::send_recv for socket index <" << m_socket_number << "> command <" << sSendString << ">" << std::endl;

  try
  { 
	if (!this->is_connected)
		this-> connect();

    std::string resp;
    m_sock->send (sSendString);
    
    if (!is_sock_for_blocking_cmd)
    {
      //- //std::cout << "SocketItem::send_recv for socket index <" << m_socket_number << "> waiting for response" << std::endl;
      m_sock->receive (resp);
      //- //std::cout << "SocketItem::send_recv for socket index <" << m_socket_number << "> response <" << resp << ">" << std::endl;
    }
    else
    {
      resp = "0, EndOfApi";
    }
    
    ::memset (sReturnString, 0, iReturnStringSize);
    resp.copy (sReturnString, iReturnStringSize);
    // //std::cout << "SocketItem::send_recv for socket index <" << m_socket_number 
    //           << "> command <" << sSendString 
    //           << "> response <" << sReturnString << ">" << std::endl;
	++m_com_success;
	m_used = false;
	m_com_consecutive_errors = 0;
  }
  catch (yat::SocketException &se)
  {
	++m_com_errors;
	++m_com_consecutive_errors;
	m_used = false;
	std::stringstream s;
	if (!is_sock_for_blocking_cmd)
	  s << "Non ";
	  
	s << "Blocking Socket <" << m_socket_number << ">"
	  << " Command <" << sSendString;

	if (!is_sock_for_blocking_cmd)
	  s << "> Response <" << sReturnString;
	  
	s << ">" << std::endl;	
	s << "caught yat::Socket Exception <" << se.errors[0].desc << ">" << std::endl;
	//std::cerr << "SocketItem::send_recv COMMUNICATION_FAILED " << s.str () << std::endl;
	THROW_DEVFAILED ("COMMUNICATION_FAILED",
					 s.str ().c_str (),
					 "SocketItem::send_recv ()");
  }
  catch (...)
  {
	++m_com_errors;
	++m_com_consecutive_errors;
    m_used = false;
    //std::cerr << "SocketItem::send_recv COMMUNICATION_FAILED caught (...) " << std::endl;
    THROW_DEVFAILED ("COMMUNICATION_FAILED",
                     "socket number out of range [call software support]",
                     "SocketItem::send_recv ()");
  }
}

//------------------------------------------------
//- SocketItem reserve reserves the socket for 1 call
//------------------------------------------------
int SocketItem::reserve (void)
{
  //- //std::cout << "SocketItem::reserve socket number <" << m_socket_number << ">" << std::endl;
  m_used = true;
  return m_socket_number;
}

//----------------------------------------
// is_used 
// returns true if already reserved or max consecutive com errors reached
//----------------------------------------
bool SocketItem::is_used (void)
{
  if (m_used || (m_com_consecutive_errors >= MAX_COM_RETRY))
    return true;
  return false;
}

//----------------------------------------
// is_com_error
// returns true if max consecutive com errors reached
//----------------------------------------
bool SocketItem::is_com_error (void)
{
  if (m_com_consecutive_errors >= MAX_COM_RETRY)
    return true;
  return false;
}

//----------------------------------------
// is_socket_lost_connection
// returns true if the the socket has lost connection 
//- TANGODEVIC-1197
//----------------------------------------
bool SocketItem::is_socket_lost_connection (void)
{
  //cout<<"errors ... <"<<m_com_consecutive_errors<<">"<<endl;
  if ((m_com_errors > MAX_COM_RETRY) || (m_com_consecutive_errors > MAX_COM_RETRY))
    return true;
  return false;
}
  
  
//------------------------------------------------
// SocketPool : Singleton Management
//-----------------------------------------------
SocketPool * SocketPool::singleton = 0;

//----------------------------------------
// SocketPool CTOR
//----------------------------------------
SocketPool::SocketPool () : 
    nb_non_block_sock_reserved_peak (0),
    internal_state (SW_NOT_INITIALIZED)
{
  status_str.reserve (512);
}

// ============================================================================
// Config::Config
// ============================================================================
SocketPool::Config::Config ()
{
  ip_address              = "Not Initialised";
  port                    = 0;
  in_tmout                = 3000;
  o_tmout                 = 3000;
  nb_blocking_sockets     = 1;
  nb_non_blocking_sockets = 2;
}

SocketPool::Config::Config (const Config & _src)
{
  *this = _src;
}

// ============================================================================
// Config::operator =
// ============================================================================
SocketPool::Config& SocketPool::Config::operator = (const Config & _src)
{
  ip_address              = _src.ip_address;
  port                    = _src.port;
  in_tmout                = _src.in_tmout;
  o_tmout                 = _src.o_tmout;
  nb_blocking_sockets     = _src.nb_blocking_sockets;
  nb_non_blocking_sockets = _src.nb_non_blocking_sockets;

  return *this;
}

//----------------------------------------
// SocketPool DTOR
//----------------------------------------
SocketPool::~SocketPool ()
{
 // NO OP
}

//----------------------------------------
// Release (delete) SocketPool
//----------------------------------------
void SocketPool::release (void)
{
  //std::cout << "SocketPool::release "<< std::endl;
  this->internal_state = SW_NOT_CONFIGURED;
  //- Cleanup all created sockets
  yat::Socket::terminate ();

  //- delete the SocketItems
  for (size_t i = 0; i < v_sock.size (); i++)
  {
    if (v_sock[i])
      delete v_sock[i];
    v_sock[i] = 0;
  }
  v_sock.clear();
}

//----------------------------------------
// SocketPool socket creation 
//----------------------------------------
void SocketPool::initialize (const Config & c)
{
  this->internal_state = SW_NOT_INITIALIZED;

  if (!singleton)
  {
    this->internal_state = SW_ALLOC_ERROR;
    THROW_DEVFAILED ("OUT_OF_MEMORY",
                     "SocketPool Singleton creation failed",
                     "SocketPool::initialize ()");
  }
  
  this->conf = c;

  //- compute the sockets indexes
  SOCK_BLOCK_MIN_NUMBER    = 0;
  SOCK_BLOCK_MAX_NUMBER	   = conf.nb_blocking_sockets ;
  SOCK_NONBLOCK_MIN_NUMBER = SOCK_BLOCK_MAX_NUMBER;
  SOCK_NONBLOCK_MAX_NUMBER = SOCK_NONBLOCK_MIN_NUMBER + conf.nb_non_blocking_sockets;

  // Software object is created 
  this->internal_state = SW_INITIALIZED;
}

 //----------------------------------------
// SocketPool socket creation 
//----------------------------------------
void SocketPool::sw_run (void)
{ 
  //- YAT socket initialization stuff
  yat::Socket::init();
  bool blocking = false;

  try
  {
    //- create all sockets in a vector
    for (size_t i = SOCK_BLOCK_MIN_NUMBER; i < SOCK_NONBLOCK_MAX_NUMBER; i++)
    {
   //std::cout << "SocketPool::sw_run. Socket number=  <" << i << ">" << std::endl;
		blocking = ( i < SOCK_BLOCK_MAX_NUMBER) ? true : false;
		SocketItem * si = new SocketItem (i, conf.ip_address, conf.port, conf.in_tmout, conf.o_tmout, blocking); 
		v_sock.push_back (si);
    }

  }
  catch (yat::SocketException &e)
  {
    this->internal_state = SW_INITIALIZATION_ERROR;
    {      
		set_status("Sockets Initialization Error caught SocketException <" + e.errors[0].desc + "> [check properties, Kill/Restart Dserver]\n");
		THROW_DEVFAILED ("OPERATION_NOT_ALLOWED",
					   "Socket creation failed",
					   "SocketPool::start()");
    }
  }
  catch (...)
  {
    this->internal_state = SW_INITIALIZATION_ERROR;
    {
		set_status("Sockets Initialization Error caught (...) [check properties, Kill/Restart Dserver]\n");
		THROW_DEVFAILED ("OPERATION_NOT_ALLOWED",
					   "Socket creation failed",
					   "SocketPool::start()");
    }
  }
  this->internal_state = SW_INITIALIZED;
  {
	set_status("Sockets Initialized\n");
  }
}



//----------------------------------------
// SocketPool get_blocking_socket
//----------------------------------------
size_t SocketPool::get_blocking_socket()
{
#ifndef TU_TRAJECTORY_TESTS
  yat::AutoMutex <yat::Mutex> guard (this->block_sock_mutex);
  //  find the 1rst free blocking socket
  
  //  //std::cout << "get_blocking_socket= SOCK_BLOCK_MIN_NUMBER" << SOCK_BLOCK_MIN_NUMBER <<std::endl;
  //  //std::cout << "get_blocking_socket= SOCK_BLOCK_MAX_NUMBER" << SOCK_BLOCK_MAX_NUMBER <<std::endl;
  
  for (size_t i = SOCK_BLOCK_MIN_NUMBER; ((i < v_sock.size ()) && (i < SOCK_BLOCK_MAX_NUMBER)); i++)
  {
   if (v_sock [i]->is_used ())
    {
      continue;
    }
    //- //std::cout << "get_blocking_socket= " << i <<std::endl;
    return v_sock [i]->reserve ();
  }
	//- all sockets are already in use
  THROW_DEVFAILED ("OPERATION_NOT_ALLOWED",
                   "All <<Blocking Sockets>> (used for blocking commands) are used",
                   "SocketPool::get_blocking_socket()");
#else
    return 0;
#endif
}

//----------------------------------------
// TANGODEVIC-1197
// SocketPool get_non_blocking_socket
//----------------------------------------
size_t SocketPool::get_non_blocking_socket()
{
#ifndef TU_TRAJECTORY_TESTS
  int socketItem_with_comm_error = -1;	
  yat::AutoMutex <yat::Mutex> guard (this->non_block_sock_mutex);

  //  find the 1rst free non blocking socket
  for (size_t i = SOCK_NONBLOCK_MIN_NUMBER; ((i < v_sock.size ()) && (i < SOCK_NONBLOCK_MAX_NUMBER)); i++)
  {
    //- //std::cout << "SocketPool::get_non_blocking_socket() trying socket index <" << i << ">" << std::endl;
	
	// TANGODEVIC-1197:
	if ( v_sock [i]->is_used ())
	{
		//- memorize the first socket item which is in comm error
		if (v_sock [i]->get_counters_comm_errors() > 0 )
			socketItem_with_comm_error = i; 
		continue;
	}
    nb_non_block_sock_reserved_peak = (i > nb_non_block_sock_reserved_peak) ? i : nb_non_block_sock_reserved_peak;
    status_stream.str ("");
    status_stream << "simultaneous used sockets peak : <" << nb_non_block_sock_reserved_peak << ">" << std::endl;
    set_status (status_stream.str ());
	
  	//- One free socket item has been found    
    return v_sock [i]->reserve ();
  }
  
  //- TANGODEVIC-1197:
  //  If there is not more socket item available and at least one socket item is in comm error,
  //  then retry connection with this socket item.
  if (socketItem_with_comm_error != -1)
 	return v_sock [socketItem_with_comm_error]->reserve ();
  
  //- all sockets are already in use
  THROW_DEVFAILED ("OPERATION_NOT_ALLOWED",
                   "All <<Non Blocking Sockets>> (used for non blocking commands) are used",
                   "SocketPool::get_non_blocking_socket()");
#else
  return 0;
#endif
}

//------------------------------------------------
// SocketPool : Interface to YAT Socket signature
//-----------------------------------------------
void SocketPool::SendAndReceive(size_t socketID, char sSendString[], char sReturnString[], int iReturnStringSize)
{
#ifndef TU_TRAJECTORY_TESTS	
  if (socketID < SOCK_BLOCK_MIN_NUMBER || socketID > SOCK_NONBLOCK_MAX_NUMBER)
  {
    //std::cerr << "SocketPool::SendAndReceive SOFTWARE_FAILURE socket number out of range [call software support]" << std::endl;
    THROW_DEVFAILED ("SOFTWARE_FAILURE",
                     "socket number out of range [call software support]",
                     "SocketPool::SendAndReceive ()");
  }
  
  
  //std::cout <<"counters_comm_errors["<<socketID<<"] ...<"<<v_sock[socketID]->get_counters_comm_errors()<<">"<<std::endl;	
  
  // Check if number of retries is reached to reopen socket 
  //- TANGODEVIC-1197: reset socket connection after 5 retries without success 
  if (v_sock [socketID]->get_counters_comm_errors() >= MAX_COM_RETRY)
  {
		SocketPool::instance()->reset_socket_connection();
  }
  
  try
  {
	v_sock [socketID]->send_recv (sSendString, sReturnString, iReturnStringSize);
  }
  catch (Tango::DevFailed &e)
  {
	//- TANGODEVIC-1197:
	//------------------------
	 //v_sock [socketID]->incr_counters_comm_errors(); already incremented in send_recv exception

	// Once number of retries is reached throw an error to warn upper layers
	if (v_sock [socketID]->get_counters_comm_errors() == MAX_COM_RETRY )
		RETHROW_DEVFAILED(e,
						_CPTC("SOFTWARE_ERROR"),
						_CPTC("xps_ns::SocketPool::SendAndReceive - MAX_COM_RETRY is reached."),
						_CPTC("xps_ns::SocketPool::SendAndReceive")); 
  }
#endif
}
		
//------------------------------------------------
// SocketPool::state
//-----------------------------------------------
SocketPool::ClassState SocketPool::state (void)
{
	if (internal_state != SW_CONFIGURED)
	  return internal_state;
	//- there are SocketItems that are healthy -> OK
	for (size_t i = SOCK_NONBLOCK_MIN_NUMBER; i < v_sock.size (); i++)
	{
	  if (!v_sock [i]->is_com_error ())
		  return internal_state;
	}
	//- no SocketItem healthy->switch to Communication Error
	set_status ("Communication Fatal Error, all sockets reached max consecutive com errors [try to restart device, or cycle power on XPS]");
	internal_state = SW_COMMUNICATION_ERROR;
}

		
//------------------------------------------------
// SocketPool::reset_socket_connection
//-----------------------------------------------	
void SocketPool::reset_socket_connection	()
{
  for (size_t i = 0; i < v_sock.size (); i++)
  {
 	// std::cout << "SocketPool::reset_socket_connection() trying socket index <" << i << ">" << std::endl;
    // Close SocketItem if connection is lost
	if (v_sock [i]->is_socket_lost_connection())
	{
		bool is_blocking = v_sock [i]->is_blocking_cmd_sock();
		SocketItem * oldsi= v_sock[i];
		// delete non functioning SocketItem
		delete oldsi;
 		
		// now create a new SocketItem		
		SocketItem * si = new SocketItem (i, conf.ip_address, conf.port, conf.in_tmout, conf.o_tmout, is_blocking); 
		
		//replace previous SocketItem by new one in the vector
		replace(v_sock.begin(), v_sock.end(), v_sock[i],si );
	}	
  }
}
} //- namespace



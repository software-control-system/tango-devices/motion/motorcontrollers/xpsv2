/*************************************************
 *   XPS_API.h                                   *
 *                                               *
 *   Description:                                *
 *       XPS functions                           *
 *************************************************/
//- original code by Micro-controle
//- ftp'd from XPS Motion Controller of SIXS
//- modified by Jean Coquet Synchrotron Soleil
//- for use with yat::Sockets


#ifndef __XPS_C8_DRIVERS_H__
#define __XPS_C8_DRIVERS_H__


#define DLL 
#ifndef _WIN32
  #define __stdcall
#endif

#ifdef __cplusplus
extern "C"
{
#else
#typedef int bool;  /* C does not know bool, only C++ */
#endif

DLL int __stdcall ErrorStringGet (size_t SocketIndex, int ErrorCode, char * ErrorString);  /* Return the error string corresponding to the error code */
DLL int __stdcall FirmwareVersionGet (size_t SocketIndex, char * Version);  /* Return firmware version */

DLL int __stdcall GroupHomeSearch (size_t SocketIndex, char * GroupName);  /* Start home search sequence */
DLL int __stdcall GroupInitialize (size_t SocketIndex, char * GroupName);  /* Start the initialization */
DLL int __stdcall GroupJogModeDisable (size_t SocketIndex, char * GroupName);  /* Disable Jog mode on selected group */
DLL int __stdcall GroupKill (size_t SocketIndex, char * GroupName);  /* Kill the group */
DLL int __stdcall GroupMoveAbort (size_t SocketIndex, char * GroupName);  /* Abort a move */


DLL int __stdcall GroupMoveAbsolute (size_t SocketIndex, char * GroupName, int NbElements, double TargetPosition[]);  /* Do an absolute move */

DLL int __stdcall GroupMoveRelative (size_t SocketIndex, char * GroupName, int NbElements, double TargetDisplacement[]);  /* Do a relative move */

DLL int __stdcall GroupMotionDisable (size_t SocketIndex, char * GroupName);  /* Set Motion disable on selected group */
DLL int __stdcall GroupMotionEnable (size_t SocketIndex, char * GroupName);  /* Set Motion enable on selected group */
DLL int __stdcall GroupPositionCurrentGet (size_t SocketIndex, char * GroupName, int NbElements, double CurrentEncoderPosition[]);  /* Return current positions */
DLL int __stdcall GroupPositionSetpointGet (size_t SocketIndex, char * GroupName, int NbElements, double SetPointPosition[]);  /* Return setpoint positions */

DLL int __stdcall GroupReferencingActionExecute (size_t SocketIndex, char * PositionerName, char * ReferencingAction, char * ReferencingSensor, double ReferencingParameter);  /* Execute an action in referencing mode */
DLL int __stdcall GroupReferencingStart (size_t SocketIndex, char * GroupName);  /* Enter referencing mode */
DLL int __stdcall GroupReferencingStop (size_t SocketIndex, char * GroupName);  /* Exit referencing mode */
DLL int __stdcall GroupStatusGet (size_t SocketIndex, char * GroupName, int * Status);  /* Return group status */
DLL int __stdcall GroupStatusStringGet (size_t SocketIndex, int GroupStatusCode, char * GroupStatusString);  /* Return the group status string corresponding to the group status code */
DLL int __stdcall GroupVelocityCurrentGet (size_t SocketIndex, char * GroupName, int NbElements, double CurrentVelocity[]);  /* Return current velocities */
DLL int __stdcall PositionerBacklashGet (size_t SocketIndex, char * PositionerName, double * BacklashValue, char * BacklaskStatus);  /* Read backlash value and status */
DLL int __stdcall PositionerBacklashEnable (size_t SocketIndex, char * PositionerName);  /* Enable the backlash */
DLL int __stdcall PositionerBacklashDisable (size_t SocketIndex, char * PositionerName);  /* Disable the backlash */

DLL int __stdcall PositionerPositionCompareGet (size_t SocketIndex, char * PositionerName, double * MinimumPosition, double * MaximumPosition, double * PositionStep, bool * EnableState);  /* Read position compare parameters */
DLL int __stdcall PositionerPositionCompareSet (size_t SocketIndex, char * PositionerName, double MinimumPosition, double MaximumPosition, double PositionStep);  /* Set position compare parameters */
DLL int __stdcall PositionerPositionCompareEnable (size_t SocketIndex, char * PositionerName);  /* Enable position compare */
DLL int __stdcall PositionerPositionCompareDisable (size_t SocketIndex, char * PositionerName);  /* Disable position compare */

DLL int __stdcall PositionerPositionComparePulseParametersGet (size_t SocketIndex, char * PositionerName, double * PCOPulseWidth, double * EncoderSettlingTime);  /* Get position compare PCO pulse parameters */
DLL int __stdcall PositionerPositionComparePulseParametersSet (size_t SocketIndex, char * PositionerName, double PCOPulseWidth, double EncoderSettlingTime);  /* Set position compare PCO pulse parameters */
DLL int __stdcall PositionerSGammaParametersGet (size_t SocketIndex, char * PositionerName, double * Velocity, double * Acceleration, double * MinimumTjerkTime, double * MaximumTjerkTime);  /* Read dynamic parameters for one axe of a group for a future displacement  */
DLL int __stdcall PositionerSGammaParametersSet (size_t SocketIndex, char * PositionerName, double Velocity, double Acceleration, double MinimumTjerkTime, double MaximumTjerkTime);  /* Update dynamic parameters for one axe of a group for a future displacement */
DLL int __stdcall PositionerUserTravelLimitsGet (size_t SocketIndex, char * PositionerName, double * UserMinimumTarget, double * UserMaximumTarget);  /* Read UserMinimumTarget and UserMaximumTarget */
DLL int __stdcall PositionerUserTravelLimitsSet (size_t SocketIndex, char * PositionerName, double UserMinimumTarget, double UserMaximumTarget);  /* Update UserMinimumTarget and UserMaximumTarget */
DLL int __stdcall MultipleAxesPVTVerification (size_t SocketIndex, char * GroupName, char * TrajectoryFileName);  /* Multiple axes PVT trajectory verification */
DLL int __stdcall MultipleAxesPVTVerificationResultGet (size_t SocketIndex, char * PositionerName, char * FileName, double * MinimumPosition, double * MaximumPosition, double * MaximumVelocity, double * MaximumAcceleration);  /* Multiple axes PVT trajectory verification result get */
DLL int __stdcall MultipleAxesPVTExecution (size_t SocketIndex, char * GroupName, char * TrajectoryFileName, int ExecutionNumber);  /* Multiple axes PVT trajectory execution */
DLL int __stdcall MultipleAxesPVTParametersGet (size_t SocketIndex, char * GroupName, char * FileName, int * CurrentElementNumber);  /* Multiple axes PVT trajectory get parameters */
DLL int __stdcall MultipleAxesPVTLoadToMemory (size_t SocketIndex, char * GroupName, char * TrajectoryPart);  /* Multiple Axes Load PVT trajectory through function */
DLL int __stdcall MultipleAxesPVTResetInMemory (size_t SocketIndex, char * GroupName);  /* Multiple Axes PVT trajectory reset in memory */

DLL int __stdcall SingleAxisSlaveModeEnable (size_t SocketIndex, char * GroupName);  /* Enable the slave mode */
DLL int __stdcall SingleAxisSlaveModeDisable (size_t SocketIndex, char * GroupName);  /* Disable the slave mode */
DLL int __stdcall SingleAxisSlaveParametersSet (size_t SocketIndex, char * GroupName, char * PositionerName, double Ratio);  /* Set slave parameters */
DLL int __stdcall SingleAxisSlaveParametersGet (size_t SocketIndex, char * GroupName, char * PositionerName, double * Ratio);  /* Get slave parameters */

DLL int __stdcall ExecLowLevelCmd(size_t SocketIndex, char * strIn, char * strOut);  /* Low level command */

#ifdef __cplusplus
}
#endif


#endif //- __XPS_C8_DRIVERS_H__

/*///////////////////////////////////////////////////////////////////////////////
* SocketWrapper.h  : This wrapper aims to make the glue beteen socket interface expected by XPS library and YATSocket interface.
*/  

#ifndef __SOCKETWRAPPER_H__
#define __SOCKETWRAPPER_H__
#include <yat4tango/ExceptionHelper.h>
#include "SocketPool.h"

//***************************************************************************************

void SendAndReceive(size_t socketID, char sSendString[], char sReturnString[], int iReturnStringSize)
{
  //- initialize sReturnString to avoid problems
  sReturnString [0] = 0;
  try
  {
    xps_ns::SocketPool::instance()->SendAndReceive( socketID, sSendString, sReturnString, iReturnStringSize);
  }
  catch (Tango::DevFailed &e)
  {
    //- avoid memory leak in XPS API due to malloc of sReturnString not free'd in case of exception
    free (sReturnString);
    throw (e);
  }
  catch (...)
  {
    //- avoid memory leak in XPS API due to malloc of sReturnString not free'd in case of exception
    free (sReturnString);
    THROW_DEVFAILED ("UNKNOWN_EXCEPTION",
		"unknown exception caught while calling SockePool::SendAndReceive",
		"SocketWrapper::SendAndReceive");
  }
}

//void SetTCPTimeout (int SocketID, double Timeout);
//void CloseSocket (int SocketID);
//char * GetError (int SocketID);
//void strncpyWithEOS(char * szStringOut, const char * szStringIn, int nNumberOfCharToCopy, int nStringOutSize);

#endif //- __SOCKETWRAPPER_H__

#ifndef __GROUP_STATE_STR_H__
#define __GROUP_STATE_STR_H__

#define MAX_XPS_GROUP_STATES_STRINGS 101

namespace xps_ns
{

  static const std::string hw_group_states_str [MAX_XPS_GROUP_STATES_STRINGS] =
  {
    "<0> NOT INIT from Boot\n",
    "<1> NOT INIT from Motion Disable\n",
    "<2> NOT INIT from Emergency Stop\n",
    "<3> NOT INIT from Homing/Referencing Error\n",
    "<4> NOT INIT from Not Ref\n",
    "<5> NOT INIT from Homing/Referencing Error\n",
    "<6> NOT INIT from Homing/Referencing Error\n",
    "<7> NOT INIT from GroupKill or KillAll\n",
    "<8> NOT INIT from Homing/Referencing Error\n",
    "<9> NOT INIT from Scaling/Encoder Calibration or Motor Init Error\n",
    
    "<10> READY from Moving Done or GroupMoveAbort or GroupJogModeDisable or Trajectory Done\n",
    "<11> READY from Homing or Referencing\n",
    "<12> READY GroupJogModeEnable from Moving\n",
    "<13> READY from GroupMotionEnable\n",
    "<14> READY STATE FROM SLAVE\n",
    "<15> READY from GroupMoveAbort from Jogging\n",
    "<16> READY from GroupAnalogTrackingModeDisable\n",
    "<17> READY from GroupMoveAbort from Trajectory \n",
    "<18> NO DESCRIPTION FOR THAT STATE\n",
    "<19> NO DESCRIPTION FOR THAT STATE\n",
    
    "<20> DISABLE\n",
    "<21> DISABLE\n",
    "<22> DISABLE\n",
    "<23> DISABLE\n",
    "<24> DISABLE\n",
    "<25> DISABLE\n",
    "<26> DISABLE\n",
    "<27> DISABLE\n",
    "<28> DISABLE\n",
    "<29> DISABLE\n",

    "<30> DISABLE\n",
    "<31> DISABLE\n",
    "<32> DISABLE\n",
    "<33> DISABLE\n",
    "<34> DISABLE\n",
    "<35> DISABLE\n",
    "<36> DISABLE\n",
    "<37> DISABLE\n",
    "<38> DISABLE\n",
    "<39> DISABLE\n",
    
    "<40> EMERGENCY_BRAKING\n",
    "<41> MOTOR_INIT\n",
    "<42> NOTREF\n",
    "<43> HOMING\n",
    "<44> MOVING\n",
    "<45> TRAJECTORY\n",
    "<46> SLAVE MODE ENABLED\n",
    "<47> JOGGING\n",
    "<48> ANALOG TRACKING\n",
    "<49> NO DESCRIPTION FOR THAT STATE\n",
    
    "<50> NOT INIT from Homing/Referencing Error\n",
    "<51> NO DESCRIPTION FOR THAT STATE\n",
    "<52> NO DESCRIPTION FOR THAT STATE\n",
    "<53> NO DESCRIPTION FOR THAT STATE\n",
    "<54> NO DESCRIPTION FOR THAT STATE\n",
    "<55> NO DESCRIPTION FOR THAT STATE\n",
    "<56> NO DESCRIPTION FOR THAT STATE\n",
    "<57> NO DESCRIPTION FOR THAT STATE\n",
    "<58> NO DESCRIPTION FOR THAT STATE\n",
    "<59> NO DESCRIPTION FOR THAT STATE\n",   
    
    "<60> NO DESCRIPTION FOR THAT STATE\n",   
    "<61> NO DESCRIPTION FOR THAT STATE\n",   
    "<62> NO DESCRIPTION FOR THAT STATE\n",   
    "<63> NOT INIT from Scaling Calibration\n",
    "<64> REFERENCING\n",
    "<65> NO DESCRIPTION FOR THAT STATE\n",   
    "<66> NO DESCRIPTION FOR THAT STATE\n",   
    "<67> NO DESCRIPTION FOR THAT STATE\n",   
    "<68> AUTOTUNING\n",   
    "<69> SCALING CALIBRATION\n",   

    "<70> READY from AutoTuning\n",
    "<71> NO DESCRIPTION FOR THAT STATE\n",   
    "<72> NO DESCRIPTION FOR THAT STATE\n",   
    "<73> EXCITATION SIGNAL\n",   
    "<74> NO DESCRIPTION FOR THAT STATE\n",   
    "<75> NO DESCRIPTION FOR THAT STATE\n",   
    "<76> NO DESCRIPTION FOR THAT STATE\n",   
    "<77> READY from Excitation Signal\n",
    "<78> READY from Excitation Signal\n",
    "<79> READY from Excitation Signal\n",

    "<80> READY from Excitation Signal\n",
    "<81> READY from Excitation Signal\n",
    "<82> READY from Excitation Signal\n",
    "<83> READY from Excitation Signal\n",
    "<84> READY from Excitation Signal\n",
    "<85> READY from Excitation Signal\n",
    "<86> READY from Excitation Signal\n",
    "<87> READY from Excitation Signal\n",
    "<88> READY from Excitation Signal\n",
    "<89> READY from Excitation Signal\n",
    
    "<90> READY from Excitation Signal\n",
    "<91> READY from Excitation Signal\n",
    "<92> READY from Excitation Signal\n",
    "<93> READY from Excitation Signal\n",
    "<94> READY from Excitation Signal\n",
    "<95> READY from Excitation Signal\n",
    "<96> READY from Excitation Signal\n",
    "<97> READY from Excitation Signal\n",
    "<98> READY from Excitation Signal\n",
    "<99> READY from Excitation Signal\n",
	
    "<100> UNKNOWN STATE (communication with XPS Error)\n"
  };
  


} //- namespace

#endif //-__GROUP_STATE_STR_H__


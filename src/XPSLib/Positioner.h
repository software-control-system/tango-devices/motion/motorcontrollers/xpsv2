#ifndef __POSITIONER__H__
#define __POSITIONER__H__

#include <string>
#include <yat/time/Timer.h>
#include "Observer.h"
const int MAX_CODE_STRING_LENGTH=512;


namespace xps_ns
{

	class Positioner : public Observable
	{
		//-------------------------------------------
		//- configs, enums, consts
		//-------------------------------------------

	public:
		typedef enum
		{
			SW_UNKNOWN_STATE,
			SW_NOT_INITIALIZED,
			SW_INITIALIZED,
			SW_INITIALIZATION_ERROR,
			SW_NOT_CONFIGURED,
			SW_ALLOC_ERROR,
			SW_CONFIGURED,
			SW_CONFIGURATION_ERROR
		}ClassState;

		typedef struct TriggerConf
		{
			double start_pos;
			double end_pos;
			double step;
			double pulse_width;
			double encoder_settling_time;
			bool enabled;
		}TriggerConf;

	private:
		typedef struct Limits
		{
			double min;
			double max;
			Limits ();
		}Limits;

    typedef struct SoftLimits
    {
      Limits xps;
      Limits tango;
    }SoftLimits;

		public:
		//-------------------------------------------
		//- Positioner
		//-------------------------------------------
		Positioner (std::string name, int number);
		void release (void) ;
	
		//- class state health
		ClassState sw_state (void);

		void sw_run (void);

		//- trigger interface
		void trigger_enable (void);
		void trigger_disable (void);
		bool is_trigger_enabled (void);
		void set_trigger_parameters (TriggerConf trg_conf);
		TriggerConf& get_trigger_parameters (void);
    //- gets soft limits from the controller
		void refresh_xps_min_max_limits ();

		std::string get_firmware_revision (void);
		std::string get_name(void)
		{
			return positioner_name;
		};
		std::string get_full_name(void)
		{
			return full_name;
		};
		int get_positioner_number (void)
		{
		  return positioner_number;
		}
		//- TANGODEVIC-57:
		void enable_backlash();
		void disable_backlash();
		
		//-------------------------------------------
		//- Accessors
		//-------------------------------------------
		double get_position (void);
		double get_xps_setpoint (void);
		double get_tango_setpoint (void);
		double get_offset   (void);
		//- returns the positioner's velocity setpoint
		double get_velocity (void);
		double get_accuracy (void); // TANGODEVIC-1375: add accuracy
		double get_current_velocity (void);
		double get_acceleration (void);
		void get_gamma_parameters (double & max_vel,
		double & max_accel, 
		double & min_jerk_tm, 
		double & max_jerk_tm);

		void get_xps_min_max_limits (double & min, double & max) 
		{
		  this->refresh_xps_min_max_limits ();
		  min = limits.xps.min; 
		  max = limits.xps.max;
		};
		void get_tango_min_max_limits (double & min, double & max) 
		{
		  min = limits.tango.min; 
		  max = limits.tango.max;
		};

		//-------------------------------------------
		//- Mutators
		//-------------------------------------------
		void set_position (double);
		void set_read_position (double new_read_position) {position= new_read_position;};
		void set_xps_setpoint (double new_xps_setpoint) {xps_setpoint= new_xps_setpoint;};
		void set_force_state_moving_time (double new_force_state_moving_time) {force_state_moving_time= new_force_state_moving_time;};
		void move_relative (double);
		void set_tango_setpoint (double);
		void set_offset   (double);
		void set_acceleration (double);
		void set_velocity (double);
		void set_tango_min_max_limits (double min, double max);
		//- rewrites the previously stored tango soft limits to the controller
		void restore_tango_min_max_limits (void);
		//- TANGODEVIC-1375: add accuracy
        void set_accuracy (double); 

        // Executes a low level command.
        // @param cmd The low level command
        // @param outParam Command reply
        void exec_low_level_cmd(std::string cmd, std::string &outParam);

	private:

		virtual ~Positioner (void);

    //- writes soft limits to the controller
		void set_xps_min_max_limits (double min, double max);

    //- refresh max velocity, accel, jerk times
		void refresh_gamma_parameters ();

		void throw_on_error (std::string origin, int ret_code);
		char ret_code_string [MAX_CODE_STRING_LENGTH];
	
		int positioner_number;
		std::string positioner_name;
		std::string full_name;

		//- trigger configuration
		TriggerConf trigger_conf;

		ClassState class_state;

		SoftLimits limits;
    
		double offset;
		double position;
		double xps_setpoint;
		double tango_setpoint;
		double velocity;
		double accuracy; // TANGODEVIC-1375:
		double max_velocity;
		double max_accel;
		double min_jerk_time;
		double max_jerk_time;
		double force_state_moving_time; // JIRA TANGODEVIC-67

		yat::Timeout trigger_tmout;
		yat::Timeout sgamma_tmout;
		yat::Timeout limits_tmout;

	};
}//- namespace
#endif //- __POSITIONER__H__


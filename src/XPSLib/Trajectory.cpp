#include "Trajectory.h"
#include <iostream>
#include <iomanip>
#include "xps_c8_drivers.h"
#include "SocketPool.h"
#include "Positioner.h"
#include "Group.h"
#include <yat4tango/ExceptionHelper.h>

#define MAX_ACC_RATIO 0.25

namespace xps_ns
{
  //-------------------------------------------
  //- Config
  //-------------------------------------------
  Trajectory::Config::Config ():
    group_name ("must be defined")
  {
    //- noop ctor
  }
  Trajectory::Config::Config (const Config & _src)
  {
    *this = _src;
  }
  // Trajectory::Config::operator =
  Trajectory::Config &  Trajectory::Config::operator = (const Config & src)
  {
    if (& src == this)
      return *this;
    group_name       = src.group_name;
    positioner_names = src.positioner_names;
    return *this;
  }
	
	Trajectory::MulAxesPVTVerResults::MulAxesPVTVerResults ()
	{
		min_pos   = 0.;
		max_pos   = 0.;
		max_vel   = 0.;
		max_accel = 0.;
	}


	// ============================================================================
	// Trajectory::Trajectory    :   CTOR
	// ============================================================================
  Trajectory::Trajectory (const Config &c) :
     class_state (SW_NOT_INITIALIZED),
     traj_state  (TRAJ_NOT_INITIALIZED),
		 traj_type   (TRAJ_NOT_INIT),
     conf (c),
     nb_positioners (c.positioner_names.size ())
	{
    //std::cout << "Trajectory::Trajectory <-\n";
    //- reserve space to avoid reallocations
    status.resize (2048);
    positioners_infos.resize (conf.positioner_names.size ());
    class_state = SW_INITIALIZED;
	}

  // ============================================================================
	// Trajectory::~Trajectory    :   DTOR
	// ============================================================================
  Trajectory::~Trajectory ()
	{
    //std::cout << "Trajectory::~Trajectory <-\n";
    //- NOOP
	}

	// ============================================================================
	// Trajectory::sw_run
	// ============================================================================
 void  Trajectory::sw_run (void)
	{
   
    //- retreive positioner infos : group name, positioner name, soft limits,...
	try
	{
		this->get_positioners_infos ();
		class_state = SW_CONFIGURED;
	}
    catch (...)
    {
      //std::cerr << " Trajectory::sw_run <Trajectory> initialization error : (...) caught\n";
      class_state = SW_CONFIGURATION_ERROR;
      THROW_DEVFAILED ("SW_CONFIGURATION_ERROR",
                       "(...) caught trying initialize Trajectory",
                       " Trajectory::sw_run ()");
    }
  }

	// ============================================================================
	// Trajectory::clear    
	// ============================================================================
	void Trajectory::clear () 
	{
		//std::cout << "Trajectory::clear " << std::endl;
    //- clear class members
    traj_state = TRAJ_NOT_INITIALIZED;

    //- clear trajectory in the XPS
    int s_n_b = SocketPool::instance()->get_non_blocking_socket();
    int ret = MultipleAxesPVTResetInMemory (s_n_b,
                                            const_cast <char*> (conf.group_name.c_str()));
    //std::cout << "Trajectory::clear MultipleAxesPVTResetInMemory ret code <" << ret << ">" << std::endl;
    throw_on_error ("Trajectory::clear::MultipleAxesPVTResetInMemory", ret);
    traj_state = TRAJ_INITIALIZED;
  }


	// ============================================================================
	// Trajectory::set_trajectory_upload_eol
	// ============================================================================
	void Trajectory::set_trajectory_upload_eol(const std::string& eol)
	{
		traj_upload_eol	= eol;
	}
	
	// ============================================================================
	// Trajectory::trajectory_upload
	// ============================================================================
  void Trajectory::trajectory_upload () 
  {
	 //- check class initialization
    if (class_state != SW_CONFIGURED)
    {
      //std::cerr << "Trajectory::trajectory_upload error class not properly configured check configurations, try to restart device]\n";
      THROW_DEVFAILED ("OPERATION_NOT_ALLOWED",
                       "class not properly configured check configurations, try to restart device]",
                       "Trajectory::trajectory_upload");
    }
    if (traj_state != TRAJ_INPUT_PROCESSED)
     {
      //std::cerr << "Trajectory::trajectory_upload OPERATION_NOT_ALLOWED : no valid trajectory [provide valid trajectory]\n";
      THROW_DEVFAILED ("OPERATION_NOT_ALLOWED",
                       "error : no valid trajectory [provide valid trajectory]",
                       "Trajectory::trajectory_upload");
    }
    traj_state = TRAJ_LOADING;
    int ret = 0;
    std::stringstream s;
    
    int s_n_b = SocketPool::instance()->get_non_blocking_socket();
    ret = MultipleAxesPVTResetInMemory (s_n_b,  const_cast <char*> (conf.group_name.c_str()));
    throw_on_error ("Trajectory::trajectory_upload::MultipleAxesPVTResetInMemory", ret);

    for (size_t i = 0; i < traj_times_rel.size (); i++)
    {
		s.clear ();
		s.str ("");

		s << traj_times_rel [i];
		for (size_t j = 0; j < positioners_infos.size (); j++)
		{
			s << "," << positioners_infos [j].rel_pos [i] << "," << positioners_infos [j].vel [i];
		}
		
		//adapt endl of line according to property (CTRLRFC-1179) 
		if(traj_upload_eol == "#")
		{
			s << "#";
		}
		else
		if(traj_upload_eol == "NUL")
		{
			s << std::ends;
		}
		else		
		{
			s << traj_upload_eol;	
		}
		/////////////////////////////////////////////////////////

		s_n_b = SocketPool::instance()->get_non_blocking_socket();
		ret = MultipleAxesPVTLoadToMemory (s_n_b,
										 const_cast <char*> (conf.group_name.c_str()),
										 const_cast <char*> (s.str ().c_str()));
		throw_on_error ("Trajectory::trajectory_upload::MultipleAxesPVTLoadToMemory", ret);
    }
    traj_state = TRAJ_LOADED;
  }

	// ============================================================================
	// Trajectory::get_uploaded_trajectory
	//- returns the previously uploaded trajectory
	// ============================================================================
  void Trajectory::get_uploaded_trajectory (long & dim_x, long & dim_y, std::vector <double> &raw_pvt)
  {
		//std::cout << "Trajectory::get_uploaded_trajectory <-" << std::endl;
    yat::AutoMutex <yat::Mutex> guard (this->uploaded_traj_copy_protect);
    dim_x = 1 + (positioners_infos.size () * 2);
    dim_y = traj_times_rel.size ();
    raw_pvt.resize (dim_x * dim_y);
		//std::cout << "Trajectory::get_uploaded_trajectory dim_x <" << dim_x << "> dim_y <" << dim_y << ">" << std::endl;
    //- for each line
    size_t  current_raw_pvt_index = 0;
    for (size_t i = 0; i < dim_y; i++)
    {
      raw_pvt.at(current_raw_pvt_index) = traj_times_rel[i];
      ++current_raw_pvt_index;
      for (size_t j = 0; j < positioners_infos.size (); j++)
      {
        raw_pvt.at(current_raw_pvt_index) = positioners_infos [j].rel_pos [i];
        ++current_raw_pvt_index;
        raw_pvt.at(current_raw_pvt_index) = positioners_infos [j].vel [i];
        ++current_raw_pvt_index;
      }
    }
  }


	// ============================================================================
	// Trajectory::trajectory_check
  // XPS check of previously uploaded trajectory
	// ============================================================================
  void Trajectory::check (void) 
  {
		//std::cout << "Trajectory::check <-" << std::endl;
    //- check class initialization
    if (class_state != SW_CONFIGURED)
    {
      //std::cerr << "Trajectory::check error class not properly configured check configurations, try to restart device]\n";
      THROW_DEVFAILED ("OPERATION_NOT_ALLOWED",
                       "class not properly configured check configurations, try to restart device]",
                       "Trajectory::check");
    }

    if (traj_state < TRAJ_LOADED ||
        traj_state == TRAJ_GOING_TO_START_POINT ||
        traj_state == TRAJ_START_POINT_ERROR ||
        traj_state == TRAJ_RUNNING_TRAJ)
    {
      //std::cerr << "Trajectory::check OPERATION_NOT_ALLOWED : XPS trajectory check condition not fullfilled [must be stopped at origin point]\n";
      THROW_DEVFAILED ("OPERATION_NOT_ALLOWED",
                       "error : XPS trajectory check condition not fullfilled [must be uploaded, stopped]",
                       "Trajectory::check");
    }
    
    traj_state = TRAJ_CHECKING;
    
    int s_n_b = SocketPool::instance()->get_non_blocking_socket();
    int ret = MultipleAxesPVTVerification (s_n_b,
                                       const_cast <char*> (conf.group_name.c_str()),
                                       "FromMemory");
    //std::cout << "Trajectory::check MultipleAxesPVTVerification returned code <" << ret << ">\n";

		//- there is an error on trajectory try to get the axis on fault
    if (ret != 0) 
    {
      traj_state = TRAJ_CHECK_ERROR;
      std::stringstream s;

      int s_n_b = SocketPool::instance()->get_non_blocking_socket();
      int c = ErrorStringGet (s_n_b, ret, this->ret_code_string);

      s << "Trajectory rejected. Reason <" << ret_code_string << ">\n";
      MulAxesPVTVerResults tmp;
			char filename [256];
      for (size_t i = 0; i < positioners_infos.size (); i++)
      {
        //- get for each axis the vezrification data
        int s_n_b = SocketPool::instance()->get_non_blocking_socket();
        int ret = MultipleAxesPVTVerificationResultGet (s_n_b,
                                                        const_cast <char*> (positioners_infos [i].full_name.c_str()),
                                                        filename,
                                                        &tmp.min_pos,
                                                        &tmp.max_pos,
                                                        &tmp.max_vel,
                                                        &tmp.max_accel);
				if (positioners_infos [i].max_vel < tmp.max_vel)
				{  
        s << "Trajectory::check trajectory check failed for positioner <" 
				  << positioners_infos [i].positioner_name 
          << "> requested max velocity <" << tmp.max_vel 
          << "> greater than positioner max velocity <" << positioners_infos [i].max_vel << ">" << std::endl;
			  }
				if (positioners_infos [i].max_accel < tmp.max_accel)
				{  
        s << "Trajectory::check trajectory check failed for positioner <" 
				  << positioners_infos [i].positioner_name
          << "> requested max acceleration <" << tmp.max_accel
          << "> greater than positioner max acceleration <" << positioners_infos [i].max_accel << ">" << std::endl;
			  }
      }

      //std::cerr << "Trajectory::check " << s.str () << std::endl;

      THROW_DEVFAILED ("DATA_OUT_OF_BOUND",
                       s.str ().c_str (),
                       "Trajectory::check");
    }
    traj_state = TRAJ_CHECK_OK;
  }

	// ============================================================================
	// Trajectory::start
	// ============================================================================
  void Trajectory::start (void) 
  {
		//std::cout << "Trajectory::trajectory_start <-" << std::endl;
    //- check class initialization
    if (class_state != SW_CONFIGURED)
    {
      //std::cerr << "Trajectory::start error class not properly configured check configurations, try to restart device]\n";
      THROW_DEVFAILED ("OPERATION_NOT_ALLOWED",
                       "class not properly configured check configurations, try to restart device]",
                       "Trajectory::start");
    }
    if (traj_state != TRAJ_CHECK_OK)
    {
      //std::cerr << "Trajectory::trajectory_start OPERATION_NOT_ALLOWED : trajectory not checked [check trajectory first]\n";
      THROW_DEVFAILED ("OPERATION_NOT_ALLOWED",
                       "error : trajectory not checked  [check trajectory first]",
                       "Trajectory::trajectory_start");
    }
    traj_state = TRAJ_RUNNING_TRAJ;
    int s_b = SocketPool::instance()->get_blocking_socket();
    Group::instance ()->force_state_moving ();
    //- Group will survey the stop of the motion to trigger the copy of XPSGroup positions attributes Read part to Write parts
    Group::instance ()->request_for_recopy_read_in_write ();
    int ret = MultipleAxesPVTExecution (s_b,
                                        const_cast <char*> (conf.group_name.c_str()),
                                        "FromMemory",
                                        1);
    throw_on_error ("Trajectory::trajectory_start::MultipleAxesPVTExecution", ret);
    
  }

	// ============================================================================
	// Trajectory::trajectory_go_to_origin
	// ============================================================================
  void Trajectory::go_to_origin (void) 
  {
      //std::cout << "Trajectory::trajectory_go_to_origin <-" << std::endl;
    //- check class initialization
    if (class_state != SW_CONFIGURED)
    {
      //std::cerr << "Trajectory::go_to_origin error class not properly configured check configurations, try to restart device]\n";
      THROW_DEVFAILED ("OPERATION_NOT_ALLOWED",
                       "class not properly configured check configurations, try to restart device]",
                       "Trajectory::go_to_origin");
    }
    if (traj_state < TRAJ_LOADED)
    {
      //std::cerr << "Trajectory::trajectory_go_to_origin OPERATION_NOT_ALLOWED : trajectory not loaded in XPS [load it first]\n";
      THROW_DEVFAILED ("OPERATION_NOT_ALLOWED",
                       "error : trajectory not loaded in XPS [load it first]",
                       "Trajectory::trajectory_go_to_origin");
    }
    Group::GroupState gst = Group::instance ()->get_group_state ();
    if (gst != Group::GRP_READY)
    {
      //std::cerr << "Trajectory::trajectory_go_to_origin OPERATION_NOT_ALLOWED : Group not Ready to move [check Group]\n";
      THROW_DEVFAILED ("OPERATION_NOT_ALLOWED",
                       "Group not Ready to move [check Group]",
                       "Trajectory::trajectory_go_to_origin");
    }

    std::vector <double> start_point;
    for (size_t i = 0; i < positioners_infos.size (); i++)
      start_point.push_back (positioners_infos [i].start_pos_abs);

    traj_state = TRAJ_GOING_TO_START_POINT;
    Group::instance ()->go_to_positions (start_point);
  }

  // ============================================================================
  // Trajectory::get_traj_state 
  // ============================================================================
  Trajectory::TrajState Trajectory::get_traj_state (void)
  {
    //- //std::cout << "Trajectory::get_traj_state <-" << std::endl;
    //- check if Group is in position
    
    Group::GroupState gst = Group::instance ()->get_group_state ();
    if (gst != Group::GRP_MOVING)
    {
      if (traj_state == TRAJ_GOING_TO_START_POINT)
        traj_state = TRAJ_AT_START_POINT;
      else if (traj_state == TRAJ_RUNNING_TRAJ)
        traj_state = TRAJ_DONE;
    }
#ifdef TU_TRAJECTORY_TESTS
    traj_state = TRAJ_CHECK_OK;
#endif	
    return traj_state;
  }

  // ============================================================================
  // Trajectory::get_class_state 
  // ============================================================================
  Trajectory::ClassState Trajectory::get_class_state (void)
  {
    //- //std::cout << "Trajectory::get_class_state <-" << std::endl;
    //- returns the state of the class
    
    return class_state;
  }


	// ============================================================================
  // Trajectory::get_trajectory_size 
	// ============================================================================
  int Trajectory::get_trajectory_size (void)
  {
		//std::cout << "Trajectory::get_trajectory_size <-" << std::endl;
    //- not implemented yet (is it usefull?)
    THROW_DEVFAILED ("SOFTWARE_ERROR",
                     "this method is not anymore implemented [it is useful? call sw support]",
                     "Trajectory::get_trajectory_size");

  }

	// ============================================================================
  //- Trajectory::get_traj_point_as_string
  //- get 1 point as string 
  //- return true if point exists
	// ============================================================================
  const char * Trajectory::get_traj_point_as_string (size_t index)
  {
		//std::cout << "Trajectory::get_traj_point_as_string <-" << std::endl;
    THROW_DEVFAILED ("SOFTWARE_ERROR",
                     "this method is not anymore implemented [it is useful? call sw support]",
                     "Trajectory::get_traj_point_as_string");
  }

	// ============================================================================
	// Trajectory::get_positioners_infos
	// ============================================================================
  void Trajectory::get_positioners_infos (void) 
  {
		//std::cout << "Trajectory::get_positioners_infos <-" << std::endl;
    //- TODO : traiter le cas ou on n'a pas lu les données
    PositionerInfos pi;
    class Positioner * p;
    this->positioners_infos.clear ();
    nb_positioners = conf.positioner_names.size ();
    for (int i = 0; i < nb_positioners; i++)
    {
      pi.group_name = conf.group_name;
      pi.positioner_name = conf.positioner_names [i];
			pi.full_name = pi.group_name + "." + pi.positioner_name;
      pi.pos_number = i;
      p = Group::instance ()->get_positioner (pi.positioner_name);
      p->get_gamma_parameters (pi.max_vel, pi.max_accel, pi.min_jerk_time, pi.max_jerk_time);
      p->get_xps_min_max_limits (pi.soft_limit_min, pi.soft_limit_max);
      this->positioners_infos.push_back (pi);
    }
/*    FOR DEBUG 
    for (size_t i = 0; i < positioners_infos.size (); i++)
    {
      std::cout << "Trajectory::get_positioners_infos positioner <#" 
          << positioners_infos [i].pos_number << ":" << positioners_infos [i].positioner_name
          << ">  soft lims ["
          << positioners_infos [i].soft_limit_min << ":" << positioners_infos [i].soft_limit_max
          << ">  max_vel <" << positioners_infos [i].max_vel << ">" 
          << ">  max_accel <" << positioners_infos [i].max_accel << ">" 
          << ">  min_jerk_time <" << positioners_infos [i].min_jerk_time << ">" 
          << ">  max_jerk_time <" << positioners_infos [i].max_jerk_time << ">" 
          << std::endl;
    }
*/    
  }
	// ============================================================================
	// Trajectory::throw_on_error
	// ============================================================================
  void Trajectory::throw_on_error (std::string origin, int return_code) 
  {
		//std::cout << "Trajectory::throw_on_error <-" << std::endl;
 #ifndef TU_TRAJECTORY_TESTS	   
    if (return_code == 0)
      return;

    int s_n_b = SocketPool::instance()->get_non_blocking_socket();
    int c = ErrorStringGet (s_n_b, return_code, this->ret_code_string);
    if (c != 0)
    {
      THROW_DEVFAILED ("COMMUNICATION_ERROR",
                       "error code returned by ErrorStringGet [call soft support]",
                       "Trajectory::throw_on_error");
    }
    THROW_DEVFAILED("COMMAND_ERROR",
                    ret_code_string,
                    origin.c_str ());
#endif
  }


	// ============================================================================
	// Trajectory::load_raw_pvt
	// ============================================================================
	void Trajectory::load_raw_pvt (long dim_x, long dim_y, std::vector <double> & pvt)
		throw (Tango::DevFailed)
	{
		//std::cout << "Trajectory::load_raw_pvt" << std::endl;
	  this->traj_type = TRAJ_RAW_POS;
    //- exception if something was wrong in argins or class state, do not catch it
    //- same checks for abs and rel trajectory
    this->check_pvt_input (dim_x, dim_y, pvt);

    //- OK? clear the XPS and internal buffers
    this->clear ();
    traj_times_abs.resize (0);
    traj_times_rel.resize (dim_y);
    for (size_t i = 0; i < positioners_infos.size (); i ++)
    {
      positioners_infos [i].abs_pos.resize (0);
      positioners_infos [i].rel_pos.resize (dim_y);
      positioners_infos [i].vel.resize (dim_y);
    }

    //- copy raw times in internal vector
    for (int i = 0; i < dim_y; i++)
      traj_times_rel [i] = pvt [i * dim_x];

    //- copy raw traj in internal vectors
    for (size_t i = 0; i < positioners_infos.size (); i ++)
    {
      for (int j = 0; j < dim_y; j++)
      {
        positioners_infos [i].rel_pos [j] = pvt [(j*dim_x) + ((i*2)+1)];
        positioners_infos [i].vel [j]     = pvt [(j*dim_x) + ((i*2)+2)];
      }
    }
    traj_state = TRAJ_INPUT_PROCESSED;
  }

  // ============================================================================
	// Trajectory::load_abs_pvt
  // input : ABSOLUTE Times and Positions
	// ============================================================================
  void Trajectory::load_abs_pvt (long dim_x, long dim_y, std::vector <double> & pvt)
	throw (Tango::DevFailed)
  {
		//std::cout << "Trajectory::load_abs_pvt" << std::endl;
    //-------------------------------------------------------------------------------------------------------------
    //- we receive a trajectory as <Abs Time0> <Pos Abs M1> <Vel M1> <Pos Abs M2> <Vel M2> ...<Pos Abs Mn> <Vel Mn>  
    //- we compute relative times and relative distances at each point
    //-------------------------------------------------------------------------------------------------------------

    //- throws exception if something was wrong in argins or class state, do not catch it
	  this->traj_type = TRAJ_ABSOLUTE_POS;

    this->check_pvt_input (dim_x, dim_y, pvt);
#ifndef TU_TRAJECTORY_TESTS
    //- OK? clear the XPS and internal buffers
    this->clear ();
#endif
  this->initialize_member_variables (dim_y);


    //- spécifique PVT (l'image en entrée contient temps, et couples (position, vitesse))
    //- copy absolute times from PVT
    for (int i = 0; i < dim_y; i++)
      traj_times_abs [i] = pvt [i*dim_x];

    //- copy positions and velocities in positioners_infos [i].abs_pos[j] and .vel[j]
    for (size_t i = 0; i < positioners_infos.size (); i++)
    {
      for (int j = 0; j < dim_y; j++)
      {
        positioners_infos [i].abs_pos [j] = pvt [(j*dim_x) + (1+(i*2))];
        positioners_infos [i].vel [j] = pvt [(j*dim_x) + (2+(i*2))];
      }
    }

//-------------------------------------PVT: debut calcul points start et end----------------------------------------------------------
    //- compute the pre time max debut ----------------------------------------
    double pre_time_max = 0.;
    double post_time_max = 0.;
    //- calculation of max times for start and stop positions
    for (size_t i = 0; i < positioners_infos.size (); i++)
    {
      //- check for DIV/0
			if (positioners_infos [i].max_accel == 0.)
			{
         //std::cerr << "Trajectory::abs_pvt OUT_OF_RANGE acceleration = 0." << std::endl;
         THROW_DEVFAILED ("OUT_OF_RANGE",
                          "acceleration = 0.",
                          "Trajectory::abs_pvt");
      }
      //- vitesse d'entree
      positioners_infos [i].pre_vel = positioners_infos [i].vel [0];
      //- we apply a ratio of MAX_ACC_RATIO on max_accel to avoid roundoff errors when sending ASCII cmds to XPS
      double start_dt = ::fabs ((positioners_infos [i].pre_vel) / (positioners_infos [i].max_accel * MAX_ACC_RATIO));
      pre_time_max = pre_time_max > start_dt? pre_time_max : start_dt;

      //- end point
      positioners_infos [i].post_vel = positioners_infos [i].vel [dim_y-1];
      double end_dt = ::fabs ((positioners_infos [i].post_vel) / (positioners_infos [i].max_accel * MAX_ACC_RATIO));
      post_time_max = post_time_max > end_dt? post_time_max : end_dt;

      /*
	  std::cout << "Trajectory::load_abs_pvt positioner <" 
                << positioners_infos [i].group_name << "." << positioners_infos [i].positioner_name
                << "> pre_time_max <" << pre_time_max
                << "> post_time_max <" << post_time_max
                << ">" << std::endl;
				*/
    }
    //- ensure the minimum time for acceleration trajectories is respected
    pre_time_max = pre_time_max > XPS_MIN_PROFILE_ACCEL_TIME ? pre_time_max : XPS_MIN_PROFILE_ACCEL_TIME;
    post_time_max = post_time_max > XPS_MIN_PROFILE_ACCEL_TIME ? post_time_max : XPS_MIN_PROFILE_ACCEL_TIME;
    //- compute the pre time max fin ---------------------------------------

    //- FIRST STEP fill "relative" times vector (e.g time for the distance)
    this->fill_relative_times_vector (pre_time_max, post_time_max, dim_y);

    this->fill_positions_vector (pre_time_max, post_time_max);
#ifndef TU_TRAJECTORY_TESTS
    //- check absolute trajectory and the absolute start and stop positions after they have been computed
    this->check_abs_traj_for_user_limits ();
#endif
    this->display_trajectory ();

    //- OKAYYYYY cool man
    traj_state = TRAJ_INPUT_PROCESSED;
#ifndef TU_TRAJECTORY_TESTS
    //- upload the trajectory
    this->trajectory_upload ();
    //- request XPS to check the trajectory for possible errors
    this->check ();
#endif	
    //- go to origin
    //    this->go_to_origin ();
  }


  // ============================================================================
	// Trajectory::load_abs_pt
  // input : ABSOLUTE Times and Positions
	// ============================================================================
  void Trajectory::load_abs_pt (long dim_x, long dim_y, std::vector <double> & pt)
	throw (Tango::DevFailed)
  {
		//std::cout << "Trajectory::load_abs_pt" << std::endl;

    //----------------------------------------------------------------------------------
    //- we receive a trajectory as <Abs Time0> <Pos Abs M1> <Pos Abs M2> ...<Pos Abs Mn> 
    //- we compute relative times, relative distances and velocities at each point
    //----------------------------------------------------------------------------------

	  this->traj_type = TRAJ_ABSOLUTE_POS;
    //- check the arguments (number, size,...)
    //- dim_x * dim_y = pt.size
    //- dim_x = Time + Number of positioners in the group
    //- throws exception if something was wrong in argins or class state, do not catch it
    this->check_abs_pt_input (dim_x, dim_y, pt);

    //- la trajectoire est fournie en points "utiles" cad en mouvement  
#ifndef TU_TRAJECTORY_TESTS
    //- OK? clear the XPS and internal buffers
    this->clear ();
#endif
  this->initialize_member_variables (dim_y);

    //- spécifique PT (l'image en entrée contient temps, et positions)
    //- copy "absolute" times from PT (le premier point devrait etre 0 mais si user met une valeur tout redecaler de cette valeur)
    for (int i = 0; i < dim_y; i++)
      traj_times_abs [i] = pt [i*dim_x] - pt[0];

    //- copy positions in positioners_infos [i].abs_pos[j] and set vel[j] to 0
    for (size_t i = 0; i < positioners_infos.size (); i++)
    {
      for (int j = 0; j < dim_y; j++)
      {
        positioners_infos [i].abs_pos [j] = pt [(j*dim_x) + (1+i)];
        positioners_infos [i].vel [j] = 0.;
      }
    }

//-------------------------------------PT: debut calcul points start et end--------------------------------------------------------------
    //- compute the start, stop points, velocities and time
    double pre_time_max = 0.;
    double post_time_max = 0.;
    //- calculation of start time, start velocity, end time, end velocity (for the start ant end positions)
    for (size_t i = 0; i < positioners_infos.size (); i++)
    {
		//- check for DIV/0
		if (positioners_infos [i].max_accel == 0.)
		{
			//std::cerr << "Trajectory::abs_pt OUT_OF_RANGE acceleration = 0." << std::endl;
			THROW_DEVFAILED ("OUT_OF_RANGE",
						"acceleration = 0.",
						"Trajectory::abs_pt");
		}
	  
		//- On calcule la distance a parcourir sur le premier segment
		double first_segment_dx = positioners_infos [i].abs_pos [1] - positioners_infos [i].abs_pos [0];
		// On calcule le temps de parcours de ce premier segment
		double first_segment_dt = traj_times_abs [1] - traj_times_abs [0];
		if (first_segment_dt == 0.)
		{
			//std::cerr << "Trajectory::abs_pt OUT_OF_RANGE start delta time = 0." << std::endl;
			THROW_DEVFAILED ("OUT_OF_RANGE",
							"start delta time = 0.",
							"Trajectory::abs_pt");
		}
		// On doit maintenant calculer la vitesse d'entrée (premier point utile)
		positioners_infos [i].pre_vel = first_segment_dx / first_segment_dt;
		//- we apply a ratio of MAX_ACC_RATIO on max_accel to avoid roundoff errors when sending ASCII cmds to XPS
		double start_dt = ::fabs ((positioners_infos [i].pre_vel) / (positioners_infos [i].max_accel * MAX_ACC_RATIO));
		// On garde le temps le plus long des positioners
		pre_time_max = pre_time_max > start_dt? pre_time_max : start_dt;

		//- On calcule la distance a parcourir sur le dernier segment
		double end_segment_dx = positioners_infos [i].abs_pos [dim_y - 1] - positioners_infos [i].abs_pos [dim_y - 2];
		// On doit maintenant calculer le temps de parcours du dernier segment
		double end_segment_dt = traj_times_abs [dim_y-1] - traj_times_abs [dim_y-2];
		if (end_segment_dt == 0.)
		{
			//std::cerr << "Trajectory::abs_pt OUT_OF_RANGE end delta time = 0." << std::endl;
			THROW_DEVFAILED ("OUT_OF_RANGE",
							"end delta time = 0.",
							"Trajectory::abs_pt");
		}
		
		// On doit maintenant calculer la vitesse du dernier point utile de la trajectoire
	    positioners_infos [i].post_vel = end_segment_dx / end_segment_dt;
			
		double end_dt = ::fabs ((positioners_infos [i].post_vel) / (positioners_infos [i].max_accel * MAX_ACC_RATIO));
		post_time_max = post_time_max > end_dt? post_time_max : end_dt;
    }
	
    //- ensure the minimum time for acceleration trajectories is respected
    pre_time_max = pre_time_max > XPS_MIN_PROFILE_ACCEL_TIME ? pre_time_max : XPS_MIN_PROFILE_ACCEL_TIME;
    post_time_max = post_time_max > XPS_MIN_PROFILE_ACCEL_TIME ? post_time_max : XPS_MIN_PROFILE_ACCEL_TIME;


    //- FIRST STEP : fill "relative" times vector (e.g time for the distance)
    this->fill_relative_times_vector (pre_time_max, post_time_max, dim_y);
 
    this->fill_positions_vector (pre_time_max, post_time_max);
#ifndef TU_TRAJECTORY_TESTS
    //- check absolute trajectory and the absolute start and stop positions after they have been computed
    this->check_abs_traj_for_user_limits ();
#endif

    //- Specific to load_abs_pt : compute the velocities
    double T0 = 0.;
    double T1 = 0.;
    double D0 = 0.;
    double D1 = 0.;
    for (size_t i = 0; i < positioners_infos.size (); i++)
    {
		//- the 1rst point position
		positioners_infos [i].vel [0] = positioners_infos [i].pre_vel;

		//- the trajectory itself

		for (int j = 1; j < dim_y; j++)
		{
			//- velocity = (T0+T1) / (D0+D1)
			T0 = traj_times_rel [j-1];
			D0 = positioners_infos [i].rel_pos [j-1];
			T1 = traj_times_rel [j];
			D1 = positioners_infos [i].rel_pos [j];

			//- T0 = traj_times_rel [j];
			//- D0 = positioners_infos [i].rel_pos [j];
			//- special case for last "useful" segment
			//-  T1 = (j < dim_y -1)? traj_times_rel [j+1] : traj_times_rel [j];
			//- D1 = (j < dim_y -1)? positioners_infos [i].rel_pos [j+1] : positioners_infos [i].rel_pos [j];

			positioners_infos [i].vel [j] = ((D0+D1) / (T0+T1));
			/* std::cout << positioners_infos [i].positioner_name << ".vel [" << j 
					  << "] T0 <" << T0 << "> T1 <" << T1 << "> D0 <" << D0 << "> D1 <" << D1 
					  << "> (T0+T1) <" << (T0+T1) << "> (D0+D1) <" << (D0+D1) 
					  << "> (D0+D1)/(T0+T1) <" << (D0+D1)/(T0+T1) << ">" << std::endl;
			*/
		}
      //- the last point, velocity is 0
      positioners_infos [i].vel [dim_y] = 0.;
    }

    //- DEBUG afficher la trajectoire 
    this->display_trajectory ();


    //- OKAYYYYY cool man
    traj_state = TRAJ_INPUT_PROCESSED;
#ifndef TU_TRAJECTORY_TESTS	

    //- upload the trajectory
    this->trajectory_upload ();
#endif
    //- request XPS to check the trajectory for possible errors
    //this->check ();
		// TODO remettre l'envoi en position initiale?
    //- go to origin
//    this->go_to_origin ();
  }


  // ============================================================================
	// Trajectory::check_abs_traj_for_user_limits
  // checks in absolute positions for soft limits
	// ============================================================================
  void Trajectory::check_abs_traj_for_user_limits (void)
  {
    //- ca ne marche que pour les trajectoires absolues!
    //- on checke la trajectoire pour les user limits 
    for (size_t i = 0; i < positioners_infos.size (); i++)
    {
      //- start point
      if ((positioners_infos [i].start_pos_abs <= positioners_infos [i].soft_limit_min) ||
          (positioners_infos [i].start_pos_abs >= positioners_infos [i].soft_limit_max))
      {
        traj_state = TRAJ_INPUT_ERROR;
        std::stringstream s;
         s << "> positioner <" << positioners_infos [i].positioner_name 
           << "> start position <" << positioners_infos [i].start_pos_abs
           << "> min limit <" << positioners_infos [i].soft_limit_min 
           << "> or max limit <" << positioners_infos [i].soft_limit_max
           << "> out of range"; 
         //std::cerr << "Trajectory::check_abs_traj_for_user_limits OUT_OF_RANGE " << s.str () << std::endl;
         THROW_DEVFAILED ("OUT_OF_RANGE",
                          s.str ().c_str (),
                          "Trajectory::check_abs_traj_for_user_limits");
      }
      //- end point
      if ((positioners_infos [i].end_pos_abs <= positioners_infos [i].soft_limit_min) ||
          (positioners_infos [i].end_pos_abs >= positioners_infos [i].soft_limit_max))
      {
        traj_state = TRAJ_INPUT_ERROR;
        std::stringstream s;
         s << "> positioner <" << positioners_infos [i].positioner_name 
           << "> end position <" << positioners_infos [i].end_pos_abs
           << "> min limit <" << positioners_infos [i].soft_limit_min 
           << "> or max limit <" << positioners_infos [i].soft_limit_max
           << "> out of range"; 
         //std::cerr << "Trajectory::check_abs_traj_for_user_limits OUT_OF_RANGE " << s.str () << std::endl;
         THROW_DEVFAILED ("OUT_OF_RANGE",
                          s.str ().c_str (),
                          "Trajectory::check_abs_traj_for_user_limits");
      }
      for (size_t j = 0; j < positioners_infos [i].abs_pos.size (); j++)
      {
        
        if ((positioners_infos [i].abs_pos [j] <= positioners_infos [i].soft_limit_min) ||
            (positioners_infos [i].abs_pos [j] >= positioners_infos [i].soft_limit_max))
        {
          traj_state = TRAJ_INPUT_ERROR;
          std::stringstream s;
           s << "> positioner <" << positioners_infos [j].positioner_name 
             << "> position [" << j <<  "] <" << positioners_infos [i].abs_pos [j]
             << "> out of soft limits [" << positioners_infos [j].soft_limit_min 
             << "..." << positioners_infos [j].soft_limit_max
             << "]"; 
           //std::cerr << "Trajectory::check_abs_traj_for_user_limits OUT_OF_RANGE " << s.str () << std::endl;
           THROW_DEVFAILED ("OUT_OF_RANGE",
                            s.str ().c_str (),
                            "Trajectory::check_abs_traj_for_user_limits");
        }
        
      } //- end for all abs points
    } //- end check soft limits
  }



	// ============================================================================
	// Trajectory::check_pvt_input 
  //- works for both absolute trajectories ans relariver (raw) trajectories
  //- checks for trajectory state 
  //- checks for size of pvt data
	// ============================================================================
  void Trajectory::check_pvt_input (long dim_x, long dim_y, std::vector <double> & pvt)
  {
		//std::cout << "Trajectory::check_pvt_input" << std::endl;
    //- check class initialization
    if (class_state != SW_CONFIGURED)
    {
      //std::cerr << "Trajectory::check_pvt_input error class not properly configured check configurations, try to restart device]\n";
      THROW_DEVFAILED ("OPERATION_NOT_ALLOWED",
                       "class not properly configured check configurations, try to restart device]",
                       "Trajectory::check_pvt_input");
    }
    //- check the XPS state
    if (traj_state == TRAJ_LOADING ||
        traj_state == TRAJ_GOING_TO_START_POINT ||
        traj_state == TRAJ_RUNNING_TRAJ ||
        traj_state == TRAJ_CHECKING)
    {
      //std::cerr << "Trajectory::check_pvt_input error : trajectory class and XPS not cleared [clear first]\n";
      THROW_DEVFAILED ("OPERATION_NOT_ALLOWED",
                       "error : trajectory class and XPS not cleared [clear first]",
                       "Trajectory::check_pvt_input");
    }
    //- check x dimension = temps + P1 V1 + ... + Pn Vn
    
    if (dim_x != (1 + nb_positioners * 2))
    {
      traj_state = TRAJ_INPUT_ERROR;
      //std::cerr << "Trajectory::check_pvt_input error : dim x failure [waiting for t,P1,V1,P2,V2,..Pn,Vn]\n";
      //std::cerr << "Trajectory::check_pvt_input error : dim x <" << dim_x << "> nb positioners <" << nb_positioners << ">\n";
      THROW_DEVFAILED ("OPERATION_NOT_ALLOWED",
                       "error : dim x failure [waiting for t,P1,V1,P2,V2,..Pn,Vn]",
                       "Trajectory::check_pvt_input");
    }
    //- check data size
    if (((pvt.size () % (nb_positioners * 2 + 1)) != 0) || ((pvt.size () / (nb_positioners * 2 + 1)) != dim_y))
    {
      traj_state = TRAJ_INPUT_ERROR;
      //std::cerr << "Trajectory::check_pvt_input error : dim y failure [data missing]\n";
      THROW_DEVFAILED ("OPERATION_NOT_ALLOWED",
                       "error : dim y failure [data missing]",
                       "Trajectory::check_pvt_input");
    }

  }

	// ============================================================================
	// Trajectory::check_abs_pt_input 
  //- works for PT trajectories (the class computes the velocities at each point)
  //- checks for trajectory state 
  //- checks for size of pvt data
	// ============================================================================
  void Trajectory::check_abs_pt_input (long dim_x, long dim_y, std::vector <double> & pt)
  {
		//std::cout << "Trajectory::check_abs_pt_input" << std::endl;
    //- check class initialization
    if (class_state != SW_CONFIGURED)
    {
      //std::cerr << "Trajectory::check_abs_pt_input error class not properly configured check configurations, try to restart device]\n";
      THROW_DEVFAILED ("OPERATION_NOT_ALLOWED",
                       "class not properly configured check configurations, try to restart device]",
                       "Trajectory::check_abs_pt_input");
    }
    //- check the XPS state
    if (traj_state == TRAJ_LOADING ||
        traj_state == TRAJ_GOING_TO_START_POINT ||
        traj_state == TRAJ_RUNNING_TRAJ ||
        traj_state == TRAJ_CHECKING)
    {
      //std::cerr << "Trajectory::check_abs_pt_input error : trajectory class and XPS not cleared [clear first]\n";
      THROW_DEVFAILED ("OPERATION_NOT_ALLOWED",
                       "error : trajectory class and XPS not cleared [clear first]",
                       "Trajectory::check_abs_pt_input");
    }
    //- check x dimension = temps + P1 + ... + Pn
    
    if (dim_x != (1 + nb_positioners))
    {
      traj_state = TRAJ_INPUT_ERROR;
      //std::cerr << "Trajectory::check_abs_pt_input error : dim x failure [waiting for t,P1,P2,..Pn]\n";
      //std::cerr << "Trajectory::check_abs_pt_input error : dim x <" << dim_x << "> nb positioners <" << nb_positioners << ">\n";
      THROW_DEVFAILED ("OPERATION_NOT_ALLOWED",
                       "error : dim x failure [waiting for t,P1,P2,..Pn]",
                       "Trajectory::check_abs_pt_input");
    }
    if (dim_y < 2)
    {
      traj_state = TRAJ_INPUT_ERROR;
      //std::cerr << "Trajectory::check_abs_pt_input error : dim y failure : nb lines < 2\n";
      THROW_DEVFAILED ("OPERATION_NOT_ALLOWED",
                       "error : dim y failure : nb lines < 2",
                       "Trajectory::check_abs_pt_input");
    }
    //- check data size
    if (((pt.size () % (nb_positioners + 1)) != 0) || ((pt.size () / (nb_positioners + 1)) != dim_y))
    {
      traj_state = TRAJ_INPUT_ERROR;
      //std::cerr << "Trajectory::check_abs_pt_input error : dim y failure [some data missing?]\n";
      THROW_DEVFAILED ("OPERATION_NOT_ALLOWED",
                       "error : dim y failure [some data missing?]",
                       "Trajectory::check_abs_pt_input");
    }
  }

	// ============================================================================
	// Trajectory::fill_relative_times_vector 
  //- code factorisation for load_abs_pt, load_abs_pvt
	// ============================================================================
  void Trajectory::fill_relative_times_vector (double pre_time_max, double post_time_max, long dim_y)
  {
    //- the first segment is from "start point" to first point of the traj
    traj_times_rel [0] = pre_time_max;
    //- assuming first "absolute" time is 0
    for (int i = 1; i < dim_y; i++)
      traj_times_rel [i] = traj_times_abs [i] - traj_times_abs [i-1];
    //- last time is from last point provided to stop point
    traj_times_rel [dim_y] = post_time_max;
    return;
  }

	// ============================================================================
	// Trajectory::fill_positions_vector 
  //- code factorisation for load_abs_pt, load_abs_pvt
	// ============================================================================
  void Trajectory::fill_positions_vector (double pre_time_max, double post_time_max)
  {
    //- set the distances for start and end points and origin point
    long dim_y = traj_times_abs.size ();
    for (size_t i = 0; i < positioners_infos.size (); i++)
    {
      positioners_infos [i].start_pos_rel = 0.75 * positioners_infos [i].pre_vel  * pre_time_max;
      positioners_infos [i].start_pos_abs = positioners_infos [i].abs_pos [0] - positioners_infos [i].start_pos_rel;

      positioners_infos [i].end_pos_rel   = 0.75 * positioners_infos [i].post_vel * post_time_max;
      positioners_infos [i].end_pos_abs   = positioners_infos [i].abs_pos [dim_y-1] + positioners_infos [i].end_pos_rel;
    }
    //- compute the relative distances for the trajectory
    for (size_t i = 0; i < positioners_infos.size (); i++)
    {
      //- the 1rst point position, velocity is already provided by user
      positioners_infos [i].rel_pos [0] = positioners_infos [i].start_pos_rel;
      for (int j = 1; j < dim_y; j++)
      {
        positioners_infos [i].rel_pos [j] = positioners_infos [i].abs_pos [j] - positioners_infos [i].abs_pos [j-1];
      }
      //- the last point, velocity is 0
      positioners_infos [i].rel_pos [dim_y] = positioners_infos [i].end_pos_rel;
      positioners_infos [i].vel [dim_y] = 0.;
    }
    return;
  }

	// ============================================================================
	// Trajectory::display_relative_trajectory 
  //- code factorisation for load_abs_pt, load_abs_pvt
	// ============================================================================
  void Trajectory::display_trajectory (void)
  {
    //- afficher point départ/arrivée absolus
#ifdef TU_TRAJECTORY_TESTS	

    for (size_t i = 0; i < positioners_infos.size (); i++)
    {
 
	  std::cout << "Trajectory::display_trajectory  <" 
                << positioners_infos [i].group_name << "." << positioners_infos [i].positioner_name
                << " first point abs. coord. <" << positioners_infos [i].start_pos_abs
                << " last point  abs. coord. <" << positioners_infos [i].end_pos_abs
                << ">" << std::endl;

    }

    //- Afficher la trajectoire (temps/positions relatives) complete
    //std::cout << "DEBUG: Trajectory::display_relative_trajectory : display the relative trajectory\n";
    for (size_t i = 0; i < traj_times_rel.size (); i++)
    {
     std::cout << "[dT<" << traj_times_rel [i] << ">; ";
      for (size_t j = 0; j < positioners_infos.size (); j++)
      {
       std::cout << "dP[" << j << "]<" << positioners_infos [j].rel_pos [i] << ">;";
       std::cout << " V[" << j << "]" <<positioners_infos [j].vel [i] << ">; ";
      }
     std::cout << "]" << std::endl;
    } 
#endif	
    return;
  }
// =============================================================================================================  
//- CTRLRFC-119: Improve PT transformation algorithm -> rawPVT to allow continuous scans in the XPS
// =============================================================================================================
// ============================================================================
// Trajectory::load_sixs_py_algorithm_abs_pt
// input :      ABSOLUTE Times and Positions same as native algorithm
//
// Description: This algorithm uses times and positions as inputs.
//              The relative Time and Velocity are calculated at each point of the trajectory.   
//              2 calculated points are added :
//                   * one at the trajectory start  (positions and velocities) 
//                   * one at the trajectory end (positions and velocities)  . 
// ============================================================================
  void Trajectory::load_sixs_py_algorithm_abs_pt (long dim_x, long dim_y, std::vector <double> & pt)
	throw (Tango::DevFailed)
  {
	//std::cout << "Trajectory::load_sixs_py_algorithm_abs_pt" << std::endl;
    //-------------------------------------------------------------------------------------------------------------
    //- we receive a trajectory as <Abs Time0> <Pos Abs M1> <Pos Abs M2> ...<Pos Abs Mn>  
    //-------------------------------------------------------------------------------------------------------------

    //- throws exception if something was wrong in argins or class state, do not catch it
	this->traj_type = TRAJ_ABSOLUTE_POS;
	  
	//- check dim_x, dim_y and pt size integrity
    this->check_abs_pt_input (dim_x, dim_y, pt);
#ifndef TU_TRAJECTORY_TESTS	
    //- OK? clear the XPS and internal buffers
    this->clear ();
#endif

	//- intialize the members avariables for new py algorithm
	this->initialize_sixs_py_algorithm_member_variables (dim_y);

    //- Intialize PT with trajectory times
	//- index 0 to dim_y, from the pt input matrix to vector<traj_times_abs>
	
    for (int i = 0; i < dim_y; i++)
		traj_times_abs [i + 1] = pt [i*dim_x];

    //- copy positions and velocities in positioners_infos [i].abs_pos[j] and .vel[j]
    for (size_t i = 0; i < positioners_infos.size (); i++)
    {
		for (int j = 0; j < dim_y; j++)
		{
			// Position: index 1 to dim_y, from the pt input matrix to vector<abs_pos>
			positioners_infos [i].abs_pos [j + 1] = pt [1 + i + j*dim_x]; 
		}
    }
	
	// Calculate the trajectory initials informations
	//------------------------------------------------
	//- The initial max speed among all the positioners
	double initial_max_speed = 0.;
	for (size_t i = 0; i < positioners_infos.size (); i++)
    {
        double speed = (positioners_infos [i].abs_pos [2] -  positioners_infos [i].abs_pos [1])
                       / (traj_times_abs[2] - traj_times_abs[1]);

		positioners_infos [i].initial_speed = speed;
		
		if (std::fabs(speed) > initial_max_speed)
		{
		  initial_max_speed = std::fabs(speed);
		}
	}

	//- Determine the initial time using 
	// py algorithm constante max acceleration
	//------------------------------------------
	double dt_initial = initial_max_speed/kPOSITIONERS_MAX_ACCEL; // try with global constant for max acceleration

	//- Calculate the absolute traj_times
	for (int j = 0; j < dim_y ; j++)
		traj_times_abs[j +1] = traj_times_abs[j + 1] + dt_initial;
		
	//- Traces	
	//for (int j = 0; j < dim_y+2 ; j++)
		//std::cout<< "[dt_abs ["<<j<<"] <"<<traj_times_abs[j]<<">"<<endl;
		
	//- Determine the trajectory finals informations
	//------------------------------------------------
	//- The final max speed among all the positioners
	double final_max_speed = 0.;
	for (size_t i = 0; i < positioners_infos.size (); i++)
    {
        double speed = (positioners_infos [i].abs_pos [dim_y] -  positioners_infos [i].abs_pos [dim_y - 1])
                       / (traj_times_abs[dim_y] - traj_times_abs[dim_y - 1]);
        
		positioners_infos [i].final_speed = speed;
		
		if (std::fabs(speed) > final_max_speed)
		{
		  final_max_speed = std::fabs(speed);
		}
	}

	//- Determine the final time
	// py algorithm constante max acceleration
	//------------------------------------------
	double dt_final = final_max_speed/kPOSITIONERS_MAX_ACCEL;
	
	//- Calculate the time at the last point of the trajectory
	//---------------------------------------------------------
	traj_times_abs[dim_y + 1] = traj_times_abs[dim_y] + dt_final;
	
	//- Calculate the initial and final position of the trajectory,
	//---------------------------------------------------------------------
	for (size_t i = 0; i < positioners_infos.size (); i++)
    {
        double start;
        
		// for initial positions on each positioner
		start = positioners_infos [i].abs_pos [1] - (0.5 * dt_initial * positioners_infos [i].initial_speed);
		positioners_infos [i].abs_pos [0] = start;  
        positioners_infos [i].start_pos_abs = start;

		// for final positions on each positioner
		positioners_infos [i].abs_pos [dim_y + 1] = positioners_infos [i].abs_pos [dim_y] + (0.5 * dt_final * positioners_infos [i].final_speed);  
	}	
	
	//- Calculate the speeds
	//---------------------------------
	for (size_t i = 0; i < positioners_infos.size (); i++)
    {
		for (int j = 0; j < dim_y; j++)
		{
			positioners_infos [i].vel[j + 1] = (positioners_infos [i].abs_pos [j + 2] - positioners_infos [i].abs_pos [j])/ 
											 (traj_times_abs[j + 2] - traj_times_abs[j]);
		}									 
		//- speed after the start point of the trajectory 
		positioners_infos [i].vel[1] = positioners_infos [i].vel[2];
		
		//- speed before the last point of the trajectory
		positioners_infos [i].vel[dim_y] = positioners_infos [i].vel[dim_y - 1];
	}

    //- FIRST STEP fill "relative" times vector (e.g time for the distance)
    this->fill_relative_times_vector (dt_initial, dt_final, dim_y + 1);
	 
	//- fill trajectory relative vector informations
    this->fill_py_algorithm_relative_vector();

    //- check absolute trajectory and the absolute start and stop positions after they have been computed
    this->check_sixs_py_algorithm_abs_traj_for_user_limits ();

    this->display_sixs_py_algorithm_trajectory (dim_y);

    //- 
    traj_state = TRAJ_INPUT_PROCESSED;
	
#ifndef TU_TRAJECTORY_TESTS	
    //- upload the trajectory to XPS controller
    this->trajectory_upload ();

    //- request XPS controller to check the trajectory for possible errors
    this->check ();
#endif	
  }
  
 // ============================================================================
 // Trajectory::initialize_member_variables  
 //- code factorisation for load_abs_pt, load_abs_pvt
 // ============================================================================
  void Trajectory::initialize_member_variables (long dim_y)
  {
    //- initialize some stuff
    traj_times_abs.resize (dim_y);
    //- 1 element de plus car on rajoute 1 element pour integrer start point + end point en relatif
    traj_times_rel.resize (dim_y + 1);

    for (size_t i = 0; i < positioners_infos.size (); i++)
    {
      //- set the size of position and velocity vectors (faster memory allocation)
      positioners_infos [i].abs_pos.resize (dim_y);
      positioners_infos [i].rel_pos.resize (dim_y + 1);
      positioners_infos [i].vel.resize     (dim_y + 1);
      positioners_infos [i].start_pos_rel = 0.;
      positioners_infos [i].end_pos_rel = 0.;
      positioners_infos [i].start_pos_abs = 0.;
      positioners_infos [i].end_pos_abs = 0.;
    }
    return;
  }
 
  
// ============================================================================
// Trajectory::initialize_sixs_py_algorithm_member_variables(implementation like python ) 
//- 
// ============================================================================
  void Trajectory::initialize_sixs_py_algorithm_member_variables (long dim_y)
  {
    // - trajectories times vector
	traj_times_rel.clear();
	traj_times_abs.clear();
	
    traj_times_rel.resize (dim_y + 2);
	traj_times_abs.resize (dim_y + 2);
	
    for (size_t i = 0; i < positioners_infos.size (); i++)
    {
		//- set the size of position and velocity vectors 
		positioners_infos [i].abs_pos.clear();
		positioners_infos [i].rel_pos.clear();
		positioners_infos [i].vel.clear();
		
		positioners_infos [i].abs_pos.resize       (dim_y + 2);
		positioners_infos [i].rel_pos.resize       (dim_y + 2);
		positioners_infos [i].vel.resize           (dim_y + 2);
		
		positioners_infos [i].initial_speed = 0.; //- for calculation of the initial speed (recul)
		positioners_infos [i].final_speed = 0.; //- for calculation of the final speed (freinage)
      
    }
    return;
  }
 
// ============================================================================
// Trajectory::check_sixs_py_algorithm_abs_traj_for_user_limits
// checks in absolute positions for soft limits
// ============================================================================
  void Trajectory::check_sixs_py_algorithm_abs_traj_for_user_limits (void)
	throw (Tango::DevFailed)
  {
	size_t i,j;
	try
	{
		//- ca ne marche que pour les trajectoires absolues!
		//- Check all positions of the trajectory are among user limits 
		for (i = 0; i < positioners_infos.size (); i++)
		{
#ifdef TU_TRAJECTORY_TESTS	       	
std::cout<<"positioner<"<<i<<">     Limits ["<<positioners_infos [i].soft_limit_min<<", "<<positioners_infos [i].soft_limit_max<<"]"<<endl;
#endif
			//- All others positions in the trajectory	
			for (j = 0; j < positioners_infos [i].abs_pos.size (); j++)
			{
				if ((positioners_infos [i].abs_pos [j] <= positioners_infos [i].soft_limit_min) ||
					(positioners_infos [i].abs_pos [j] >= positioners_infos [i].soft_limit_max))
				{
					traj_state = TRAJ_INPUT_ERROR;
					std::stringstream s;
					s << "> positioner <" << positioners_infos [i].positioner_name 
   					  << "> position [" << j <<  "] <" << positioners_infos [i].abs_pos [j]
					  << "> out of soft limits [" << positioners_infos [j].soft_limit_min 
					  << "..." << positioners_infos [i].soft_limit_max
					  << "]"; 
				    //std::cerr << "Trajectory::check_sixs_py_algorithm_abs_traj_for_user_limits OUT_OF_RANGE " << s.str () << std::endl;
				    THROW_DEVFAILED ("OUT_OF_RANGE",
					 				s.str ().c_str (),
									"Trajectory::check_sixs_py_algorithm_abs_traj_for_user_limits");
				}
	#ifdef TU_TRAJECTORY_TESTS	       
	std::cout<<"    abs     <"<<positioners_infos [i].abs_pos [j]<<"> "<<endl;
	#endif
			} //- end for all abs points
		} //- end check soft limits
	}
	catch(...)
	{
		std::stringstream s;
		s << "Invalid data for positioner <" << positioners_infos [i].positioner_name; 		    
	    THROW_DEVFAILED ("OUT_OF_RANGE",
					 	s.str ().c_str (),
						"Trajectory::check_sixs_py_algorithm_abs_traj_for_user_limits");
	}
  }
  
// ============================================================================
// Trajectory::fill_py_algorithm_relative_vector 
// Desc: fills the relative positions and trajectory time vectors   
// ============================================================================
  void Trajectory::fill_py_algorithm_relative_vector (void)
  {
  
	//- calculate trajectory times relatives
	
	for (size_t i = 1; i < traj_times_abs.size(); i++)
		traj_times_rel[i] = traj_times_abs[i] - traj_times_abs[i - 1];
	
    //- calculate the relative distances for the trajectory
    for (size_t i = 0; i < positioners_infos.size (); i++)
    {
		long dim_y = positioners_infos [i].abs_pos.size();
		
		for (int j = 0; j < dim_y; j++)
		{
			positioners_infos [i].rel_pos [j + 1] = positioners_infos [i].abs_pos[j + 1] - positioners_infos [i].abs_pos [j];
		}	
    }
    return;
  }
  
// ============================================================================
// Trajectory::display_sixs_py_algorithm_trajectory
// Desc:  displays the relative and absolute informations of the trajectory.
// param.: long dim_y
// ============================================================================
  void Trajectory::display_sixs_py_algorithm_trajectory(long dim_y)
  {
#ifdef TU_TRAJECTORY_TESTS    
	//- afficher point départ/arrivée absolus
    for (size_t i = 0; i < positioners_infos.size (); i++)
    {
		std::cout << "Trajectory::display_sixs_py_algorithm_trajectory <" 
                << positioners_infos [i].group_name << "." << positioners_infos [i].positioner_name
                << " first point abs. coord. <" << positioners_infos [i].abs_pos[0]
                << "> last point  abs. coord. <" << positioners_infos [i].abs_pos[dim_y + 1]
                << ">" << std::endl;
    }

    //- Afficher la trajectoire (temps/positions relatives) complete
    std::cout << "DEBUG: Trajectory::display_sixs_py_algorithm_trajectory : display trajectory informations\n";
    for (size_t i = 0; i < traj_times_rel.size (); i++)
    {
		std::cout << i <<" ... [T_rel<" << traj_times_rel [i] << ">, T_abs<"<< traj_times_abs [i] << "> ";
		for (size_t j = 0; j < positioners_infos.size (); j++)
		{
			std::cout << "P_rel[" << i << ", "<<j << "]<" << positioners_infos [j].rel_pos [i] << ">, "
			          << "P_abs[" << i << ", "<<j << "]<" << positioners_infos [j].abs_pos [i] << ">\t";
			std::cout << "V[" << i << ", "<< j << "]<" << positioners_infos [j].vel [i] << "> ";
		}
		std::cout << "]" << std::endl;
    }
#endif	
    return;
  }
 
  // ============================================================================
  // Trajectory::send_low_level_cmd
  // ============================================================================
  void Trajectory::exec_low_level_cmd(std::string cmd, std::string &outParam)
  {
      //- check class initialization
      if (class_state != SW_CONFIGURED)
      {
          //std::cerr << "Trajectory::exec_low_level_cmd error class not properly configured check configurations, try to restart device]\n";
          THROW_DEVFAILED("OPERATION_NOT_ALLOWED",
              "class not properly configured check configurations, try to restart device]",
              "Trajectory::exec_low_level_cmd");
      }

      char strOut[65535];

      int s_b = SocketPool::instance()->get_blocking_socket();
      int ret = ExecLowLevelCmd(s_b,
          const_cast <char*> (cmd.c_str()),
          &strOut[0]);

      throw_on_error(std::string("Trajectory::exec_low_level_cmd::ExecLowLevelCmd"), ret);

      outParam = std::string(strOut);
  }

} //- namespace

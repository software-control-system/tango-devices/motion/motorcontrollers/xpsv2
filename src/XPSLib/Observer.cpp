#include "Observer.h"

namespace xps_ns
{

  Observer::~Observer ()
  {
    //pour chaque objet observ�, 
    //on lui dit qu'on doit supprimer l'Observer courant
    const_iterator ite = m_list.end();

    for(iterator itb = m_list.begin(); itb!=ite; ++itb)
    {
      (*itb)->DelObs (this);
    }
  }

  void Observer::AddObs (Observable * obs)
  {
    m_list.push_back (obs);
  }

  void Observer::DelObs(Observable * obs)
  {
    //on enl�ve l'objet observ�.
    iterator it = std::find (m_list.begin(), m_list.end(), obs);
    if (it != m_list.end ())
      m_list.erase (it);
  }

  void Observable::AddObs (Observer * obs)
  {
    //on ajoute l'Observer � notre liste 
    m_list.push_back (obs);

    //et on lui donne un nouvel objet observ�.
    obs->AddObs (this);
  }

  void Observable::DelObs (Observer * obs)
  {
    //m�me chose que dans Observer::DelObs
    iterator it = find(m_list.begin(), m_list.end(), obs);
    if(it != m_list.end ())
      m_list.erase (it);
  }

  void Observable::Notify (void)
  {
    //on pr�vient chaque Observer que l'on change de valeur
    iterator itb = m_list.begin ();
    const_iterator ite = m_list.end ();
   
   for(; itb != ite ; ++itb)
   {
     (*itb)->Update ();
   }
  }

  Observable::~Observable ()
  {
    //m�me chose qu'avec Observer::~Observer
    iterator itb = m_list.begin ();
    const_iterator ite = m_list.end ();

    for(; itb != ite ; ++itb)
    {
      (*itb)->DelObs (this);
    }
  }
} //- namespace

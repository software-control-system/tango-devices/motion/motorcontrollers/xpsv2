

#ifndef __XPS_TRAJECTORY_H__
#define __XPS_TRAJECTORY_H__

#include <tango.h>
#include <string>
#include <vector>
#include <yat/threading/Mutex.h>

namespace xps_ns
{
// CTRLRFC-119
// ---------------------------
static double kPOSITIONERS_MAX_ACCEL = 0.5;

//-------------------------------------------
  static const std::string traj_state_str [14] =
{
  "Trajectory not initialized",
  "Trajectory initialized",
  "Trajectory input error",
  "Trajectory input processed",
  "Loading trajectory",
  "Trajectory loaded",
  "going to start point",
  "at start point",
  "ERROR when going at start point",
  "Trajectory checking",
  "Trajectory check OK",
  "Trajectory Moving",
  "Trajectory done",   
  "Trajectory check ERROR"
};
class Trajectory
{
public:
  //-------------------------------------------
  //- configs, enums, consts
  //-------------------------------------------
  typedef struct Config
  {
    std::string group_name;
    std::vector <std::string> positioner_names;
    Config ();
    Config (const Config & conf);
  //- operator = -------
  Config & operator = (const Config & src);
  } Config;
  //-------------------------------------------
  typedef enum
  {
  SW_UNKNOWN_STATE,
  SW_NOT_INITIALIZED,
  SW_INITIALIZED,
  SW_INITIALIZATION_ERROR,
  SW_NOT_CONFIGURED,
  SW_ALLOC_ERROR,
  SW_CONFIGURED,
  SW_CONFIGURATION_ERROR
  }ClassState;
  //-------------------------------------------
typedef enum
{
  TRAJ_NOT_INITIALIZED = 0,
  TRAJ_INITIALIZED,
  TRAJ_INPUT_ERROR,
  TRAJ_INPUT_PROCESSED,
  TRAJ_LOADING,
  TRAJ_LOADED,
  TRAJ_GOING_TO_START_POINT,
  TRAJ_AT_START_POINT,
  TRAJ_START_POINT_ERROR,
  TRAJ_CHECKING,
  TRAJ_CHECK_OK,
  TRAJ_RUNNING_TRAJ,
  TRAJ_DONE,
  TRAJ_CHECK_ERROR
}TrajState;

typedef enum
{
  TRAJ_NOT_INIT,
  TRAJ_ABSOLUTE_POS,
	TRAJ_RAW_POS
}TrajType;

  //-------------------------------------------
  typedef struct PositionerInfos
  {
    std::string group_name;
    std::string positioner_name;
	std::string full_name;
    int pos_number;
    double soft_limit_min;
    double soft_limit_max;
    double max_vel;
    double max_accel;
    double min_jerk_time;
    double max_jerk_time;

    double start_pos_abs;
    double start_pos_rel;
    double pre_vel;
    double initial_speed;// CTRLRFC-119

    double end_pos_abs;
    double end_pos_rel;
    double post_vel;
    double final_speed;// CTRLRFC-119

    std::vector <double> abs_pos;
    std::vector <double> rel_pos;
    std::vector <double> vel;
	
  }PositionerInfos;

  typedef struct MulAxesPVTVerResults
  {
    double min_pos;
    double max_pos;
    double max_vel;
    double max_accel;
		MulAxesPVTVerResults ();
  }MulAxesPVTVerResults;

  #define XPS_MIN_PROFILE_ACCEL_TIME 0.25


  //-------------------------------------------
	//- CTor -------------
  //-------------------------------------------
	Trajectory (const Config & conf);
  //-------------------------------------------
	//- DTor -------------
  //-------------------------------------------
	virtual ~Trajectory (void);

  void sw_run (void);

  //- void configure ();

  //- load raw trajectory as expected by XPS : delta time, delta Positions, velocities
  void load_raw_pvt (long dim_x, long dim_y, std::vector <double> & pvt)
	throw (Tango::DevFailed);
  //- load fully described trajectory, Absolute Time, Absolute Positions, Velocities
  void load_abs_pvt (long dim_x, long dim_y, std::vector <double> & pvt)
	throw (Tango::DevFailed);
  void load_sixs_py_algorithm_abs_pt (long dim_x, long dim_y, std::vector <double> & pt)
	throw (Tango::DevFailed);

  //- load partially described trajectory, time, Positions without vitesse (to be computed)
  void load_abs_pt (long dim_x, long dim_y, std::vector <double> & pt)
	throw (Tango::DevFailed);
	//-fix end of line for trajectory upload
	void set_trajectory_upload_eol(const std::string& eol);	
  //- upload in XPS the trajectory
  void trajectory_upload (void);
  //- go to trajectory origin
	void go_to_origin();
	//- requests a check for the current trajectory loaded in memory  
	void check (void);
	//- requests a start of the current trajectory loaded in memory  
	void start ();

  //- return the previously uploaded trajectory
  void get_uploaded_trajectory (long & dim_x, long & dim_y, std::vector<double> &raw_pvt);

  TrajState get_traj_state (void);
  ClassState get_class_state (void);

  //- clears the trajectory data in the class and the hw
  void clear (void);

  //- get 1 point as string 
  //- return true if point exists
 const char * get_traj_point_as_string (size_t index);
  int get_trajectory_size (void);

  //- execute low level command
  void exec_low_level_cmd(std::string cmd, std::string &outParam);

private:

  int nb_positioners;
  std::vector <PositionerInfos> positioners_infos;

  std::vector <double> traj_times_abs;
  std::vector <double> traj_times_rel;
  std::vector <double> uploaded_raw_trajectory_vect;

  //- checks the PVT inputs
  void check_pvt_input (long dim_x, long dim_y, std::vector <double> & pvt);

  //- checks the PT inputs
  void check_abs_pt_input (long dim_x, long dim_y, std::vector <double> & pt);
  
  //- checks for user limits
  void check_abs_traj_for_user_limits (void);
  void check_sixs_py_algorithm_abs_traj_for_user_limits (void)
	throw (Tango::DevFailed); //- CTRLRFC-119

  //- retreive positioners infos
  void get_positioners_infos (void);

  //- utility method to initalize member variables in load_abs_[pt|pvt]
  void initialize_member_variables (long dim_y);
  void initialize_sixs_py_algorithm_member_variables (long dim_y);//- CTRLRFC-119
  
  //- utility method to fill relative times vector in load_abs_[pt|pvt]
  void fill_relative_times_vector (double pre_time, double post_time, long dim_y);

  //- utility method to fill relative positions vector in load_abs_[pt|pvt]
  //- fills relative positions vector ans start, end absolute positions
  void fill_positions_vector (double pre_time, double post_time);

  //- fills relative positions and trajectory times vectors
  void fill_py_algorithm_relative_vector (void);//- CTRLRFC-119

  //- affichage de la trajectoire relative et des points de debut/fin en absolu
  void display_trajectory (void);
  void display_sixs_py_algorithm_trajectory (long dim_y); //- CTRLRFC-119

  
  yat::Mutex lock;
  yat::Mutex uploaded_traj_copy_protect;

  void throw_on_error (std::string origin, int ret_code);
	int state;
	std::string status;
  //- JGO no use //int socket_non_blocking;
  char ret_code_string [512];

  //- state/status stuffs
  ClassState class_state;
  TrajState traj_state;
	TrajType traj_type;
  std::string traj_upload_eol;
  //- configuration 
  Config conf;

};
} //- namespace
# endif //- __XPS_TRAJECTORY_H__


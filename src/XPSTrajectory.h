//=============================================================================
//
// file :        XPSTrajectory.h
//
// description : Include for the XPSTrajectory class.
//
// project :	XPSV2
//
// $Author:  $
//
// $Revision:  $
// $Date:  $
//
// SVN only:
// $HeadURL: $
//
// CVS only:
// $Source:  $
// $Log:  $
//
// copyleft :    Synchrotron SOLEIL 
//               L'Orme des merisiers - Saint Aubin
//		 BP48 - 91192 Gif sur Yvette
//               FRANCE
//
//=============================================================================
//
//  		This file is generated by POGO
//	(Program Obviously used to Generate tango Object)
//
//         (c) - Software Engineering Group - ESRF
//=============================================================================
#ifndef _XPSTRAJECTORY_H
#define _XPSTRAJECTORY_H

#include <tango.h>
#include <yat4tango/PropertyHelper.h>
#include "XPSLib/Trajectory.h"

//using namespace Tango;

/**
 * @author	$Author:  $
 * @version	$Revision:  $
 */

 //	Add your own constant definitions here.
 //-----------------------------------------------


namespace XPSTrajectory_ns
{

/**
 * Class Description:
 * Handles MultipleAxesGroup Trajectories features
 */

/*
 *	Device States Description:
 */


class XPSTrajectory: public Tango::Device_4Impl
{
public :
	//	Add your own data members here
	//-----------------------------------------


	//	Here is the Start of the automatic code generation part
	//-------------------------------------------------------------	
/**
 *	@name attributes
 *	Attribute member data.
 */
//@{
		Tango::DevBoolean	*attr_currentTrajectoryOK_read;
		Tango::DevDouble	attr_pvt_write;
		Tango::DevDouble	attr_pt_write;
		Tango::DevDouble	attr_rawPVT_write;
		Tango::DevDouble	*attr_uploadedRawPVT_read;
//@}

/**
 * @name Device properties
 * Device properties member data.
 */
//@{
/**
 *	Enumerated type Trajectory Algorithm:<BR>
 *	0 = native algorithm <BR>
 *	1 =  SIXS python script algorithm<BR>
 */
	Tango::DevShort	trajectoryAlgoType;
/**
 *	Define the end of line sent to the driver (MultipleAxesPVTLoadToMemory) when use Upload command:<BR>
 *	NUL (i.e \0 ) (OLD FIRMWARE)<BR>
 *	# (NEW FIRMWARE)<BR>
 *	
 */
	string	trajectoryUploadEOL;
//@}

/**
 *	@name Device properties
 *	Device property member data.
 */
//@{
//@}

/**@name Constructors
 * Miscellaneous constructors */
//@{
/**
 * Constructs a newly allocated Command object.
 *
 *	@param cl	Class.
 *	@param s 	Device Name
 */
	XPSTrajectory(Tango::DeviceClass *cl,string &s);
/**
 * Constructs a newly allocated Command object.
 *
 *	@param cl	Class.
 *	@param s 	Device Name
 */
	XPSTrajectory(Tango::DeviceClass *cl,const char *s);
/**
 * Constructs a newly allocated Command object.
 *
 *	@param cl	Class.
 *	@param s 	Device name
 *	@param d	Device description.
 */
	XPSTrajectory(Tango::DeviceClass *cl,const char *s,const char *d);
//@}

/**@name Destructor
 * Only one destructor is defined for this class */
//@{
/**
 * The object destructor.
 */	
	~XPSTrajectory() {delete_device();}
/**
 *	will be called at device destruction or at init command.
 */
	void delete_device();
//@}

	
/**@name Miscellaneous methods */
//@{
/**
 *	Initialize the device
 */
	virtual void init_device();
/**
 *	Always executed method before execution command method.
 */
	virtual void always_executed_hook();

//@}

/**
 * @name XPSTrajectory methods prototypes
 */

//@{
/**
 *	Hardware acquisition for attributes.
 */
	virtual void read_attr_hardware(vector<long> &attr_list);
/**
 *	Extract real attribute values for currentTrajectoryOK acquisition result.
 */
	virtual void read_currentTrajectoryOK(Tango::Attribute &attr);
/**
 *	Extract real attribute values for pvt acquisition result.
 */
	virtual void read_pvt(Tango::Attribute &attr);
/**
 *	Write pvt attribute values to hardware.
 */
	virtual void write_pvt(Tango::WAttribute &attr);
/**
 *	Extract real attribute values for pt acquisition result.
 */
	virtual void read_pt(Tango::Attribute &attr);
/**
 *	Write pt attribute values to hardware.
 */
	virtual void write_pt(Tango::WAttribute &attr);
/**
 *	Extract real attribute values for rawPVT acquisition result.
 */
	virtual void read_rawPVT(Tango::Attribute &attr);
/**
 *	Write rawPVT attribute values to hardware.
 */
	virtual void write_rawPVT(Tango::WAttribute &attr);
/**
 *	Extract real attribute values for uploadedRawPVT acquisition result.
 */
	virtual void read_uploadedRawPVT(Tango::Attribute &attr);
/**
 *	Read/Write allowed for currentTrajectoryOK attribute.
 */
	virtual bool is_currentTrajectoryOK_allowed(Tango::AttReqType type);
/**
 *	Read/Write allowed for pvt attribute.
 */
	virtual bool is_pvt_allowed(Tango::AttReqType type);
/**
 *	Read/Write allowed for pt attribute.
 */
	virtual bool is_pt_allowed(Tango::AttReqType type);
/**
 *	Read/Write allowed for rawPVT attribute.
 */
	virtual bool is_rawPVT_allowed(Tango::AttReqType type);
/**
 *	Read/Write allowed for uploadedRawPVT attribute.
 */
	virtual bool is_uploadedRawPVT_allowed(Tango::AttReqType type);
/**
 *	Execution allowed for GoToOrigin command.
 */
	virtual bool is_GoToOrigin_allowed(const CORBA::Any &any);
/**
 *	Execution allowed for Reset command.
 */
	virtual bool is_Reset_allowed(const CORBA::Any &any);
/**
 *	Execution allowed for CheckValidity command.
 */
	virtual bool is_CheckValidity_allowed(const CORBA::Any &any);
/**
 *	Execution allowed for Start command.
 */
	virtual bool is_Start_allowed(const CORBA::Any &any);
/**
 *	Execution allowed for Upload command.
 */
	virtual bool is_Upload_allowed(const CORBA::Any &any);
/**
 *	Execution allowed for Stop command.
 */
	virtual bool is_Stop_allowed(const CORBA::Any &any);
/**
 *	Execution allowed for GetOrderedAxisList command.
 */
	virtual bool is_GetOrderedAxisList_allowed(const CORBA::Any &any);
/**
 *	Execution allowed for ExecLowLevelCmd command.
 */
	virtual bool is_ExecLowLevelCmd_allowed(const CORBA::Any &any);
/**
 * This command gets the device state (stored in its <i>device_state</i> data member) and returns it to the caller.
 *	@return	State Code
 *	@exception DevFailed
 */
	virtual Tango::DevState	dev_state();
/**
 * This command gets the device status (stored in its <i>device_status</i> data member) and returns it to the caller.
 *	@return	Status description
 *	@exception DevFailed
 */
	virtual Tango::ConstDevString	dev_status();
/**
 * go to origin point of the trajectory
 *	@exception DevFailed
 */
	void	go_to_origin();
/**
 * reset the trajectory
 *	@exception DevFailed
 */
	void	reset();
/**
 * Checks if the trajectory is possible
 *	* You must be at trajectory origin position
 *	@return	true if trajectory is possible
 *	@exception DevFailed
 */
	Tango::DevBoolean	check_validity();
/**
 * Starts the trajectory
 *	* you must be at origin point
 *	* trajectory check must have been successful
 *	@exception DevFailed
 */
	void	start();
/**
 * Uploads the previously written "trajectory" attribute in the controller
 *	@exception DevFailed
 */
	void	upload();
/**
 * Stops the execution of the running trajectory
 *	@exception DevFailed
 */
	void	stop();
/**
 * Gives the list of axis in the order required when setting trajectory arrays
 *	@return	Ordrered list of axis
 *	@exception DevFailed
 */
	Tango::DevVarStringArray	*get_ordered_axis_list();
/**
 * Execute low level command.
 *	@param	argin	command
 *	@return	return value if any
 *	@exception DevFailed
 */
	Tango::DevString	exec_low_level_cmd(Tango::DevString);

/**
 *	Read the device properties from database
 */
	 void get_device_property();
//@}

	//	Here is the end of the automatic code generation part
	//-------------------------------------------------------------	

protected :	
	//	Add your own data members here
	//-----------------------------------------
	//- CTRLCRF-119:
	//-------------------------------------------
	//- Trajectory algorithm, enums, consts
	//-------------------------------------------
	typedef enum
	{
		E_NATIVE_ALGORITHM = 0,
		E_SIXS_PYTHON_ALGORITHM,
		E_UNKNOWN
	}TrajectoryAlgo;
  xps_ns::Trajectory * xps_trajectory;

	bool properties_missing;
	bool init_device_done;
	std::string status_str;
};

}	// namespace_ns

#endif	// _XPSTRAJECTORY_H

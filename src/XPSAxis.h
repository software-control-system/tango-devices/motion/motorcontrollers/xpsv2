//=============================================================================
//
// file :        XPSAxis.h
//
// description : Include for the XPSAxis class.
//
// project :	XPSAxis
//
// $Author:  $
//
// $Revision:  $
// $Date:  $
//
// SVN only:
// $HeadURL: $
//
// CVS only:
// $Source:  $
// $Log:  $
//
// copyleft :    Synchrotron SOLEIL 
//               L'Orme des merisiers - Saint Aubin
//     BP48 - 91192 Gif sur Yvette
//               FRANCE
//
//=============================================================================
//
//      This file is generated by POGO
//  (Program Obviously used to Generate tango Object)
//
//         (c) - Software Engineering Group - ESRF
//=============================================================================
#ifndef _XPSAXIS_H
#define _XPSAXIS_H

#include <tango.h>
#include "XPSLib/Positioner.h"
#include "XPSLib/Observer.h"
#include <yat4tango/CommonHeader.h>
#include <yat4tango/ExceptionHelper.h>

//using namespace Tango;

/**
 * @author $Author:  $
 * @version  $Revision:  $
 */

//  Add your own constant definitions here.
//-----------------------------------------------

namespace XPSAxis_ns
{

  /**
  * Class Description:
   * Pure virtual Class for Axis family
 */

/*
 *	Device States Description:
*  Tango::STANDBY :  axis iddle, ready
*  Tango::MOVING :   axis moving
*  Tango::ALARM :    non-bloquing fault axis can move
*  Tango::FAULT :    bloquing fault, axis cannot move
*  Tango::OFF :      power unit off
*  Tango::UNKNOWN :  communication broken with controller
 */


	class XPSAxis: public Tango::Device_4Impl, public xps_ns::Observer
	{
	public :
		//	Add your own data members here
		//-----------------------------------------


		//	Here is the Start of the automatic code generation part
		//-------------------------------------------------------------	
		/**
		*	@name attributes
		*	Attribute member data.
		*/
		//@{
		Tango::DevDouble	*attr_position_read;
		Tango::DevDouble	attr_position_write;
		Tango::DevDouble	*attr_acceleration_read;
		Tango::DevDouble	attr_acceleration_write;
		Tango::DevDouble	*attr_deceleration_read;
		Tango::DevDouble	attr_deceleration_write;
		Tango::DevDouble	*attr_velocity_read;
		Tango::DevDouble	attr_velocity_write;
		Tango::DevDouble	*attr_accuracy_read;
		Tango::DevDouble	attr_accuracy_write;
		Tango::DevDouble	*attr_offset_read;
		Tango::DevDouble	attr_offset_write;
		Tango::DevDouble	attr_relativeMove_write;
		Tango::DevDouble	*attr_xpsSetpoint_read;
		Tango::DevDouble	*attr_softLimitMin_read;
		Tango::DevDouble	attr_softLimitMin_write;
		Tango::DevDouble	*attr_softLimitMax_read;
		Tango::DevDouble	attr_softLimitMax_write;
		Tango::DevBoolean	*attr_isTriggerActive_read;
//@}

		/**
		* @name Device properties
		* Device properties member data.
		*/
		//@{
/**
 *	nom de l'axe
 */
	string	axisName;
/**
 *	When a movement is started , the device statys in MOVING state during forcestateMovingTimeAxis milliseconds whatever the real state is .
 *	In fact the state information returned by XPS can be not very reliable depending on axis configuration (tuning of setlling time parameters for example)
 */
	Tango::DevDouble	forceStateMovingTimeAxis;
//@}

		/**
		*	@name Device properties
		*	Device property member data.
		*/
		//@{
		//@}

		/**@name Constructors
		* Miscellaneous constructors */
		//@{
		/**
		* Constructs a newly allocated Command object.
		*
		*	@param cl	Class.
		*	@param s 	Device Name
		*/
		XPSAxis(Tango::DeviceClass *cl,string &s);
		/**
		* Constructs a newly allocated Command object.
		*
		*	@param cl	Class.
		*	@param s 	Device Name
		*/
		XPSAxis(Tango::DeviceClass *cl,const char *s);
		/**
		* Constructs a newly allocated Command object.
		*
		*	@param cl	Class.
		*	@param s 	Device name
		*	@param d	Device description.
		*/
		XPSAxis(Tango::DeviceClass *cl,const char *s,const char *d);
		//@}

		/**@name Destructor
		* Only one destructor is defined for this class */
		//@{
		/**
		* The object destructor.
		*/	
		~XPSAxis() {delete_device();}
		/**
		*	will be called at device destruction or at init command.
		*/
		void delete_device();
		//@}


		/**@name Miscellaneous methods */
		//@{
		/**
		*	Initialize the device
		*/
		virtual void init_device();
		/**
		*	Always executed method before execution command method.
		*/
		virtual void always_executed_hook();

//@}

/**
 * @name XPSAxis methods prototypes
 */

//@{
/**
 *	Hardware acquisition for attributes.
 */
	virtual void read_attr_hardware(vector<long> &attr_list);
/**
 *	Extract real attribute values for position acquisition result.
 */
	virtual void read_position(Tango::Attribute &attr);
/**
 *	Write position attribute values to hardware.
 */
	virtual void write_position(Tango::WAttribute &attr);
/**
 *	Extract real attribute values for acceleration acquisition result.
 */
	virtual void read_acceleration(Tango::Attribute &attr);
/**
 *	Write acceleration attribute values to hardware.
 */
	virtual void write_acceleration(Tango::WAttribute &attr);
/**
 *	Extract real attribute values for deceleration acquisition result.
 */
	virtual void read_deceleration(Tango::Attribute &attr);
/**
 *	Write deceleration attribute values to hardware.
 */
	virtual void write_deceleration(Tango::WAttribute &attr);
/**
 *	Extract real attribute values for velocity acquisition result.
 */
	virtual void read_velocity(Tango::Attribute &attr);
/**
 *	Write velocity attribute values to hardware.
 */
	virtual void write_velocity(Tango::WAttribute &attr);
/**
 *	Extract real attribute values for accuracy acquisition result.
 */
	virtual void read_accuracy(Tango::Attribute &attr);
/**
 *	Write accuracy attribute values to hardware.
 */
	virtual void write_accuracy(Tango::WAttribute &attr);
/**
 *	Extract real attribute values for offset acquisition result.
 */
	virtual void read_offset(Tango::Attribute &attr);
/**
 *	Write offset attribute values to hardware.
 */
	virtual void write_offset(Tango::WAttribute &attr);
/**
 *	Extract real attribute values for relativeMove acquisition result.
 */
	virtual void read_relativeMove(Tango::Attribute &attr);
/**
 *	Write relativeMove attribute values to hardware.
 */
	virtual void write_relativeMove(Tango::WAttribute &attr);
/**
 *	Extract real attribute values for xpsSetpoint acquisition result.
 */
	virtual void read_xpsSetpoint(Tango::Attribute &attr);
/**
 *	Extract real attribute values for softLimitMin acquisition result.
 */
	virtual void read_softLimitMin(Tango::Attribute &attr);
/**
 *	Write softLimitMin attribute values to hardware.
 */
	virtual void write_softLimitMin(Tango::WAttribute &attr);
/**
 *	Extract real attribute values for softLimitMax acquisition result.
 */
	virtual void read_softLimitMax(Tango::Attribute &attr);
/**
 *	Write softLimitMax attribute values to hardware.
 */
	virtual void write_softLimitMax(Tango::WAttribute &attr);
/**
 *	Extract real attribute values for isTriggerActive acquisition result.
 */
	virtual void read_isTriggerActive(Tango::Attribute &attr);
/**
 *	Read/Write allowed for position attribute.
 */
	virtual bool is_position_allowed(Tango::AttReqType type);
/**
 *	Read/Write allowed for acceleration attribute.
 */
	virtual bool is_acceleration_allowed(Tango::AttReqType type);
/**
 *	Read/Write allowed for deceleration attribute.
 */
	virtual bool is_deceleration_allowed(Tango::AttReqType type);
/**
 *	Read/Write allowed for velocity attribute.
 */
	virtual bool is_velocity_allowed(Tango::AttReqType type);
/**
 *	Read/Write allowed for accuracy attribute.
 */
	virtual bool is_accuracy_allowed(Tango::AttReqType type);
/**
 *	Read/Write allowed for offset attribute.
 */
	virtual bool is_offset_allowed(Tango::AttReqType type);
/**
 *	Read/Write allowed for relativeMove attribute.
 */
	virtual bool is_relativeMove_allowed(Tango::AttReqType type);
/**
 *	Read/Write allowed for xpsSetpoint attribute.
 */
	virtual bool is_xpsSetpoint_allowed(Tango::AttReqType type);
/**
 *	Read/Write allowed for softLimitMin attribute.
 */
	virtual bool is_softLimitMin_allowed(Tango::AttReqType type);
/**
 *	Read/Write allowed for softLimitMax attribute.
 */
	virtual bool is_softLimitMax_allowed(Tango::AttReqType type);
/**
 *	Read/Write allowed for isTriggerActive attribute.
 */
	virtual bool is_isTriggerActive_allowed(Tango::AttReqType type);
/**
 *	Execution allowed for Stop command.
 */
	virtual bool is_Stop_allowed(const CORBA::Any &any);
/**
 *	Execution allowed for ComputeNewOffset command.
 */
	virtual bool is_ComputeNewOffset_allowed(const CORBA::Any &any);
/**
 *	Execution allowed for GetFirmwareRevision command.
 */
	virtual bool is_GetFirmwareRevision_allowed(const CORBA::Any &any);
/**
 *	Execution allowed for TriggerEnable command.
 */
	virtual bool is_TriggerEnable_allowed(const CORBA::Any &any);
/**
 *	Execution allowed for TriggerDisable command.
 */
	virtual bool is_TriggerDisable_allowed(const CORBA::Any &any);
/**
 *	Execution allowed for TriggerParameters command.
 */
	virtual bool is_TriggerParameters_allowed(const CORBA::Any &any);
/**
 *	Execution allowed for ExecLowLevelCmd command.
 */
	virtual bool is_ExecLowLevelCmd_allowed(const CORBA::Any &any);
/**
 * This command gets the device state (stored in its <i>device_state</i> data member) and returns it to the caller.
 *	@return	State Code
 *	@exception DevFailed
 */
	virtual Tango::DevState	dev_state();
/**
 * This command gets the device status (stored in its <i>device_status</i> data member) and returns it to the caller.
 *	@return	Status description
 *	@exception DevFailed
 */
	virtual Tango::ConstDevString	dev_status();
/**
 * 
 *	@exception DevFailed
 */
	void	stop();
/**
 * the so smart program computes the offset to goal the user position
 *	@param	argin	the new user position 
 *	@exception DevFailed
 */
	void	compute_new_offset(Tango::DevDouble);
/**
 * Returns the firmware revision.
 *	@return	The firmware revision
 *	@exception DevFailed
 */
	Tango::DevString	get_firmware_revision();
/**
 * Enables Trigger
 *	@exception DevFailed
 */
	void	trigger_enable();
/**
 * Disables Trigger
 *	@exception DevFailed
 */
	void	trigger_disable();
/**
 * pulseWidth must be in [0.2|1|2.5|10] (0.2 is default)
 *	Encoder SettlingTime must be in [0.075|1|4|12] (0.075 is default)
 *	@param	argin	Pos Debut,Pos Fin, Step, PulseWidh in [0.2|1|2.5|10] , SettlingTime in [0.075|1|4|12]
 *	@exception DevFailed
 */
	void	trigger_parameters(const Tango::DevVarDoubleArray *);
/**
 * Execute low level command.
 *	@param	argin	command
 *	@return	return value if any
 *	@exception DevFailed
 */
	Tango::DevString	exec_low_level_cmd(Tango::DevString);

/**
 *	Read the device properties from database
 */
	 void get_device_property();
//@}

		//	Here is the end of the automatic code generation part
		//-------------------------------------------------------------	

	protected :	
		//	Add your own data members here
		//-----------------------------------------
		xps_ns::Positioner * positioner;

		bool properties_missing;
		bool init_device_done;

		std::string status_str;
//		void get_attr_position_min_max_props (double & min, double & max);

		// description :  surcharge methode virtuelle pure Observer
		void Update (void);

		// writes back the write part of attribute
		template<class T> void write_wattribute(Tango::DevString attribute_name, T value)
		{
			Tango::WAttribute &att = dev_attr->get_w_attr_by_name(attribute_name);
			att.set_write_value(value);
		} 
		
		//- TANGODEVIC-57:
		//--------------------------------
		//bool m_backlashEnabled;
		
		//- get stored value in Device property
		template <class T>
		T get_value_as_property(Tango::DeviceImpl* dev_p, const std::string& property_name)
			throw (Tango::DevFailed)
		{
			if (!Tango::Util::instance()->_UseDb)
			{
				THROW_DEVFAILED(_CPTC("DEVICE_ERROR"), 
					_CPTC("NO DB"),
					_CPTC("XPSAxis::get_value_as_property")); 
			} 

			T value;
			Tango::DbData	dev_prop;
			dev_prop.push_back(Tango::DbDatum(property_name));

			//-	Call database and extract values
			//--------------------------------------------
			try
			{
				dev_p->get_db_device()->get_property(dev_prop);
			}
			catch (Tango::DevFailed &df)
			{
				std::string message = "Error in reading " + property_name + " in Configuration DataBase.";
				ERROR_STREAM << df << endl;
				RETHROW_DEVFAILED(df,	_CPTC("DEVICE_ERROR"), 
					  _CPTC(std::string(df.errors[0].desc).c_str()), 
					  _CPTC("XPSAxis::get_value_as_property")); 
			}

			//-	Try to extract saved property from database
			if (dev_prop[0].is_empty()==false)
			{
				dev_prop[0] >> value;
			}
			else
			{
			  // no value stored in data base
				THROW_DEVFAILED(_CPTC("DEVICE_ERROR"), 
					_CPTC("No value in database"), 
					_CPTC("XPSAxis::get_value_as_property")); 
			}

			return value;
		}	
        
	};

}	// namespace_ns

#endif	// _XPSAXIS_H

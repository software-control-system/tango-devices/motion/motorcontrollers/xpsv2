from conan import ConanFile

class XPSV2Recipe(ConanFile):
    name = "xpsv2"
    executable = "ds_XPSV2"
    version = "1.6.1"
    package_type = "application"
    user = "soleil"
    python_requires = "base/[>=1.0]@soleil/stable"
    python_requires_extend = "base.Device"

    license = "GPL-3.0-or-later"    
    author = "Jean Coquet"
    url = "https://gitlab.synchrotron-soleil.fr/software-control-system/tango-devices/motion/motorcontrollers/xpsv2.git"
    description = "None"
    topics = ("control-system", "tango", "device")

    settings = "os", "compiler", "build_type", "arch"

    exports_sources = "CMakeLists.txt", "src/*"
    
    def requirements(self):
        self.requires("yat4tango/[>=1.0]@soleil/stable")
        self.requires("curl/7.37.0@soleil/stable")
        if self.settings.os == "Linux":
            self.requires("crashreporting2/[>=1.0]@soleil/stable")
